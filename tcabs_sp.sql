USE tcabs;

DROP PROCEDURE IF EXISTS AUTHENTICATE_USER;

DROP PROCEDURE IF EXISTS ADD_USER;
DROP PROCEDURE IF EXISTS GET_USER;
DROP PROCEDURE IF EXISTS GET_ALL_USERS;
DROP PROCEDURE IF EXISTS GET_USERS_BY_ROLE;
DROP PROCEDURE IF EXISTS GET_USERS_BY_PERMISSION;
DROP PROCEDURE IF EXISTS UPDATE_USER;
DROP PROCEDURE IF EXISTS DELETE_USER;

DROP PROCEDURE IF EXISTS ADD_UNIT;
DROP PROCEDURE IF EXISTS GET_UNIT;
DROP PROCEDURE IF EXISTS GET_ALL_UNITS;
DROP PROCEDURE IF EXISTS UPDATE_UNIT;
DROP PROCEDURE IF EXISTS DELETE_UNIT;

DROP PROCEDURE IF EXISTS ADD_UNIT_OFFERING;
DROP PROCEDURE IF EXISTS UPDATE_UNIT_OFFERING;
DROP PROCEDURE IF EXISTS GET_UNIT_OFFERING;
DROP PROCEDURE IF EXISTS GET_ALL_UNIT_OFFERINGS;
DROP PROCEDURE IF EXISTS DELETE_UNIT_OFFERING;
DROP PROCEDURE IF EXISTS GET_ALL_TEACHING_PERIODS;
DROP PROCEDURE IF EXISTS GET_ALL_TEACHING_YEARS;

DROP PROCEDURE IF EXISTS ADD_ENROLMENT;
DROP PROCEDURE IF EXISTS GET_ENROLMENT;
DROP PROCEDURE IF EXISTS GET_ALL_ENROLMENTS;
DROP PROCEDURE IF EXISTS GET_ENROLMENTS_BY_UNIT_OFFERING;
DROP PROCEDURE IF EXISTS GET_ENROLMENTS_BY_UNIT_OFFERING_INVERT;
DROP PROCEDURE IF EXISTS UPDATE_ENROLMENT;
DROP PROCEDURE IF EXISTS DELETE_ENROLMENT;

DROP PROCEDURE IF EXISTS ADD_TEAM;
DROP PROCEDURE IF EXISTS GET_TEAM;
DROP PROCEDURE IF EXISTS GET_ALL_TEAMS;
DROP PROCEDURE IF EXISTS UPDATE_TEAM;
DROP PROCEDURE IF EXISTS DELETE_TEAM;

DROP PROCEDURE IF EXISTS ADD_PROJECT;
DROP PROCEDURE IF EXISTS GET_PROJECT;
DROP PROCEDURE IF EXISTS GET_ALL_PROJECTS;
DROP PROCEDURE IF EXISTS UPDATE_PROJECT;
DROP PROCEDURE IF EXISTS DELETE_PROJECT;

DROP PROCEDURE IF EXISTS ADD_DUTY;
DROP PROCEDURE IF EXISTS GET_DUTY;
DROP PROCEDURE IF EXISTS GET_ALL_DUTIES;
DROP PROCEDURE IF EXISTS UPDATE_DUTY;
DROP PROCEDURE IF EXISTS DELETE_DUTY;

DROP PROCEDURE IF EXISTS ADD_PROJECT_TEAM;
DROP PROCEDURE IF EXISTS GET_ALL_PROJECT_TEAMS;
DROP PROCEDURE IF EXISTS GET_ALL_TEAM_PROJECTS;
DROP PROCEDURE IF EXISTS DELETE_PROJECT_TEAM;

DROP PROCEDURE IF EXISTS ADD_INDIVIDUAL_TASK;
DROP PROCEDURE IF EXISTS GET_INDIVIDUAL_TASK;
DROP PROCEDURE IF EXISTS GET_ALL_INDIVIDUAL_TASKS;
DROP PROCEDURE IF EXISTS UPDATE_INDIVIDUAL_TASK;
DROP PROCEDURE IF EXISTS DELETE_INDIVIDUAL_TASK;


DELIMITER $$

-- Verifies that the provided username/password matches the username password in the system
-- Returns a single-column table (select statement) which contains all the permissions for that user
CREATE PROCEDURE AUTHENTICATE_USER (pUserID VARCHAR(32), pPassword VARCHAR(128))
BEGIN
	DECLARE vPassword VARCHAR(128);
    DECLARE eInvalidEx CONDITION FOR SQLSTATE '45000';
	
    SET vPassword = (SELECT Password FROM User WHERE UserID = pUserId);

	IF STRCMP(pPassword, vPassword) = 0 THEN
		SELECT Permission.PermissionID, Permission.Name
		FROM Permission
		INNER JOIN RolePermission ON RolePermission.PermissionID = Permission.PermissionID
		INNER JOIN Role ON RolePermission.RoleID = Role.RoleID
		INNER JOIN UserRole ON Role.RoleID = UserRole.RoleID
		INNER JOIN User ON UserRole.UserID = User.UserID
		WHERE User.UserID = pUserID
		GROUP BY Permission.PermissionID,Permission.Name;
	ELSE
		SIGNAL eInvalidEx
			SET MESSAGE_TEXT = 'Invalid username or password.';
	END IF;
END$$


-- ************************** --
-- *       PASS TASKS       * --
-- ************************** --

-- 1.1 REGISTER NEW EMPLOYEE
-- 1.3 REGISTER STUDENT

-- Add a user to the database
CREATE PROCEDURE ADD_USER (pUserId VARCHAR(32), pPassword VARCHAR(128), pFirstName VARCHAR(32), pLastName VARCHAR(32), pRole VARCHAR(32))
BEGIN
	DECLARE vExisting INT UNSIGNED DEFAULT 0;
    DECLARE eUserEx CONDITION FOR SQLSTATE '20012';
    
	SET vExisting = (SELECT COUNT(*) FROM User WHERE UserID = pUserId);
	IF vExisting = 0 THEN
		INSERT INTO User (UserID, Password, FirstName, LastName)
		VALUES (pUserId, pPassword, pFirstName, pLastName);
		INSERT INTO UserRole (UserID, RoleID)
		VALUES (pUserId, (SELECT RoleID FROM Role WHERE Name = pRole));
	ELSE
		SIGNAL eUserEx
			SET MESSAGE_TEXT = 'User ID already exists.';
	END IF;
END$$

-- Get a user by ID
CREATE PROCEDURE GET_USER (IN pUserID VARCHAR(32), OUT rFirstName VARCHAR(32), OUT rLastName VARCHAR(32), OUT rRoleID INT, OUT rRoleName VARCHAR(32))
BEGIN
	DECLARE vUserId VARCHAR(32);
	DECLARE eUserEx CONDITION FOR SQLSTATE '45000';
	
    SET vUserId = (SELECT UserID FROM User WHERE UserID = pUserId);
	IF STRCMP(pUserId, vUserId) = 0 THEN
		SELECT User.FirstName, User.LastName, Role.RoleID, Role.Name
		INTO rFirstName, rLastName, rRoleID, rRoleName
		FROM User
			INNER JOIN UserRole ON User.UserID = UserRole.UserID
			INNER JOIN Role ON UserRole.RoleID = Role.RoleID
		WHERE User.UserID = pUserId;
	ELSE
		SIGNAL eUserEx
		SET MESSAGE_TEXT = 'User ID does not exist.';
	END IF;
END$$

-- Get all users in the database
CREATE PROCEDURE GET_ALL_USERS ()
BEGIN
	SELECT User.UserID, User.FirstName, User.LastName, Role.RoleID, Role.Name
	FROM User
	INNER JOIN UserRole ON User.UserID = UserRole.UserID
	INNER JOIN Role ON UserRole.RoleID = Role.RoleID;
END$$

-- Get all users in the database by role
CREATE PROCEDURE GET_USERS_BY_ROLE (pName VARCHAR(32))
BEGIN
	SELECT User.UserID, User.FirstName, User.LastName, Role.RoleID, Role.Name
	FROM User
	INNER JOIN UserRole ON User.UserID = UserRole.UserID
	INNER JOIN Role ON UserRole.RoleID = Role.RoleID
    WHERE Role.Name = pName;
END$$

-- Get all users in the database by permissions
CREATE PROCEDURE GET_USERS_BY_PERMISSION (pName VARCHAR(32))
BEGIN
	SELECT User.UserID, User.FirstName, User.LastName, Role.RoleID, Role.Name
	FROM User
	INNER JOIN UserRole ON User.UserID = UserRole.UserID
	INNER JOIN Role ON UserRole.RoleID = Role.RoleID
    INNER JOIN RolePermission ON Role.RoleID = RolePermission.RoleID
    INNER JOIN Permission ON RolePermission.PermissionID = Permission.PermissionID
    WHERE Permission.Name = pName;
END$$

-- Update a user in the database
CREATE PROCEDURE UPDATE_USER (pUserId VARCHAR(32), pPassword VARCHAR(128), pFirstName VARCHAR(32), pLastName VARCHAR(32), pRole VARCHAR(32))
BEGIN
    DECLARE vUpdatePassword BOOLEAN;
	DECLARE vUserId VARCHAR(32);
    DECLARE eAdminBlockEx CONDITION FOR SQLSTATE '45000';
    DECLARE eNotFoundEx CONDITION FOR SQLSTATE '45000';
    
    IF STRCMP(pUserId, 'admin') = 0 && (NOT STRCMP(pRole, 'Administrator') = 0) THEN
		SIGNAL eAdminBlockEx
			SET MESSAGE_TEXT = 'Default administrator\'s role cannot be downgraded.';
    END IF;
	
    SET vUserId = (SELECT UserID FROM User WHERE UserID = pUserId);
	IF STRCMP(pUserId, vUserId) = 0 THEN
		IF LENGTH(pPassword) > 0 THEN
			SET vUpdatePassword = TRUE;
		ELSE
			SET vUpdatePassword = FALSE;
		END IF;

		IF vUpdatePassword = TRUE THEN
			UPDATE User
			SET	FirstName = pFirstname,
				LastName = pLastName,
				Password = pPassword
			WHERE 
				UserID = pUserId;
		ELSE
			UPDATE User
			SET	FirstName = pFirstname,
				LastName = pLastName
			WHERE 
				UserID = pUserId;
		END IF;

		UPDATE UserRole
		SET RoleID = (SELECT RoleID FROM Role WHERE Name = pRole)
		WHERE UserID = pUserID;
	ELSE
		SIGNAL eNotFoundEx
			SET MESSAGE_TEXT = 'User ID does not exist.';
	END IF;
END$$

-- Delete a user to the database
CREATE PROCEDURE DELETE_USER (pUserId VARCHAR(32))
BEGIN
	DECLARE vUserId VARCHAR(32);
    DECLARE eNotFoundEx CONDITION FOR SQLSTATE '45000';
    DECLARE eAdminBlockEx CONDITION FOR SQLSTATE '45000';
    
    DECLARE EXIT HANDLER FOR SQLSTATE '23000'
	BEGIN
    SIGNAL SQLSTATE '23000' SET 
		MYSQL_ERRNO = 1451,
		MESSAGE_TEXT = 'This user is currently enrolled into a unit or is managing a unit and cannot be removed from the system.';
	END;
	
    -- Do not allow admin user to be deleted
    IF STRCMP(pUserId, 'admin') = 0 THEN
		SIGNAL eAdminBlockEx
			SET MESSAGE_TEXT = 'Default administrator cannot be removed from system.';
	END IF;
    
    SET vUserId = (SELECT UserID FROM User WHERE UserID = pUserId);
    IF STRCMP(pUserId, vUserId) = 0 THEN
		DELETE FROM UserRole WHERE UserID = pUserId;
		DELETE FROM User WHERE UserID = pUserId;
	ELSE
		SIGNAL eNotFoundEx
			SET MESSAGE_TEXT = 'User ID does not exist.';
	END IF;
END$$

-- 1.2 REGISTER UNITS OF STUDY
-- Add a unit to the database
CREATE PROCEDURE ADD_UNIT (pUnitCode VARCHAR(32), pConvenorID VARCHAR(128), pName VARCHAR(32))
BEGIN
	DECLARE vExisting INT UNSIGNED DEFAULT 0;
    DECLARE eUnitEx CONDITION FOR SQLSTATE '20012';
	
    SET vExisting = (SELECT COUNT(*) FROM Unit WHERE UnitCode = pUnitCode);
	IF vExisting = 0 THEN
		INSERT INTO Unit (UnitCode, ConvenorID, Name)
		VALUES (pUnitCode, pConvenorID, pName);
	ELSE
		SIGNAL eUnitEx
			SET MESSAGE_TEXT = 'Duplicate Unit Code';
	END IF;
END$$

-- Get a unit in the database
CREATE PROCEDURE GET_UNIT (pUnitCode VARCHAR(32))
BEGIN
select (Unitcode,ConvernorID, Titile)
from Unit
where UnitCode = pUnitCode ;
END $$

-- Get all units in the database
CREATE PROCEDURE GET_ALL_UNITS ()
BEGIN
	SELECT Unit.UnitCode, Unit.Name AS UnitName, User.UserID, User.FirstName, User.LastName, Role.RoleID, Role.Name AS RoleName
	FROM Unit
		INNER JOIN User ON Unit.ConvenorID = User.UserID
		INNER JOIN UserRole ON User.UserID = UserRole.UserID
		INNER JOIN Role ON UserRole.RoleID = Role.RoleID;
END $$

-- Update Unit in the database
CREATE PROCEDURE UPDATE_UNIT (pUnitCode VARCHAR(32), pConvenorID VARCHAR(128), pName VARCHAR(32))
BEGIN
	DECLARE vUnitCode VARCHAR(32);
    DECLARE eUnitEx CONDITION FOR SQLSTATE '45000';
    
	SET vUnitCode = (SELECT UnitCode FROM Unit WHERE UnitCode = pUnitCode);
	IF STRCMP(pUnitCode, vUnitCode) = 0 THEN
		UPDATE Unit
		SET ConvenorID = pConvenorId, Name = pName
		WHERE UnitCode = pUnitCode;
	ELSE
		SIGNAL eUnitEx
			SET MESSAGE_TEXT = 'Unit code does not exist.';
	END IF;
END $$

-- Delete Unit in the database
CREATE PROCEDURE DELETE_UNIT ( pUnitCode VARCHAR(32))
BEGIN
	DECLARE vUnitCode VARCHAR(32);
    DECLARE eUnitEx CONDITION FOR SQLSTATE '45000';
    
    DECLARE EXIT HANDLER FOR SQLSTATE '23000'
	BEGIN
    SIGNAL SQLSTATE '23000' SET 
		MYSQL_ERRNO = 1451,
		MESSAGE_TEXT = 'This unit is currently being used in a unit offering and cannot be removed.';
	END;
	
    SET vUnitCode = (SELECT UnitCode FROM Unit WHERE UnitCode = pUnitCode);
	IF STRCMP(pUnitCode, vUnitCode) = 0 THEN
		DELETE FROM Unit where UnitCode = pUnitCode ;
	ELSE
		SIGNAL eUnitEx
			SET MESSAGE_TEXT = 'Unit code does not exist.';
	END IF;
END $$

-- Add Unit Offering to database
CREATE PROCEDURE ADD_UNIT_OFFERING (pUnitCode VARCHAR(32), pTeachingPeriodID INT, pYear INT)
BEGIN
	DECLARE vExisting INT UNSIGNED DEFAULT 0;
    DECLARE eDuplicateEx CONDITION FOR SQLSTATE '45000';
	
    SELECT COUNT(*)
    INTO vExisting
    FROM UnitOffering
    WHERE UnitCode = pUnitCode AND TeachingPeriodID = pTeachingPeriodID AND Year = pYear;
    
    IF vExisting = 0 THEN
		INSERT INTO UnitOffering (UnitCode, TeachingPeriodID, Year)
		VALUES (pUnitCode, pTeachingPeriodID, pYear);
	ELSE
		SIGNAL eDuplicateEx
			SET MESSAGE_TEXT = 'A unit offering matching this selection already exists.';
	END IF;
END $$

-- Get Unit Offering from database
-- UNUSED
CREATE PROCEDURE GET_UNIT_OFFERING (pUnitOfferingID INT,
	OUT rUnitCode VARCHAR(32),
    OUT rUnitName VARCHAR(32),
    OUT rTeachingPeriodID INT,
    OUT rTeachingPeriodName VARCHAR(32),
    OUT rYear INT)
BEGIN
	SELECT	UnitOffering.UnitOfferingID,
			Unit.UnitCode, Unit.Name AS UnitName,
			TeachingPeriod.TeachingPeriodID, TeachingPeriod.Name AS TeachingPeriodName,
			TeachingYear.Year
    INTO
			rUnitCode, rUnitName, rTeachingPeriodID, rTeachingPeriodName, rYear
    FROM
			UnitOffering
			INNER JOIN Unit ON UnitOffering.UnitCode = Unit.UnitCode
			INNER JOIN TeachingPeriod ON UnitOffering.TeachingPeriodID = TeachingPeriod.TeachingPeriodID
			INNER JOIN TeachingYear ON UnitOffering.Year = TeachingYear.Year
    WHERE
			Unit.UnitOfferingID = pUnitOfferingID;
END $$

-- Get all Unit Offerings from database
CREATE PROCEDURE GET_ALL_UNIT_OFFERINGS ()
BEGIN
	SELECT	
			UnitOffering.UnitOfferingID,
			Unit.UnitCode, Unit.Name AS UnitName,
			TeachingPeriod.TeachingPeriodID, TeachingPeriod.Name AS TeachingPeriodName,
			TeachingYear.Year
	FROM
			UnitOffering
			INNER JOIN Unit ON UnitOffering.UnitCode = Unit.UnitCode
			INNER JOIN TeachingPeriod ON UnitOffering.TeachingPeriodID = TeachingPeriod.TeachingPeriodID
			INNER JOIN TeachingYear ON UnitOffering.Year = TeachingYear.Year;
END $$

-- Update Unit Offering in database
CREATE PROCEDURE UPDATE_UNIT_OFFERING (pUnitOfferingID INT, pUnitCode VARCHAR(32), pTeachingPeriodID INT, pYear INT)
BEGIN
    DECLARE vExists INT UNSIGNED DEFAULT 0;
    DECLARE vUnitOfferingID INT UNSIGNED DEFAULT 0;
    DECLARE eDuplicateEx CONDITION FOR SQLSTATE '45000';
	
    SELECT COUNT(*), UnitOfferingID
    INTO vExists, vUnitOfferingID
    FROM UnitOffering
    WHERE UnitCode = pUnitCode AND TeachingPeriodID = pTeachingPeriodID AND Year = pYear;
    
    IF vExists = 0 OR vUnitOfferingID = pUnitOfferingID THEN
		UPDATE UnitOffering
		SET
			UnitCode = pUnitCode,
            TeachingPeriodID = pTeachingPeriodID,
            Year = pYear
		WHERE
			UnitOfferingID = pUnitOfferingID;
	ELSE
		SIGNAL eDuplicateEx
			SET MESSAGE_TEXT = 'A unit offering matching this selection already exists.';
	END IF;
END $$

CREATE PROCEDURE DELETE_UNIT_OFFERING (pUnitOfferingID INT)
BEGIN
	DECLARE vUnitOfferingID VARCHAR(32);
    DECLARE eNotFoundEx CONDITION FOR SQLSTATE '45000';
    
    DECLARE EXIT HANDLER FOR SQLSTATE '23000'
	BEGIN
    SIGNAL SQLSTATE '23000' SET 
		MYSQL_ERRNO = 1451,
		MESSAGE_TEXT = 'This unit offering currently has students enrolled and cannot be removed.';
	END;
	
    SET vUnitOfferingID = (SELECT UnitOfferingID FROM UnitOffering WHERE UnitOfferingID = pUnitOfferingID);
	IF STRCMP(pUnitOfferingID, vUnitOfferingID) = 0 THEN
		DELETE FROM UnitOffering
		WHERE UnitOfferingID = pUnitOfferingID;
	ELSE
		SIGNAL eNotFoundEx
			SET MESSAGE_TEXT = 'Unit Offering ID does not exist.';
	END IF;
END $$

CREATE PROCEDURE GET_ALL_TEACHING_PERIODS ()
BEGIN
	SELECT TeachingPeriodID, Name
    FROM TeachingPeriod;
END $$

CREATE PROCEDURE GET_ALL_TEACHING_YEARS ()
BEGIN
	SELECT Year
    FROM TeachingYear;
END $$

-- 1.4 ENROL STUDENT IN UNIT
-- Add enrolment
CREATE PROCEDURE ADD_ENROLMENT(pUnitOfferingID INT, pStudentID varchar(32))
BEGIN
	INSERT INTO Enrolment (UnitOfferingID, StudentID)
	VALUES (pUnitOfferingID, pStudentID);
END $$

-- Get an enrolment in the database
-- NOT USED
CREATE PROCEDURE GET_ENROLMENT (pEnrolmentID varchar(32))
BEGIN
   SELECT *
   FROM enrolment
   WHERE EnrolmentID = pEnrolmentID ;
END $$


-- Get all enrolments in the database
CREATE PROCEDURE GET_ALL_ENROLMENTS ()
BEGIN
	SELECT
			Enrolment.EnrolmentID,
			User.UserID, User.FirstName, User.LastName,
            UnitOffering.UnitOfferingID,
            Unit.UnitCode,
            TeachingPeriod.Name AS TeachingPeriodName,
            TeachingYear.Year
	FROM
			Enrolment
            INNER JOIN User ON Enrolment.StudentID = User.UserID
            INNER JOIN UnitOffering ON Enrolment.UnitOfferingID = UnitOffering.UnitOfferingID
            INNER JOIN Unit ON UnitOffering.UnitCode = Unit.UnitCode
            INNER JOIN TeachingPeriod ON UnitOffering.TeachingPeriodID = TeachingPeriod.TeachingPeriodID
            INNER JOIN TeachingYear ON UnitOffering.Year = TeachingYear.Year;
END$$

-- Get All students who are not enrolled int he specified unit offering
CREATE PROCEDURE GET_ENROLMENTS_BY_UNIT_OFFERING (pUnitOfferingID INT)
BEGIN
	SELECT
		Enrolment.EnrolmentID,
		User.UserID, User.FirstName, User.LastName
	FROM User
		INNER JOIN UserRole ON User.UserID = UserRole.UserID
		INNER JOIN Role ON UserRole.RoleID = Role.RoleID
		INNER JOIN Enrolment ON User.UserID = Enrolment.StudentID
	WHERE Role.Name = 'Student'
	AND Enrolment.UnitOfferingID = pUnitOfferingID;
END$$

CREATE PROCEDURE GET_ENROLMENTS_BY_UNIT_OFFERING_INVERT (pUnitOfferingID INT)
BEGIN
	SELECT
	(SELECT NULL) AS EnrolmentID,
	User.UserID, User.FirstName, User.LastName
FROM User
	LEFT JOIN UserRole ON User.UserID = UserRole.UserID
	LEFT JOIN Role ON UserRole.RoleID = Role.RoleID
	LEFT JOIN Enrolment ON User.UserID = Enrolment.StudentID
WHERE Role.Name = 'Student'
	AND User.UserID NOT IN (
		SELECT User.UserID
		FROM User
			INNER JOIN UserRole ON User.UserID = UserRole.UserID
			INNER JOIN Role ON UserRole.RoleID = Role.RoleID
			INNER JOIN Enrolment ON User.UserID = Enrolment.StudentID
		WHERE Enrolment.UnitOfferingID = pUnitOfferingID
	)
    GROUP BY User.UserID, User.FirstName, User.LastName;
END$$

-- Delete enrolment
CREATE PROCEDURE DELETE_ENROLMENT (pEnrolmentID INT)
BEGIN
	DELETE FROM Enrolment
	WHERE EnrolmentID = pEnrolmentID;
END $$


-- 2.1 REGISTER TEAMS
-- Add a team to the database
CREATE PROCEDURE ADD_TEAM (pSupervisorID VARCHAR(32), pName VARCHAR(128))
BEGIN
	INSERT INTO Team (SupervisorID, Name)
	VALUES (pSupervisorID, pName);
END$$

-- Get a team in the database
-- NOT USED
CREATE PROCEDURE GET_TEAM (pTeamID VARCHAR(32) )
BEGIN
	DECLARE vTeamID VARCHAR(32);
    DECLARE eTeamEx CONDITION FOR SQLSTATE '45000';
	
    SET vTeamID = (SELECT TeamID FROM Team WHERE TeamID = pTeamID);
	IF STRCMP(pTeamID, vTeamID) = 0 THEN
		SELECT TeamID, Name, SupervisorID
        FROM Team
        WHERE TeamID = pTeamID ;
	ELSE
		SIGNAL eTeamEx
			SET MESSAGE_TEXT = 'Team ID does not exist.';
	END IF;
END$$

-- Get all teams in the database
 CREATE PROCEDURE GET_ALL_TEAMS ()
 BEGIN
    SELECT Team.TeamID, Team.Name AS TeamName, User.UserID, User.FirstName, User.LastName, Role.RoleID, Role.Name AS RoleName
	FROM Team
	INNER JOIN User ON Team.SupervisorID = User.UserID
	INNER JOIN UserRole ON User.UserID = UserRole.UserID
	INNER JOIN Role ON UserRole.RoleID = Role.RoleID;
 END $$

-- Update team in the database
CREATE PROCEDURE UPDATE_TEAM (pTeamID int ,pSupervisorID VARCHAR(32), pName VARCHAR(128))
BEGIN
	DECLARE vTeamID VARCHAR(32);
    DECLARE eTeamEx CONDITION FOR SQLSTATE '45000';
	
    SET vTeamID = (SELECT TeamID FROM Team WHERE TeamID = pTeamID);
	IF STRCMP(pTeamID, vTeamID) = 0 THEN
		UPDATE Team
		SET SupervisorId = pSupervisorID, Name = pName
		WHERE TeamID = pTeamID ;
	ELSE
		SIGNAL eTeamEx
			SET MESSAGE_TEXT = 'Team ID does not exist.';
	END IF;
END$$

-- Delete team in the database
CREATE PROCEDURE DELETE_TEAM (pTeamID int )
BEGIN
	DECLARE vTeamID VARCHAR(32);
    DECLARE eTeamEx CONDITION FOR SQLSTATE '45000';
	
    SET vTeamID = (SELECT TeamID FROM Team WHERE TeamID = pTeamID);
	IF STRCMP(pTeamID, vTeamID) = 0 THEN
		DELETE FROM Team WHERE TeamID = pTeamID ;
	ELSE
		SIGNAL eTeamEx
			SET MESSAGE_TEXT = 'Team ID does not exist.';
	END IF;
END$$

-- 2.2 REGISTER PROJECTS
-- Add a project to the database
CREATE PROCEDURE ADD_PROJECT (pName VARCHAR(128), pDescription VARCHAR(32))
BEGIN
	INSERT INTO Project (Name, Description)
	VALUES (pName, pDescription);
END$$

-- Get a project in the database
CREATE PROCEDURE GET_PROJECT ( pProjectId int)
BEGIN
	DECLARE vProjectID VARCHAR(32);
    DECLARE eProjectEx CONDITION FOR SQLSTATE '45000';
	
    SET vProjectID = (SELECT ProjectID FROM Project WHERE ProjectID = pProjectID);
	IF STRCMP(pProjectID, vProjectID) = 0 THEN
		SELECT projectid , name , description FROM project WHERE projectId = pProjectId;
	ELSE
		SIGNAL eProjectEx
			SET MESSAGE_TEXT = 'Project ID does not exist.';
	END IF;
END$$

-- Get all projects in the database
CREATE PROCEDURE GET_ALL_PROJECTS ()
BEGIN
  SELECT ProjectID, Name, Description FROM Project;
END$$

-- Update project in the database
CREATE PROCEDURE UPDATE_PROJECT (pProjectId int,  pName varchar(128) , pDescription varchar(2048))
BEGIN
	DECLARE vProjectID VARCHAR(32);
    DECLARE eProjectEx CONDITION FOR SQLSTATE '45000';
	
    SET vProjectID = (SELECT ProjectID FROM Project WHERE ProjectID = pProjectID);
    IF STRCMP(pProjectID, vProjectID) = 0 THEN
		UPDATE Project
		SET
			Name = pName,
			Description = pDescription
			WHERE ProjectID = pProjectId;
	ELSE
		SIGNAL eProjectEx
			SET MESSAGE_TEXT = 'Project ID does not exist.';
	END IF;
END$$

-- Delete project in the database
CREATE PROCEDURE DELETE_PROJECT ( pProjectID int)
BEGIN
	DECLARE vProjectID VARCHAR(32);
    DECLARE eProjectEx CONDITION FOR SQLSTATE '45000';
	
    SET vProjectID = (SELECT ProjectID FROM Project WHERE ProjectID = pProjectID);
	IF STRCMP(pProjectID, vProjectID) = 0 THEN
		DELETE FROM Project WHERE ProjectID = pProjectID ;
	ELSE
		SIGNAL eProjectEx
			SET MESSAGE_TEXT = 'Project ID does not exist.';
	END IF;
END$$


-- ************************** --
-- *      CREDIT TASKS      * --
-- ************************** --


-- 2.3 SETTING UP THE PROJECT ROLES
-- Add a duty to the database
CREATE PROCEDURE ADD_DUTY (	pDutyID VARCHAR(32), 
							pName VARCHAR(128), 
                            pDescription VARCHAR(32), 
                            pCost VARCHAR(32), 
                            pProjectID VARCHAR(32))
BEGIN
	INSERT INTO Duty (DutyID, Name, Description, Cost)
	VALUES (pDutyID, pName, pDescription, pCost);

	INSERT INTO ProjectDuty (ProjectID, DutyID)
	VALUES	(pProjectID),
			(pDutyID);
END$$


-- Get duty from the database
CREATE PROCEDURE GET_DUTY (pDutyID VARCHAR(32))
BEGIN
	SELECT DutyID, Name, Description, Cost FROM DUTY WHERE DutyID = pDutyID;
END$$


-- Get all duties from the database
CREATE PROCEDURE GET_ALL_DUTIES ( )
BEGIN
	SELECT DutyID, Name, Description, Cost FROM DUTY;
END$$


-- Update duty in the database
CREATE PROCEDURE UPDATE_DUTY (pDutyID VARCHAR(32), 
							pName VARCHAR(128), 
                            pDescription VARCHAR(32), 
                            pCost VARCHAR(32))
BEGIN
	UPDATE DUTY
	SET
	Name = pName ,
	Description = pDescription,
	Cost = pCost
	WHERE DutyID = pDutyID;
END$$

-- Delete duty in the database
CREATE PROCEDURE DELETE_DUTY (pDutyID VARCHAR(32))
BEGIN
	DELETE FROM DUTY WHERE DutyID = pDutyID;
END$$



-- 2.4 PROJECT ALLOCATION
-- Allocate a project to a team
CREATE PROCEDURE ADD_PROJECT_TEAM (pProjectOfferingID VARCHAR(32), pProjectId VARCHAR(32), pTeamID VARCHAR(32))
BEGIN
	INSERT INTO ProjectOffering (ProjectOfferingID, ProjectID)
	VALUES	(pProjectOfferingID), (pProjectId);
			
	INSERT INTO ProjectTeam (ProjectOfferingID, TeamID)
	VALUES	(pProjectOfferingID),
			(pTeamID);
END$$


-- Get all teams associated with a project from the database
CREATE PROCEDURE GET_ALL_PROJECT_TEAMS (pProjectID VARCHAR(32))
BEGIN
	SELECT TeamID FROM Team INNER JOIN Project WHERE ProjectID = pProjectID;
END$$


-- Get all projects associated with a team from the database
CREATE PROCEDURE GET_ALL_TEAM_PROJECTS (pTeamID VARCHAR(32))
BEGIN
	SELECT ProjectID FROM Project INNER JOIN Team WHERE TeamID = pTeamID;
END$$


-- Delete project team from the database
CREATE PROCEDURE DELETE_PROJECT_TEAM (pProjectOfferingID VARCHAR(32), pTeamID VARCHAR(32))
BEGIN
	DELETE FROM ProjectTeam WHERE (ProjectOfferingID = pProjectOfferingID) AND (TeamID = pTeamID);
END$$


-- 3.1 SUBMIT INDIVIDUAL TASK
-- Submit student individual task for team
CREATE PROCEDURE ADD_INDIVIDUAL_TASK (pTaskID INT, pStudentID VARCHAR(32), pDutyID VARCHAR(32), pTeamID VARCHAR(32), 
										pName VARCHAR(32), pDesc VARCHAR(32), pMinutesTaken VARCHAR(32))
BEGIN
    INSERT INTO IndividualTask(TaskID, StudentID, DutyID, TeamID, Name, Description, MinutesTaken)
    VALUES (pTaskID, pStudentID, pDutyID, pTeamID, pName, pDesc, pMinutesTaken);
END$$



-- Get student individual task
CREATE PROCEDURE GET_INDIVIDUAL_TASK ( pTaskID INT, pStudentID VARCHAR(32))
BEGIN
    SELECT TaskID, StudentID, DutyID, TeamID, Name, Description, MinutesTasken
    FROM IndividualTask
    WHERE StudentID = pStudentID && TaskID = pTaskID;
END $$


-- Get student individual task
CREATE PROCEDURE GET_ALL_INDIVIDUAL_TASKS ( )
BEGIN
      SELECT TaskID, StudentID, TeamID, Name, Description
      FROM IndividualTask;
END$$

-- Update student individual task
CREATE PROCEDURE UPDATE_INDIVIDUAL_TASK (pTaskID INT, pStudentID VARCHAR(32), pDutyID INT, pTeamID INT, pName VARCHAR(32), pDescription VARCHAR(32), pTime VARCHAR(32))
BEGIN 
     UPDATE INDIVIDUAL TASK
     SET
	 TaskID = pTaskID,
	 DutyID = pDutyID,
	 TeamID = pTeamID,
     Name = pName ,
     Description = pDescription,
     Time = pTime
     WHERE TaskID = pTaskID;

END$$

-- Delete student individual task
CREATE PROCEDURE DELETE_INDIVIDUAL_TASK ( pTaskID VARCHAR(32))
BEGIN
     DELETE FROM IndividuaTask WHERE (TaskID = pTaskID );

END$$


-- ************************** --
-- *    DISTINCTION TASKS   * --
-- ************************** --



-- ************************** --
-- * HIGH DISTINCTION TASKS * --
-- ************************** --


DELIMITER ;