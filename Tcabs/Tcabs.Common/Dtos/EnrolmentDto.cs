﻿using System;
using System.Collections.Generic;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Dtos
{
    public class EnrolmentDto
    {
        public uint? EnrolmentID { get; set; }
        public UserDto Student { get; set; }
    }
}
