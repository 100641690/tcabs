﻿using System;

namespace Tcabs.Common.Dtos
{
    public class PermissionDto
    {
        public uint PermissionID { get; set; }
        public string Name { get; set; }
    }
}
