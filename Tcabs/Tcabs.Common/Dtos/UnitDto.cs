﻿using System;
using System.Collections.Generic;

namespace Tcabs.Common.Dtos
{
    public class UnitDto
    {
        public string UnitCode { get; set; }
        public UserDto Convenor { get; set; }
        public string Name { get; set; }
    }
}