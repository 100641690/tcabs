﻿using System;

namespace Tcabs.Common.Dtos
{
    public class RoleDto
    {
        public uint RoleID { get; set; }
        public string Name { get; set; }
    }
}