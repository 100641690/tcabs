﻿using System;
using System.Collections.Generic;

namespace Tcabs.Common.Dtos
{
    public class TeamDto
    {
        public int TeamID { get; set; }
        public UserDto Supervisor { get; set; }
        public string Name { get; set; }
       
    }
}