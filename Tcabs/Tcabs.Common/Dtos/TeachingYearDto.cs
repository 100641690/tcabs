﻿using System;
namespace Tcabs.Common.Dtos
{
    public class TeachingYearDto
    {
        public uint Year { get; set; }
    }
}
