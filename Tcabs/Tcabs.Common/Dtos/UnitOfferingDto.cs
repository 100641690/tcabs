﻿using System;
namespace Tcabs.Common.Dtos
{
    public class UnitOfferingDto
    {
        public uint UnitOfferingID { get; set; }
        public UnitDto Unit { get; set; }
        public TeachingPeriodDto TeachingPeriod { get; set; }
        public TeachingYearDto TeachingYear { get; set; }
    }
}
