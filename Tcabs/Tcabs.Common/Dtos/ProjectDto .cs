﻿ using System;
using System.Collections.Generic;

namespace Tcabs.Common.Dtos
{
    public class ProjectDto
    {
        public int ProjectID { get; set; }
       // public UserDto Supervisor { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}