﻿using System;
using System.Collections.Generic;

namespace Tcabs.Common.Dtos
{
    public class UserDto
    {
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public RoleDto Role { get; set; }
    }
}