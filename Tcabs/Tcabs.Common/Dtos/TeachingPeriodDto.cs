﻿using System;
namespace Tcabs.Common.Dtos
{
    public class TeachingPeriodDto
    {
        public uint TeachingPeriodID { get; set; }
        public string Name { get; set; }
    }
}
