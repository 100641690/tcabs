﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tcabs.Common.Requests
{
    public class AddUpdateUnitRequest: Request
    {
        public bool IsUpdate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a Unit Code.")]
        [RegularExpression(@"^[a-zA-Z]{3}[\d]{5}$", ErrorMessage = "Unit ID must be in the format ABC12345.")]
        public string UnitCode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "You must select a convenor for the unit.")]
        public string ConvenorID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a Unit name.")]
        [StringLength(32, MinimumLength = 1, ErrorMessage = "Unit name must be between {2} and {1} characters long.")]
        [RegularExpression(@"^[a-zA-Z\d\s]+$", ErrorMessage = "Unit names cannot contain special characters.")]
        public string Name { get; set; }

        /*
        // Other Validation
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            return results;
        }
        */
    }
}
