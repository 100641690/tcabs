﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tcabs.Common.Requests
{
    /// <summary>
    /// Request for retrieving a unit from the database.
    /// </summary>
    public class GetUnitRequest : Request
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a {0}.")]
        public string UnitCode { get; set; }
    }
}
