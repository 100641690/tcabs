﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tcabs.Common.Requests
{
    public class DeleteUserRequest: Request
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a {0}.")]
        public string UserID { get; set; }
    }
}
