﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tcabs.Common.Requests
{
    public class AddUpdateUnitOfferingRequest: Request
    {
        // If null, assume this is an add request
        public uint? UnitOfferingID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a Unit Code.")]
        public string UnitCode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a Teaching Period.")]
        public uint TeachingPeriodID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a Year.")]
        public uint Year { get; set; }
    }
}
