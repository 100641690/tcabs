﻿using Tcabs.Common.Responses;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace Tcabs.Common.Requests
{
    /// <summary>
    /// Base class to handle validation for requests.
    /// Should be implemented by all request classes.
    /// </summary>
    public abstract class Request : IValidatableObject
    {
        /// <summary>
        /// Validates the data annotations specified in the sub class.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public Response CheckValidation(Response response)
        {
            List<ValidationResult> validationResults = new List<ValidationResult>();
            ValidationContext validationContext = new ValidationContext(this);
            bool isValid = Validator.TryValidateObject(this, validationContext, validationResults, true);

            if (!isValid)
            {
                response.Status = HttpStatusCode.BadRequest;

                foreach (ValidationResult result in validationResults)
                    response.StatusMessages.Add(new StatusMessage(HttpStatusCode.BadRequest, result.ErrorMessage));
            }

            return response;
        }

        /// <summary>
        /// Can be overridden in the sub class to perform more complex validation.
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            return results;
        }
    }
}
