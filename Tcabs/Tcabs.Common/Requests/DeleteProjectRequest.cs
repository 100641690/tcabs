﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tcabs.Common.Requests
{
    public class DeleteProjectRequest: Request
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a {0}.")]
        public int ProjectID { get; set; }
    }
}
