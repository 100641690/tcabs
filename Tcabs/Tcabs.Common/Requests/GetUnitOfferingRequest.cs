﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tcabs.Common.Requests
{
    public class GetUnitOfferingRequest: Request
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a Unit Offering ID.")]
        public uint UnitOfferingID { get; set; }
    }
}
