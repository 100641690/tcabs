﻿using System;
using System.Collections.Generic;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Requests
{
    public class AddUpdateEnrolmentsRequest: Request
    {
        public uint UnitOfferingID { get; set; }
        public List<string> StudentsToEnrol { get; set; } // Student User ID
        public List<uint> EnrolmentsToRemove { get; set; } // Enrolment ID
    }
}
