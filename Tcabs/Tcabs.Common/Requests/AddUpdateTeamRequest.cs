﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Tcabs.Common.Utilities;
using System.Linq;
using System.Text.RegularExpressions;

namespace Tcabs.Common.Requests
{
    /// <summary>
    /// Request for adding a new team to the database.
    /// </summary>
    public class AddUpdateTeamRequest: Request
    {
        // {0} = property name, {1} min value, {2} max value.

        public bool IsUpdate { get; set; }

        public int TeamID { get; set; }

        public string SupervisorID { get; set; }

        // Validate Team Name
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a {0}.")]
        [StringLength(32, MinimumLength = 1, ErrorMessage = "Team names must be between {2} and {1} characters long.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Tea names can only contain alpha characters.")]
        public string Name { get; set; }



    
    
            
    }
}
