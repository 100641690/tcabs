﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tcabs.Common.Requests
{
    public class DeleteTeamRequest: Request
    {
      
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a {0}.")]
        public int TeamID { get; set; }
    }
}
