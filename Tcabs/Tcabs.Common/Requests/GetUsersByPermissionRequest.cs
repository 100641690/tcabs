﻿using System;
namespace Tcabs.Common.Requests
{
    public class GetUsersByPermissionRequest : Request
    {
        public string Name { get; set; }
    }
}
