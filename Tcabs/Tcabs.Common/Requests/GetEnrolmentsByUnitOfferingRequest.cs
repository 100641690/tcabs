﻿using System;
namespace Tcabs.Common.Requests
{
    public class GetEnrolmentsByUnitOfferingRequest: Request
    {
        public uint UnitOfferingID { get; set; }
    }
}
