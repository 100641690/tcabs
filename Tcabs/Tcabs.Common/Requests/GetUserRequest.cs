﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tcabs.Common.Requests
{
    /// <summary>
    /// Request for retrieving a user from the database.
    /// </summary>
    public class GetUserRequest: Request
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a {0}.")]
        public string UserID { get; set; }
    }
}
