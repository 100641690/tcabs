﻿using System;
namespace Tcabs.Common.Requests
{
    /// <summary>
    /// Request for logging a user into the system.
    /// </summary>
    public class AuthenticateUserRequest: Request
    {
        public string UserID { get; set; }
        public string Password { get; set; }
    }
}
