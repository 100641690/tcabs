﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Tcabs.Common.Utilities;
using System.Linq;
using System.Text.RegularExpressions;

namespace Tcabs.Common.Requests
{
    /// <summary>
    /// Request for adding a new user to the database.
    /// </summary>
    public class AddUpdateUserRequest: Request
    {
        // {0} = property name, {1} min value, {2} max value.

        public bool IsUpdate { get; set; }

        public string UserID { get; set; }

        public string Password { get; set; }

        // Validate First Name
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a {0}.")]
        [StringLength(32, MinimumLength = 1, ErrorMessage = "First names must be between {2} and {1} characters long.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "First names can only contain alpha characters.")]
        public string FirstName { get; set; }

        // Validate last name
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a {0}.")]
        [StringLength(32, MinimumLength = 1, ErrorMessage = "Last names must be between {2} and {1} characters long.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Last names can only contain alpha characters.")]
        public string LastName { get; set; }

        public string Role { get; set; }

        // Other Validation
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            // Validate User ID format based on provided Role
            ValidRoles role;
            int minStudentID = 7;
            int maxStudentID = 10;
            int minEmployeeID = 3;
            int maxEmployeeID = 32;
            if (Enum.TryParse(Role, out role))
            {
                if (role == ValidRoles.Student)
                {
                    if (UserID.Length >= minStudentID && UserID.Length <= maxStudentID)
                    {
                        Match match = Regex.Match(UserID, @"^[\d]+$");
                        if (!match.Success)
                        {
                            results.Add(new ValidationResult("Student ID can only contain numeric characters."));
                        }
                    }
                    else
                    {
                        results.Add(new ValidationResult("Student ID must be between " +
                            minStudentID+ " and " + maxStudentID + " characters long."));
                    }
                }
                else
                {
                    if (UserID.Length >= minEmployeeID && UserID.Length <= maxEmployeeID)
                    {
                        Match match = Regex.Match(UserID, @"^[a-zA-Z]+$");
                        if (!match.Success)
                        {
                            results.Add(new ValidationResult(role.ToString() +
                                "ID can only contain letters."));
                        }
                    }
                    else
                    {
                        results.Add(new ValidationResult(role.ToString() + " ID must be between " +
                            minEmployeeID + " and " + maxEmployeeID + " characters long."));
                    }
                }
            }
            else
            {
                results.Add(new ValidationResult("Invalid Role \"" + Role + "\" supplied."));
            }

            // Validte Password based on whether the request is to add or update a record.
            int minPassword = 6;
            int maxPassword = 32;
            if (!IsUpdate)
            {
                if (Password.Length < 6 || Password.Length > 32)
                {
                    results.Add(new ValidationResult("Passwords must be between " + minPassword
                        + " and " + maxPassword + " characters long."));
                }
            }

            return results;
        }
    }
}
