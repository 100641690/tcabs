﻿using System;
using System.Collections.Generic;

namespace Tcabs.Common.Requests
{
    /// <summary>
    /// Request for checking the user has permission to view a page.
    /// The permissions provided by the request are the permissions the page requires.
    /// </summary>
    public class CheckPermissionsRequest: Request
    {
        public List<string> Permissions { get; set; }
    }
}
