﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tcabs.Common.Requests
{
    /// <summary>
    /// Request for retrieving a user from the database.
    /// </summary>
    public class GetProjectRequest: Request
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a {0}.")]
        public int ProjectID { get; set; }
    }
}
