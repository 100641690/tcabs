﻿using System;
namespace Tcabs.Common.Requests
{
    public class GetUsersByRoleRequest : Request
    {
        public string Name { get; set; }
    }
}
