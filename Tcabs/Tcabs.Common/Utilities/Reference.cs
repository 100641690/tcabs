﻿using System;
namespace Tcabs.Common.Utilities
{
    public enum ValidRoles
    {
        Administrator,
        Convenor,
        Supervisor,
        Student
    }
}
