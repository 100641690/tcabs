﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace Tcabs.Common
{
    /// <summary>
    /// Common app settings to use throughout project.
    /// Settings sourced from appsettings.json in Presentation project.
    /// </summary>
    public class AppSettings
    {
        public string Database { get; } = string.Empty;

        public AppSettings()
        {
            var configurationBuilder = new ConfigurationBuilder();

            #if DEBUG
                var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.Development.json");
            #else
                var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            #endif

            configurationBuilder.AddJsonFile(path, false);

            var root = configurationBuilder.Build();

            Database = root.GetConnectionString("Database");
        }

    }
}
