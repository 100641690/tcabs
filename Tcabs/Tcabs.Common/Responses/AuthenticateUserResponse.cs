﻿using System;
using System.Collections.Generic;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Responses
{
    /// <summary>
    /// Response for verifying provided user login credentials against the credentials in the database.
    /// Returned from the business logic layer following the data layer call.
    /// </summary>
    public class AuthenticateUserResponse: Response
    {
        public UserDto User { get; set; }
        public List<PermissionDto> Permissions { get; set; }
    }
}
