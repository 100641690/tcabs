﻿using System.Collections.Generic;
using System.Net;

namespace Tcabs.Common.Responses
{
    /// <summary>
    /// Base response class used to handle message statuses resulting from requests.
    /// All response classes should implement this base class.
    /// </summary>
    public abstract class Response
    {
        public Response()
        {
            StatusMessages = new List<StatusMessage>();
        }

        public List<StatusMessage> StatusMessages { get; set; }

        public HttpStatusCode Status = HttpStatusCode.InternalServerError;
    }

    /// <summary>
    /// Individual message that can be appended to the StatusMessages list.
    /// </summary>
    public class StatusMessage
    {
        public HttpStatusCode MessageStatus { get; set; }

        public string Message { get; set; }

        public StatusMessage(HttpStatusCode statusCode, string message)
        {
            MessageStatus = statusCode;
            Message = message;
        }
    }
}
