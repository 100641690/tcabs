﻿using System;
using System.Collections.Generic;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Responses
{
    public class GetUsersByPermissionResponse : Response
    {
        public List<UserDto> Users { get; set; }
    }
}
