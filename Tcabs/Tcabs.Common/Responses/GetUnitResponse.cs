﻿using System;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Responses
{
    /// <summary>
    /// Response for retrieving a unit from the database.
    /// Returned from the business logic layer following the data layer call.
    /// </summary>
    public class GetUnitResponse : Response
    {
        public UnitDto Unit { get; set; }
    }
}
