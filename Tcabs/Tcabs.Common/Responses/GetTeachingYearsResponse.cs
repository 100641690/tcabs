﻿using System;
using Tcabs.Common.Dtos;
using System.Collections.Generic;

namespace Tcabs.Common.Responses
{
    public class GetTeachingYearsResponse: Response
    {
        public List<TeachingYearDto> TeachingYears { get; set; }
    }
}
