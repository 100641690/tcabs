﻿using System;
using Tcabs.Common.Dtos;
using System.Collections.Generic;

namespace Tcabs.Common.Responses
{
    public class GetTeachingPeriodsResponse: Response
    {
        public List<TeachingPeriodDto> TeachingPeriods { get; set; }
    }
}
