﻿using System;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Responses
{
    /// <summary>
    /// Response for retrieving a user from the database.
    /// Returned from the business logic layer following the data layer call.
    /// </summary>
    public class GetProjectResponse: Response
    {
        public ProjectDto Project { get; set; }
    }
}
