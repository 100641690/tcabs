﻿using System;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Responses
{
    public class GetUnitOfferingResponse: Response
    {
        public UnitOfferingDto UnitOffering { get; set; }
    }
}
