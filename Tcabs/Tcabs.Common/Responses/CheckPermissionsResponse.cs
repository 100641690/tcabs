﻿using System;
using System.Collections.Generic;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Responses
{
    /// <summary>
    /// Response for checking user has permissions to view a page.
    /// TODO: This is currently only used on presentation layer but should probably be verified against the database.
    /// </summary>
    public class CheckPermissionsResponse
    {
        public UserDto User { get; set; }
        public List<string> Permissions { get; set; }
    }
}
