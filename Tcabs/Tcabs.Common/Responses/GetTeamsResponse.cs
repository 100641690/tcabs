﻿using System;
using System.Collections.Generic;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Responses
{
    public class GetTeamsResponse: Response
    {
        public List<TeamDto> Teams { get; set; }
    }
}
