﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tcabs.Common.Responses
{
    /// <summary>
    /// Response to indicate the result of the request to add student to the database
    /// </summary>
    public class AddUpdateUserResponse: Response
    {
        
    }
}
