﻿using System;
using System.Collections.Generic;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Responses
{
    public class GetProjectsResponse: Response
    {
        public List<ProjectDto> Projects { get; set; }
    }
}
