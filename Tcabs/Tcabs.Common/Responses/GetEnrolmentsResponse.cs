﻿using System;
using System.Collections.Generic;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Responses
{
    public class GetEnrolmentsResponse: Response
    {
        public List<EnrolmentDto> Enrolments { get; set; }
    }
}
