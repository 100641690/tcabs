﻿using System;
using System.Collections.Generic;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Responses
{
    public class GetUnitOfferingsResponse: Response
    {
        public List<UnitOfferingDto> UnitOfferings { get; set; }
    }
}
