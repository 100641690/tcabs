﻿using System;
using System.Collections.Generic;
using Tcabs.Common.Dtos;

namespace Tcabs.Common.Responses
{
    public class GetEnrolmentsByUnitOfferingResponse : Response
    {
        public List<EnrolmentDto> Enrolments { get; set; }
        public List<EnrolmentDto> NotEnrolled { get; set; }
    }
}
