﻿using System;
using System.Collections.Generic;

namespace Tcabs.Common.Daos
{
    public class User
    {
        public uint UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
