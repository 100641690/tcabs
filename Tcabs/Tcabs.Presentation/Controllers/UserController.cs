﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tcabs.Logic;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Dtos;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Session;
using Tcabs.Common.Utilities;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace Tcabs.Presentation.Controllers
{
    [ApiController]
    [Route("user")]
    public class UserController : ControllerBase
    {
        /// <summary>
        /// Used to sign the user into the system.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("login/authenticate")]
        public async Task<IActionResult> AuthenticateUserAsync([FromBody] AuthenticateUserRequest request)
        {
            try
            {
                var service = new UserService();
                var response = service.AuthenticateUser(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:

                        var claims = new List<Claim>();

                        // Add the user details to the authetnication claim
                        claims.Add(new Claim(ClaimTypes.NameIdentifier, response.User.UserID));
                        claims.Add(new Claim(ClaimTypes.GivenName, response.User.FirstName));
                        claims.Add(new Claim(ClaimTypes.Surname, response.User.LastName));

                        // Add permissions to the authentication claim
                        foreach (PermissionDto p in response.Permissions)
                        {
                            claims.Add(new Claim(ClaimTypes.Role, p.Name));
                        }

                        var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                        var authProperties = new AuthenticationProperties
                        {
                            AllowRefresh = true,
                            // Refreshing the authentication session should be allowed.

                            //ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                            // The time at which the authentication ticket expires. A 
                            // value set here overrides the ExpireTimeSpan option of 
                            // CookieAuthenticationOptions set with AddCookie.

                            IsPersistent = true,
                            // Whether the authentication session is persisted across 
                            // multiple requests. When used with cookies, controls
                            // whether the cookie's lifetime is absolute (matching the
                            // lifetime of the authentication ticket) or session-based.

                            IssuedUtc = DateTime.Now,
                            // The time at which the authentication ticket was issued.

                            //RedirectUri = <string>
                            // The full path or absolute URI to be used as an http 
                            // redirect response value.
                        };

                        await HttpContext.SignInAsync(
                            CookieAuthenticationDefaults.AuthenticationScheme,
                            new ClaimsPrincipal(claimsIdentity),
                            authProperties
                        );
                        return Ok(response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        /// <summary>
        /// Checks permissions required for the current page against the permissions granted to the user.
        /// If 401 is returned, they aren't signed in; if 403 is returned they don't have permission.
        /// TODO: This method isn't currently verfying that the credentials the user signed in with are still active in the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("login/permitted")]
        public IActionResult CheckPermissions([FromBody] CheckPermissionsRequest request)
        {
            try
            {
                var response = new CheckPermissionsResponse();
                response.User = new UserDto();
                response.Permissions = new List<string>();

                if (HttpContext.User.Identity.IsAuthenticated)
                {
                    response.User.UserID = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                    response.User.FirstName = HttpContext.User.FindFirst(ClaimTypes.GivenName).Value;
                    response.User.LastName = HttpContext.User.FindFirst(ClaimTypes.Surname).Value;

                    var roleClaims = HttpContext.User.Claims.Where
                        (c => c.Type == ClaimTypes.Role).ToList();

                    foreach (Claim c in roleClaims)
                    {
                        response.Permissions.Add(c.Value);
                    }

                    if (request.Permissions.Any())
                    {
                        foreach (string p in request.Permissions)
                        {
                            if (HttpContext.User.IsInRole(p))
                            {
                                // User is signed in and has permission required by page.
                                return Ok(response);
                            }
                        }
                        // User is signed in but does not have permission to view the page.
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    }
                    // User is signed in and no permission is required to view the page.
                    return Ok(response);
                }
                // User is not signed in
                return StatusCode(StatusCodes.Status401Unauthorized, response);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Redirects the user to the website if they visit the login page while already logged in.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("login/check")]
        public IActionResult CheckAuthentication()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return Ok();
            }
            return Unauthorized();
        }

        /// <summary>
        /// Signs the user out of the system.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("logout/destroy")]
        public async Task<IActionResult> LogoutUserAsync()
        {
            try
            {
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Adds a user to the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Authorize(Roles = "Administrator")]
        public IActionResult AddUpdateUser([FromBody] AddUpdateUserRequest request)
        {
            try
            {
                var service = new UserService();
                var response = service.AddUpdateUser(request);
                
                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetUsers()
        {
            var request = new GetUsersRequest();

            try
            {
                var service = new UserService();
                var response = service.GetUsers(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        [Route("role/{name}")]
        public IActionResult GetUsersByRole([FromRoute] string name)
        {
            var request = new GetUsersByRoleRequest()
            {
                Name = name
            };

            try
            {
                var service = new UserService();
                var response = service.GetUsersByRole(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        [Route("permission/{name}")]
        public IActionResult GetUsersByPermission([FromRoute] string name)
        {
            var request = new GetUsersByPermissionRequest()
            {
                Name = name
            };

            try
            {
                var service = new UserService();
                var response = service.GetUsersByPermission(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize(Roles = "Administrator")]
        public IActionResult DeleteUser([FromRoute] string id)
        {
            var request = new DeleteUserRequest()
            {
                UserID = id
            };

            try
            {
                var service = new UserService();
                var response = service.DeleteUser(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}