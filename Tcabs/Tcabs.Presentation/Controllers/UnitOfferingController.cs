﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tcabs.Logic;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Dtos;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Session;
using Tcabs.Common.Utilities;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace Tcabs.Presentation.Controllers
{
    [ApiController]
    [Route("unitoffering")]
    public class UnitOfferingController : ControllerBase
    {
        /// <summary>
        /// Adds a unit offering to the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Authorize(Roles = "Administrator")]
        public IActionResult AddUpdateUnitOffering([FromBody] AddUpdateUnitOfferingRequest request)
        {
            try
            {
                var service = new UnitOfferingService();
                var response = service.AddUpdateUnitOffering(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetUnitOfferings()
        {
            var request = new GetUnitOfferingsRequest();

            try
            {
                var service = new UnitOfferingService();
                var response = service.GetUnitOfferings(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize(Roles = "Administrator")]
        public IActionResult DeleteUnitOffering([FromRoute] uint id)
        {
            var request = new DeleteUnitOfferingRequest()
            {
                UnitOfferingID = id
            };

            try
            {
                var service = new UnitOfferingService();
                var response = service.DeleteUnitOffering(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        [Route("periods")]
        public IActionResult GetTeachingPeriods()
        {
            var request = new GetTeachingPeriodsRequest();

            try
            {
                var service = new UnitOfferingService();
                var response = service.GetTeachingPeriods(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        [Route("years")]
        public IActionResult GetTeachingYears()
        {
            var request = new GetTeachingYearsRequest();

            try
            {
                var service = new UnitOfferingService();
                var response = service.GetTeachingYears(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}