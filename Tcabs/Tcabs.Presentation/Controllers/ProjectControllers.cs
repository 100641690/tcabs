using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tcabs.Logic;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Dtos;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Session;
using Tcabs.Common.Utilities;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace Tcabs.Presentation.Controllers
{
    //[ApiController]
    [Route("project")]
    public class ProjectControllers : ControllerBase
    {
        /// <summary>
        /// Adds a project to the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Authorize(Roles = "Administrator")]
        public IActionResult AddUpdateProject([FromBody] AddUpdateProjectRequest request)
        {
            try
            {
                var service = new ProjectService();
                var response = service.AddUpdateProject(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetProjects()
        {
            var request = new GetProjectsRequest();

            try
            {
                var service = new ProjectService();
                var response = service.GetProjects(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize(Roles = "Administrator")]
        public IActionResult DeleteProject([FromRoute] int id)
        {
            var request = new DeleteProjectRequest()
            {
                ProjectID = id
            };

            try
            {
                var service = new ProjectService();
                var response = service.DeleteProject(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}