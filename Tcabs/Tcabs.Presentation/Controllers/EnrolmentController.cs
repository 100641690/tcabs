﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tcabs.Logic;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Dtos;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Session;
using Tcabs.Common.Utilities;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace Tcabs.Presentation.Controllers
{
    [ApiController]
    [Route("enrolment")]
    public class EnrolmentController : ControllerBase
    {
        /// <summary>
        /// Adds and removes student enrolments in the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Authorize(Roles = "Administrator")]
        public IActionResult AddUpdateEnrolment([FromBody] AddUpdateEnrolmentsRequest request)
        {
            try
            {
                var service = new EnrolmentService();
                var response = service.AddUpdateEnrolments(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetEnrolments()
        {
            var request = new GetEnrolmentsRequest();

            try
            {
                var service = new EnrolmentService();
                var response = service.GetEnrolments(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        [Route("enrolments/{id}")]
        public IActionResult GetEnrolmentsByUnitOffering([FromRoute] string id)
        {
            var request = new GetEnrolmentsByUnitOfferingRequest()
            {
                UnitOfferingID = Convert.ToUInt32(id)
            };

            try
            {
                var service = new EnrolmentService();
                var response = service.GetEnrolmentsByUnitOffering(request);

                switch (response.Status)
                {
                    case HttpStatusCode.OK:
                        return Ok(response);
                    case HttpStatusCode.BadRequest:
                        return BadRequest(response);
                    case HttpStatusCode.Forbidden:
                        return StatusCode(StatusCodes.Status403Forbidden, response);
                    case HttpStatusCode.InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, response);
                }
            }
            catch
            {
                // Something unexpected happened...
                throw;
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}