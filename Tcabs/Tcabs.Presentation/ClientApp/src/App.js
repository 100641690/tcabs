import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Login } from './components/Login';
import { Home } from './components/Home';
import { User } from './components/User';
import { Unit } from './components/Unit';
import { Enrolment } from './components/Enrolment';
import { Team } from './components/Team';
import { Project } from './components/Project';
import { Task } from './components/Task';
import { Logout } from './components/Logout';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

    render () {
        return (
            <Layout>
                <Route exact path='/user/login' component={Login} />
                <Route exact path='/' component={Home} />
                <Route exact path='/user' component={User} />
                <Route exact path='/unit' component={Unit} />
                <Route exact path='/enrolment' component={Enrolment} />
                <Route exact path='/team' component={Team} />
                <Route exact path='/project' component={Project} />
                <Route exact path='/task' component={Task} />
                <Route exact path='/user/logout' component={Logout} />
            </Layout>
        );
    }
}
