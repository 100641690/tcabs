﻿import React, { Component } from 'react';
import Select from 'react-select';
import { authenticating, unauthorized, forbidden } from '../Utilities';
import { checkPermissions,getUsers, getUsersByPermission } from '../calls/UserCalls';
import { addUpdateTeam, getTeams, deleteTeam } from '../calls/TeamCalls';


export class Team extends Component {
    static displayName = Team.name;

    constructor(props) {
        super(props);
        this.state = {
            permissions: ["Administrator", "Convenor"],
            user: null,
            permitted: null,
            authenticated: null,
            selectUser: [{ label: "", value: { userID: "", password: "", firstName: "", lastName: "", role: "" }}],
            selectedUser: { label: "", value: { userID: "", password: "", firstName: "", lastName: "", role: "" }},            
            selectTeam: [{label:"", value:{teamID: "", supervisor:{userID:"", firstName: "", lastName: "", role:""}}}],
            selectedTeam: {label:"", value:{teamID: "", supervisor:{userID:"", firstName: "", lastName: "", role:""}}}, 
            selectedTeamID:null,
           teamMode: "add",
            selectedTeamName: null,
           selectSupervisor: [{
                label: "",
                value: {
                    userID: "",
                    firstName: "",
                    lastName: "",
                    role: ""
                }
            }],
            selectedSupervisor: {
                label: "",
                value: {
                    userID: "",
                    firstName: "",
                    lastName: "",
                    role: ""
                }
            }
        };
       // this.handleSelectTeamChange = this.handleSelectTeamChange.bind(this);
        this.handleSelectSupervisorChange = this.handleSelectSupervisorChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSubmitTeam = this.handleSubmitTeam.bind(this);
        this.addUpdateTeam = this.addUpdateTeam.bind(this);
        this.getTeams = this.getTeams.bind(this);
        this.handleTeamChange = this.handleTeamChange.bind(this);
    }

    componentDidMount() {
        // Startup; check that user has permission to view this page
        this.checkPermissions();
    }

    getUsers() {
        // Call getUsers from UserCalls.js
        getUsers()
            .then((response) => {
                // Iterate over user results and make them usable for react-select (label/value).
                var i;
                var userSelection = [];
                for(i in response.users) {
                    userSelection.push({
                        label: response.users[i].firstName + " " + response.users[i].lastName
                            + " (" + response.users[i].userID + ")",
                        value: {
                            userID: response.users[i].userID,
                            password: "",
                            firstName: response.users[i].firstName,
                            lastName: response.users[i].lastName,
                            role: response.users[i].role.name
                        }
                    });
                }

                // apply results from loop to selectUser state for use with react-select
                this.setState({
                    selectUser: userSelection
                });
            })
            .catch((error) => {
                console.error("Error getting users for selection: " + error.status);
            });
    }

    // User selected a mode radio button (Add, Edit, Remove)
    handleTeamModeChange = (event) => {
        

        console.log(event.target.value);
        this.setState({
            teamMode: event.target.value
        })
    }
    // User changed the selected option in the react-select
    handleSelectUserChange(event) {
        this.setState({
            selectedUser: {
                label: event.label,
                value: {
                    userID: event.value.userID,
                    password: "",
                    firstName: event.value.firstName,
                    lastName: event.value.lastName,
                    role: event.value.role
                }
            },
            userID: event.value.userID,
            firstName: event.value.firstName,
            lastName: event.value.lastName,
            role: event.value.role
        });
    }

    
        
    checkPermissions() {
        var authMessage = "Team page response from checkPermissions(): ";
        // Call checkPermissions from UserCalls.js
        checkPermissions(this.state.permissions)
            .then((response) => {

                // User authentication verified with required permissions
                console.log(authMessage + response.status)
                this.setState({
                    authenticated: true,
                    permitted: true,
                    user: response.user
                });

                // Get users to populate the react-select
                this.getUsersByPermission();
                this.getTeams();
            })
            .catch((error) => {
                console.error(authMessage + error.status);

                if (error.status === 401) {
                    // User is not authenticated
                    this.setState({
                        authenticated: false,
                        permitted: false,
                        user: error.user
                    });
                }

                if (error.status === 403) {
                    // User is authenticated but does not have required permissions
                    this.setState({
                        authenticated: true,
                        permitted: false
                    });
                }
            });
    }

    getUsersByPermission() {
        // Call getUsers from UserCalls.js
        getUsersByPermission("Supervisor")
            .then((response) => {
                // Iterate over user results and make them usable for react-select (label/value).
                var i;
                var userSelection = [];
                for (i in response.users) {
                    userSelection.push({
                        label: response.users[i].firstName + " " + response.users[i].lastName
                            + " (" + response.users[i].userID + ")",
                        value: {
                            userID: response.users[i].userID,
                            password: "",
                            firstName: response.users[i].firstName,
                            lastName: response.users[i].lastName,
                            role: response.users[i].role.name
                        }
                    });
                }

                // apply results from loop to selectUser state for use with react-select
                this.setState({
                    selectSupervisor: userSelection
                });
            })
            .catch((error) => {
                console.error("Error getting users for selection: " + error.status);
            });
    }

    // User changed the selected option in the react-select
    handleSelectSupervisorChange(event) {
        this.setState({
            selectedSupervisor: {
                label: event.label,
                value: {
                    userID: event.value.userID,
                    firstName: event.value.firstName,
                    lastName: event.value.lastName,
                    role: event.value.role
                }
            }
        });
    }

    // User typed something into Team Name text input
    handleNameChange(event) {
        this.setState({ TeamName: event.target.value });
    }

 // User changed the selected option in the Convenor react-select
 handleSelectSupervisorChange(event) {
    this.setState({
        selectedSupervisor: {
            label: event.label,
            value: {
                userID: event.value.userID,
                firstName: event.value.firstName,
                lastName: event.value.lastName,
                role: event.value.role
            }
        }
    });
}



addUpdateTeam() {
    var authMessage = "Team page response from addUpdateTeam(): ";
    var alertMessage = "";

    console.log(this.state.selectedSupervisor)

    // Check that all form values are set
    if (this.state.selectedSupervisor.value.userID === "" || this.state.name === "") {
        alertMessage = "Please enter Team ID, Supervisor and Name.";
        alert(alertMessage);
    }
    else {
        // Call addUpdateTean from UserCalls.js
        // this.state.teamMode === "edit" sets isUpdate to true in api call if the edit radio button is active
       
        if(this.state.teamMode === "edit")
        {
            var superVID = this.state.selectedSupervisor.value.userID;
            var name = this.state.selectedTeamName;
            var teamID = Number(this.state.selectedTeam.value.teamID);
            
        }
        else{
            var superVID = this.state.selectedSupervisor.value.userID;
            var name = this.state.TeamName;
            var teamID = 0;
        }     

        addUpdateTeam(this.state.teamMode === "edit", superVID, name, teamID)
            .then((response) => {
                // User add successful
                var s;
                for (s in response.statusMessages) {
                    alertMessage += response.statusMessages[s].message;
                    if (s < response.statusMessages.length - 1) {
                        alertMessage += "\n";
                    }
                }
                console.log(authMessage + alertMessage);
                alert(alertMessage);

                // Reset the form
                if (this.state.teamMode === "add") {
                    this.setState({
                        name: "",
                        selectedSupervisor: {
                            label: "",
                            value: {
                                userID: "",
                                firstName: "",
                                lastName: "",
                                role: ""
                            }
                        }
                    });
                }
                else {
                    // Update the react-select
                    this.setState({
                        selectedTeam: {
                            label: this.state.name + " " + " (" + this.state.teamID + ")",
                            value: {
                                teamID: this.state.teamID,
                                supervisor: this.state.selectedSupervisor,
                                name: this.state.name
                            }
                        }
                    });
                }
                this.getTeams();
            })
            .catch((error) => {
                // User add failed
                // Concatenate status messages into alertMessage
                var s;
                for (s in error.statusMessages) {
                    alertMessage += error.statusMessages[s].message;
                    if (s < error.statusMessages.length - 1) {
                        alertMessage += "\n";
                    }
                }
                console.log(authMessage + alertMessage);
                alert(alertMessage);
            });
    }
}



getTeams() {
    // Call getTeams from Teams.js
    console.log("Retreiving Team data ...")
    getTeams()
        .then((response) => {
            // Iterate over team results and make them usable for react-select (label/value).
            var i;
            var teamSelection = [];
            for (i in response.teams) {
                teamSelection.push({
                    label: response.teams[i].name + " (" + response.teams[i].teamID + ")",
                    value: {
                        teamID: response.teams[i].teamID,
                        supervisor: {
                            userID: response.teams[i].supervisor.userID,
                            firstName: response.teams[i].supervisor.firstName,
                            lastName: response.teams[i].supervisor.lastName,
                            role: response.teams[i].supervisor.role.name
                        },
                        name: response.teams[i].name
                    }
                });
            }
            console.log("teamSel", teamSelection);

            // apply results from loop to selectTeam state for use with react-select
            this.setState({
                selectTeam: teamSelection
            });
            console.log("Teams state updated");
        })
        .catch((error) => {
            console.error("Error getting teams for selection: " + error.status);
        });
}

getUsersByPermission() {
    // Call getUsers from UserCalls.js
    getUsersByPermission("Supervisor")
        .then((response) => {
            // Iterate over user results and make them usable for react-select (label/value).
            var i;
            var userSelection = [];
            for (i in response.users) {
                userSelection.push({
                    label: response.users[i].firstName + " " + response.users[i].lastName
                        + " (" + response.users[i].userID + ")",
                    value: {
                        userID: response.users[i].userID,
                        password: "",
                        firstName: response.users[i].firstName,
                        lastName: response.users[i].lastName,
                        role: response.users[i].role.name
                    }
                });
            }

            // apply results from loop to selectUser state for use with react-select
            this.setState({
                selectSupervisor: userSelection
            });
        })
        .catch((error) => {
            console.error("Error getting users for selection: " + error.status);
        });
}

deleteTeam() {
    var authMessage = "Team page response from deleteTeam(): ";
    var alertMessage = "";

    // Call deleteTeam from TeamCalls.js
    deleteTeam(this.state.selectedTeam.value.teamID)
        .then((response) => {
            // Team deletion successful
            var s;
            for (s in response.statusMessages) {
                alertMessage += response.statusMessages[s].message;
                if (s < response.statusMessages.length - 1) {
                    alertMessage += "\n";
                }
            }
            console.log(authMessage + alertMessage);
            alert(alertMessage);

            // Clear react-select
            this.setState({
                selectedTeam: {
                    label: "",
                    value: {
                        teamID: "",
                        name: "",
                        Supervisor: {
                            userID: "",
                            firstName: "",
                            lastName: "",
                            role: ""
                        }
                    }
                }
            });
            this.getTeams();
        })
        .catch((error) => {
            // User add failed
            // Concatenate status messages into alertMessage
            var s;
            for (s in error.statusMessages) {
                alertMessage += error.statusMessages[s].message;
                if (s < error.statusMessages.length - 1) {
                    alertMessage += "\n";
                }
            }
            console.log(authMessage + alertMessage);
            alert(alertMessage);
        });
}



    handleSubmitTeam(event) {
        event.preventDefault();

        // Dump all the form values into the console
        // console.log("TeamId: '" + this.state.TeamId + "'");
        if(this.state.teamMode === 'add' || this.state.teamMode === 'edit'){
            console.log("TeamName: '" + this.state.selectedTeamName + "'");
            this.addUpdateTeam();
        }else{
            console.log(this.state.selectedTeam);
            this.deleteTeam();
        }
        

    }

    //selectedTeam: {label:"", value:{teamID: "", supervisor:{userID:"", firstName: "", lastName: "", role:""}}}, 
    handleTeamChange(event){
        this.setState({
            selectedTeam: {
                label: event.label,
                value: {
                    teamID:event.value.teamID,
                    supervisor:{
                        userID: event.value.userID,
                        firstName: event.value.firstName,
                        lastName: event.value.lastName,
                        role: event.value.role
                    }                   
                }
            },            
        });
        this.setState({
            selectedTeamName: event.label.split("(")[0].trim()

        });
    }

    handleTeamChangeInput(event){
        this.setState({
            selectedTeamName: event.target.value
        })
    }

    

    render() {

        //var teamName = this.state.selectedTeam.label !== "" ? this.state.selectedTeam.label.split("(")[0] : null;
        if (this.state.authenticated === null) {
            // Waiting for authentication to complete
            return authenticating();
        }

        if (this.state.authenticated === false) {
            // Authentication check completed and user is not logged in.
            // Send them back to the login page.
            console.log("Team page: Redirecting to Login page.");
            return unauthorized();
        }

        if (this.state.permitted === false) {
            // User is authenticated but doesn't have required permissions to view page.
            // This shouldn't happen if menu links applicable to the user are displayed.
            return forbidden();
        }

        return (
            <div>
                <div className="top-div">
                    <h1>Manage Teams</h1>
                    <p>Add and edit teams here. Allocate projects and supervisors to teams.</p>
                </div>
                <div className="left-div">
                <form onSubmit={this.handleSubmitTeam}>
                <label>
                            <input type="radio" value="add" name="teamMode" onChange={this.handleTeamModeChange} defaultChecked />
                            Add
                        </label>
                        <label>
                            <input type="radio" value="edit" name="teamMode" onChange={this.handleTeamModeChange} />
                            Edit
                        </label>
                        <label>
                            <input type="radio" value="remove" name="teamMode" onChange={this.handleTeamModeChange} />
                            Remove
                        </label>
                        {this.state.teamMode === "add" ? (<>
                            <br /> <input type="text" value={this.state.TeamName || ""}
                        placeholder={"Team Name"}
                        onChange={this.handleNameChange} /> 
                        <br /><Select className={"react-select"} options={this.state.selectSupervisor}
                            placeholder={"Select Supervisor..."}
                            onChange={this.handleSelectSupervisorChange}
                            value={this.state.selectedSupervisor.label !== ""
                                ? { label: this.state.selectedSupervisor.label, value: this.state.selectedSupervisor.value }
                                : null} /> 
                                <input type="submit" value="Add Team"/>
                        </>) : (<></>)}

                         {this.state.teamMode === "edit" ? (<>
                           

                           <Select className={"react-select"} options={this.state.selectTeam}
                            placeholder={"Select Teams..."} 
                             onChange={this.handleTeamChange}
                            value={this.state.selectedTeam.label !== ""
                                ? { label: this.state.selectedTeam.label, value: this.state.selectedTeam.value }
                                : null} />  

                        <br />
                        {this.state.selectedTeam.label !== "" ? (<>                        
                        <input type="text" value={this.state.selectedTeam.label || ""}
                                    placeholder={"Team ID"}
                                    onChange={this.handleTeamChange}
                                    readOnly={this.state.teamMode === "edit" ? true : null} /> 
                                    <input type="text" onChange={this.handleTeamChangeInput.bind(this)} value={this.state.selectedTeamName}/>
                            <Select className={"react-select"} options={this.state.selectSupervisor}
                            placeholder={"Select Supervisor..."}
                            onChange={this.handleSelectSupervisorChange}
                            value={this.state.selectedSupervisor.label !== ""
                                ? { label: this.state.selectedSupervisor.label, value: this.state.selectedSupervisor.value }
                                : null} /> 
                                <input type="submit" value="Edit Team"/>
                        </>) : (<></>)}
                        
                                
                        </>) : (<></>)} 

                        {this.state.teamMode === "remove" ? (<>
                        
                            <Select className={"react-select"} options={this.state.selectTeam}
                            placeholder={"Select Teams..."} 
                             onChange={this.handleTeamChange}
                            value={this.state.selectedTeam.label !== ""
                                ? { label: this.state.selectedTeam.label, value: this.state.selectedTeam.value }
                                : null} />  
                            <input type="submit" value="Remove Team"/>
                        
                        </>) : (<></>)}
                    
                </form>
                </div>

            </div>
        );
    }
}
