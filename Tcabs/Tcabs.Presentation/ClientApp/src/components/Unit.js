﻿import React, { Component } from 'react';
import Select from 'react-select';
import { authenticating, unauthorized, forbidden } from '../Utilities';
import { checkPermissions, getUsersByPermission } from '../calls/UserCalls';
import {
    addUpdateUnit,
    getUnits,
    deleteUnit,
    addUpdateUnitOffering,
    getUnitOfferings,
    deleteUnitOffering,
    getTeachingPeriods,
    getTeachingYears
} from '../calls/UnitCalls';

export class Unit extends Component {
    static displayName = Unit.name;

    constructor(props) {
        super(props);
        this.state = {
            permissions: ["Administrator"],
            user: null,
            permitted: null,
            authenticated: null,

            unitMode: "add",
            selectUnit: [{
                label: "",
                value: {
                    unitCode: "",
                    name: "",
                    convenor: {
                        userID: "",
                        firstName: "",
                        lastName: "",
                        role: ""
                    }
                }
            }],
            selectedUnit: {
                label: "",
                value: {
                    unitCode: "",
                    name: "",
                    convenor: {
                        userID: "",
                        firstName: "",
                        lastName: "",
                        role: ""
                    }
                }
            },
            unitCode: "",
            name: "",
            selectConvenor: [{
                label: "",
                value: {
                    userID: "",
                    firstName: "",
                    lastName: "",
                    role: ""
                }
            }],
            selectedConvenor: {
                label: "",
                value: {
                    userID: "",
                    firstName: "",
                    lastName: "",
                    role: ""
                }
            },

            unitOfferingMode: "add",
            selectUnitOffering: [{
                label: "",
                value: {
                    unitOfferingID: "",
                    unit: {
                        unitCode: "",
                        convenor: {
                            userID: "",
                            firstName: "",
                            lastName: "",
                            role: ""
                        },
                        name: "",
                    },
                    teachingPeriod: {
                        teachingPeriodID: "",
                        name: ""
                    },
                    teachingYear: {
                        year: ""
                    }
                }
            }],
            selectedUnitOffering: {
                label: "",
                value: {
                    unitOfferingID: "",
                    unit: {
                        unitCode: "",
                        convenor: {
                            userID: "",
                            firstName: "",
                            lastName: "",
                            role: ""
                        },
                        name: "",
                    },
                    teachingPeriod: {
                        teachingPeriodID: "",
                        name: ""
                    },
                    teachingYear: {
                        year: ""
                    }
                }
            },
            selectUnitOfferingUnit: [{
                label: "",
                value: {
                    unitCode: "",
                    name: "",
                    convenor: {
                        userID: "",
                        firstName: "",
                        lastName: "",
                        role: ""
                    }
                }
            }],
            selectedUnitOfferingUnit: {
                label: "",
                value: {
                    unitCode: "",
                    name: "",
                    convenor: {
                        userID: "",
                        firstName: "",
                        lastName: "",
                        role: ""
                    }
                }
            },
            selectTeachingPeriod: [{
                label: "",
                value: {
                    teachingPeriodID: "",
                    name: ""
                }
            }],
            selectedTeachingPeriod: {
                label: "",
                value: {
                    teachingPeriodID: "",
                    name: ""
                }
            },
            selectTeachingYear: [{
                label: "",
                value: {
                    year: ""
                }
            }],
            selectedTeachingYear: {
                label: "",
                value: {
                    year: ""
                }
            }
        };

        this.handleSelectUnitChange = this.handleSelectUnitChange.bind(this);
        this.handleUnitCodeChange = this.handleUnitCodeChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSelectConvenorChange = this.handleSelectConvenorChange.bind(this);
        this.handleSubmitUnit = this.handleSubmitUnit.bind(this);

        this.handleSelectUnitOfferingChange = this.handleSelectUnitOfferingChange.bind(this);
        this.handleSelectUnitOfferingUnitChange = this.handleSelectUnitOfferingUnitChange.bind(this);
        this.handleSelectTeachingPeriodChange = this.handleSelectTeachingPeriodChange.bind(this);
        this.handleSelectTeachingYearChange = this.handleSelectTeachingYearChange.bind(this);
        this.handleSubmitUnitOffering = this.handleSubmitUnitOffering.bind(this);
    }

    componentDidMount() {
        // Startup; check that user has permission to view this page
        this.checkPermissions();
    }

    handleUnitModeChange = (event) => {
        if (event.target.value === "add") {
            this.setState({
                unitMode: event.target.value,
                selectedUnit: {
                    label: "",
                    value: {
                        unitCode: "",
                        name: "",
                        convenor: {
                            userID: "",
                            firstName: "",
                            lastName: "",
                            role: ""
                        }
                    }
                },
                unitCode: "",
                name: "",
                selectedConvenor: {
                    label: "",
                    value: {
                        userID: "",
                        firstName: "",
                        lastName: "",
                        role: ""
                    }
                }
            })
        }
        else {
            this.setState({
                unitMode: event.target.value,
                unitCode: this.state.selectedUnit.value.unitCode,
                name: this.state.selectedUnit.value.name,
                selectedConvenor: this.state.selectedConvenor
            })
        }
    }

    handleUnitOfferingModeChange = (event) => {
        if (event.target.value === "add") {
            this.setState({
                unitOfferingMode: event.target.value,
                selectedUnitOffering: {
                    label: "",
                    value: {
                        unitOfferingID: "",
                        unit: {
                            unitCode: "",
                            convenor: {
                                userID: "",
                                firstName: "",
                                lastName: "",
                                role: ""
                            },
                            name: "",
                        },
                        teachingPeriod: {
                            teachingPeriodID: "",
                            name: ""
                        },
                        teachingYear: {
                            year: ""
                        }
                    }
                },
                selectedUnitOfferingUnit: {
                    label: "",
                    value: {
                        unitCode: "",
                        name: "",
                        convenor: {
                            userID: "",
                            firstName: "",
                            lastName: "",
                            role: ""
                        }
                    }
                },
                selectedTeachingPeriod: {
                    label: "",
                    value: {
                        teachingPeriodID: "",
                        name: ""
                    }
                },
                selectedTeachingYear: {
                    label: "",
                    value: {
                        year: ""
                    }
                }
            })
        }
        else {
            this.setState({
                unitOfferingMode: event.target.value
            })
        }
    }

    // User changed the selected option in the Unit react-select
    handleSelectUnitChange(event) {
        this.setState({
            selectedUnit: {
                label: event.label,
                value: {
                    unitCode: event.value.unitCode,
                    name: event.value.name,
                    convenor: event.value.convenor
                }
            },
            unitCode: event.value.unitCode,
            name: event.value.name,
            selectedConvenor: {
                label: event.value.convenor.firstName + " " + event.value.convenor.lastName
                    + " (" + event.value.convenor.userID + ")",
                value: {
                    userID: event.value.convenor.userID,
                    firstName: event.value.convenor.firstName,
                    lastName: event.value.convenor.lastName,
                    role: event.value.convenor.role
                }
            }
        });
    }

    handleSelectUnitOfferingChange(event) {
        this.setState({
            selectedUnitOffering: {
                label: event.label,
                value: {
                    unitOfferingID: event.value.unitOfferingID,
                    unit: {
                        unitCode: event.value.unit.unitCode,
                        convenor: {
                            userID: "",
                            firstName: "",
                            lastName: "",
                            role: ""
                        },
                        name: event.value.unit.name,
                    },
                    teachingPeriod: {
                        teachingPeriodID: event.value.teachingPeriod.teachingPeriodID,
                        name: event.value.teachingPeriod.name
                    },
                    teachingYear: {
                        year: event.value.teachingYear.year
                    }
                }
            },
            selectedUnitOfferingUnit: {
                label: event.value.unit.name + " (" + event.value.unit.unitCode + ")",
                value: {
                    unitCode: event.value.unit.unitCode,
                    name: event.value.unit.name,
                    convenor: {
                        userID: "",
                        firstName: "",
                        lastName: "",
                        role: ""
                    }
                }
            },
            selectedTeachingPeriod: {
                label: event.value.teachingPeriod.name,
                value: {
                    teachingPeriodID: event.value.teachingPeriod.teachingPeriodID,
                    name: event.value.teachingPeriod.name
                }
            },
            selectedTeachingYear: {
                label: event.value.teachingYear.year,
                value: {
                    year: event.value.teachingYear.year
                }
            }
        });
    }

    handleSelectUnitOfferingUnitChange(event) {
        this.setState({
            selectedUnitOfferingUnit: {
                label: event.label,
                value: {
                    unitCode: event.value.unitCode,
                    name: event.value.name
                }
            }
        });
    }

    handleSelectTeachingPeriodChange(event) {
        this.setState({
            selectedTeachingPeriod: {
                label: event.value.name,
                value: {
                    teachingPeriodID: event.value.teachingPeriodID,
                    name: event.value.name
                }
            }
        });
    }

    handleSelectTeachingYearChange(event) {
        this.setState({
            selectedTeachingYear: {
                label: event.value.year,
                value: {
                    year: event.value.year
                }
            }
        });
    }

    // User typed something into Unit ID text input
    handleUnitCodeChange(event) {
        this.setState({ unitCode: event.target.value });
    }

    // User typed something into Unit Name text input
    handleNameChange(event) {
        this.setState({ name: event.target.value });
    }

    // User changed the selected option in the Convenor react-select
    handleSelectConvenorChange(event) {
        this.setState({
            selectedConvenor: {
                label: event.label,
                value: {
                    userID: event.value.userID,
                    firstName: event.value.firstName,
                    lastName: event.value.lastName,
                    role: event.value.role
                }
            }
        });
    }

    addUpdateUnit() {
        var authMessage = "Unit page response from addUpdateUnit(): ";
        var alertMessage = "";

        // Check that all form values are set
        if (this.state.unitCode === "" || this.state.selectedConvenor.value.userID === "" || this.state.name === "") {
            alertMessage = "Please enter Unit Code, Convenor and Unit Name.";
            alert(alertMessage);
        }
        else {
            // Call addUpdateUnit from UserCalls.js
            // this.state.unitMode === "edit" sets isUpdate to true in api call if the edit radio button is active
            addUpdateUnit(this.state.unitMode === "edit", this.state.unitCode, this.state.selectedConvenor.value.userID, this.state.name)
                .then((response) => {
                    // User add successful
                    var s;
                    for (s in response.statusMessages) {
                        alertMessage += response.statusMessages[s].message;
                        if (s < response.statusMessages.length - 1) {
                            alertMessage += "\n";
                        }
                    }
                    console.log(authMessage + alertMessage);
                    alert(alertMessage);

                    // Reset the form
                    if (this.state.unitMode === "add") {
                        this.setState({
                            unitCode: "",
                            name: "",
                            selectedConvenor: {
                                label: "",
                                value: {
                                    userID: "",
                                    firstName: "",
                                    lastName: "",
                                    role: ""
                                }
                            }
                        });
                    }
                    else {
                        // Update the react-select
                        this.setState({
                            selectedUnit: {
                                label: this.state.name + " (" + this.state.unitCode + ")",
                                value: {
                                    unitCode: this.state.unitCode,
                                    convenor: this.state.selectedConvenor,
                                    name: this.state.name
                                }
                            }
                        });
                    }
                    this.getUnits();
                })
                .catch((error) => {
                    // User add/update failed
                    // Concatenate status messages into alertMessage
                    var s;
                    for (s in error.statusMessages) {
                        alertMessage += error.statusMessages[s].message;
                        if (s < error.statusMessages.length - 1) {
                            alertMessage += "\n";
                        }
                    }
                    console.error(authMessage + alertMessage);
                    alert(alertMessage);
                });
        }
    }

    addUpdateUnitOffering() {
        var authMessage = "Unit page response from addUpdateUnitOffering(): ";
        var alertMessage = "";

        // Check that all form values are set
        if (this.state.selectedUnitOfferingUnit.value.unitCode === ""
            || this.state.selectedTeachingPeriod.value.teachingPeriodID === ""
            || this.state.selectedTeachingYear.value.year === "") {
            alertMessage = "Please enter Unit Code, Teaching Period and Teaching Year.";
            alert(alertMessage);
        }
        else {
            // Call addUpdateUnitOffering from UserCalls.js
            // this.state.unitMode === "edit" sets isUpdate to true in api call if the edit radio button is active
            addUpdateUnitOffering(this.state.selectedUnitOffering.value.unitOfferingID,
                this.state.selectedUnitOfferingUnit.value.unitCode,
                this.state.selectedTeachingPeriod.value.teachingPeriodID,
                this.state.selectedTeachingYear.value.year)
                .then((response) => {
                    // User Offering add/update successful
                    var s;
                    for (s in response.statusMessages) {
                        alertMessage += response.statusMessages[s].message;
                        if (s < response.statusMessages.length - 1) {
                            alertMessage += "\n";
                        }
                    }
                    console.log(authMessage + alertMessage);
                    alert(alertMessage);

                    // Reset the form
                    if (this.state.unitOfferingMode === "add") {
                        this.setState({
                            selectedUnitOffering: {
                                label: "",
                                value: {
                                    unitOfferingID: "",
                                    unit: {
                                        unitCode: "",
                                        convenor: {
                                            userID: "",
                                            firstName: "",
                                            lastName: "",
                                            role: ""
                                        },
                                        name: "",
                                    },
                                    teachingPeriod: {
                                        teachingPeriodID: "",
                                        name: ""
                                    },
                                    teachingYear: {
                                        year: ""
                                    }
                                }
                            },
                            selectedUnitOfferingUnit: {
                                label: "",
                                value: {
                                    unitCode: "",
                                    name: "",
                                    convenor: {
                                        userID: "",
                                        firstName: "",
                                        lastName: "",
                                        role: ""
                                    }
                                }
                            },
                            selectedTeachingPeriod: {
                                label: "",
                                value: {
                                    teachingPeriodID: "",
                                    name: ""
                                }
                            },
                            selectedTeachingYear: {
                                label: "",
                                value: {
                                    year: ""
                                }
                            }
                        });
                    }
                    else {
                        // Update the react-select
                        this.setState({
                            selectedUnitOffering: {
                                label: this.state.selectedUnitOfferingUnit.value.name
                                    + " (" + this.state.selectedUnitOfferingUnit.value.unitCode + ") "
                                    + this.state.selectedTeachingPeriod.value.name + ", "
                                    + this.state.selectedTeachingYear.value.year,
                                value: {
                                    unitOfferingID: this.state.selectedUnitOffering.value.unitOfferingID,
                                    unit: {
                                        unitCode: this.state.selectedUnitOfferingUnit.value.unitCode
                                    },
                                    teachingPeriod: {
                                        teachingPeriodID: this.state.selectedTeachingPeriod.value.teachingPeriodID
                                    },
                                    teachingYear: {
                                        year: this.state.selectedUnitOfferingUnit.value.year
                                    }
                                }
                            }
                        });
                    }
                    this.getUnitOfferings();
                })
                .catch((error) => {
                    // User Offering add/update failed
                    // Concatenate status messages into alertMessage
                    var s;
                    for (s in error.statusMessages) {
                        alertMessage += error.statusMessages[s].message;
                        if (s < error.statusMessages.length - 1) {
                            alertMessage += "\n";
                        }
                    }
                    console.error(authMessage + alertMessage);
                    alert(alertMessage);
                });
        }
    }

    getUnits() {
        // Call getUnits from UnitCalls.js
        getUnits()
            .then((response) => {
                // Iterate over unit results and make them usable for react-select (label/value).
                var i;
                var unitSelection = [];
                for (i in response.units) {
                    unitSelection.push({
                        label: response.units[i].name + " (" + response.units[i].unitCode + ")",
                        value: {
                            unitCode: response.units[i].unitCode,
                            convenor: {
                                userID: response.units[i].convenor.userID,
                                firstName: response.units[i].convenor.firstName,
                                lastName: response.units[i].convenor.lastName,
                                role: response.units[i].convenor.role.name
                            },
                            name: response.units[i].name
                        }
                    });
                }

                // apply results from loop to selectUnit state for use with react-select
                this.setState({
                    selectUnit: unitSelection,
                    selectUnitOfferingUnit: unitSelection
                });
            })
            .catch((error) => {
                console.error("Error getting units for selection: " + error.status);
            });
    }

    getUnitOfferings() {
        // Call getUnitOfferings from UnitCalls.js
        getUnitOfferings()
            .then((response) => {
                // Iterate over unit offering results and make them usable for react-select (label/value).
                var i;
                var unitOfferingSelection = [];
                for (i in response.unitOfferings) {
                    unitOfferingSelection.push({
                        label: response.unitOfferings[i].unit.name
                            + " (" + response.unitOfferings[i].unit.unitCode + ") "
                            + response.unitOfferings[i].teachingPeriod.name + ", "
                            + response.unitOfferings[i].teachingYear.year,
                        value: {
                            unitOfferingID: response.unitOfferings[i].unitOfferingID,
                            unit: {
                                unitCode: response.unitOfferings[i].unit.unitCode,
                                convenor: {
                                    userID: "",
                                    firstName: "",
                                    lastName: "",
                                    role: ""
                                },
                                name: response.unitOfferings[i].unit.name
                            },
                            teachingPeriod: {
                                teachingPeriodID: response.unitOfferings[i].teachingPeriod.teachingPeriodID,
                                name: response.unitOfferings[i].teachingPeriod.name
                            },
                            teachingYear: {
                                year: response.unitOfferings[i].teachingYear.year
                            }
                        }
                    });
                }

                // apply results from loop to selectUser state for use with react-select
                this.setState({
                    selectUnitOffering: unitOfferingSelection
                });
            })
            .catch((error) => {
                console.error("Error getting unit offerings for selection: " + error.status);
            });
    }

    getTeachingPeriods() {
        // Call getYears from UnitCalls.js
        getTeachingPeriods()
            .then((response) => {
                // Iterate over year results and make them usable for react-select (label/value).
                var i;
                var teachingPeriodSelection = [];
                for (i in response.teachingPeriods) {
                    teachingPeriodSelection.push({
                        label: response.teachingPeriods[i].name,
                        value: {
                            teachingPeriodID: response.teachingPeriods[i].teachingPeriodID,
                            name: response.teachingPeriods[i].name
                        }
                    });
                }

                // apply results from loop to selectUser state for use with react-select
                this.setState({
                    selectTeachingPeriod: teachingPeriodSelection
                });
            })
            .catch((error) => {
                console.error("Error getting units for selection: " + error.status);
            });
    }

    getTeachingYears() {
        // Call getYears from UnitCalls.js
        getTeachingYears()
            .then((response) => {
                // Iterate over year results and make them usable for react-select (label/value).
                var i;
                var teachingYearSelection = [];
                for (i in response.teachingYears) {
                    teachingYearSelection.push({
                        label: response.teachingYears[i].year,
                        value: {
                            year: response.teachingYears[i].year
                        }
                    });
                }

                // apply results from loop to selectUser state for use with react-select
                this.setState({
                    selectTeachingYear: teachingYearSelection
                });
            })
            .catch((error) => {
                console.error("Error getting units for selection: " + error.status);
            });
    }

    getUsersByPermission() {
        // Call getUsers from UserCalls.js
        getUsersByPermission("Convenor")
            .then((response) => {
                // Iterate over user results and make them usable for react-select (label/value).
                var i;
                var userSelection = [];
                for (i in response.users) {
                    userSelection.push({
                        label: response.users[i].firstName + " " + response.users[i].lastName
                            + " (" + response.users[i].userID + ")",
                        value: {
                            userID: response.users[i].userID,
                            password: "",
                            firstName: response.users[i].firstName,
                            lastName: response.users[i].lastName,
                            role: response.users[i].role.name
                        }
                    });
                }

                // apply results from loop to selectUser state for use with react-select
                this.setState({
                    selectConvenor: userSelection
                });
            })
            .catch((error) => {
                console.error("Error getting users for selection: " + error.status);
            });
    }

    deleteUnit() {
        var authMessage = "Unit page response from deleteUnit(): ";
        var alertMessage = "";

        // Call deleteUnit from UnitCalls.js
        deleteUnit(this.state.unitCode)
            .then((response) => {
                // Unit deletion successful
                var s;
                for (s in response.statusMessages) {
                    alertMessage += response.statusMessages[s].message;
                    if (s < response.statusMessages.length - 1) {
                        alertMessage += "\n";
                    }
                }
                console.log(authMessage + alertMessage);
                alert(alertMessage);

                // Clear react-select
                this.setState({
                    selectedUnit: {
                        label: "",
                        value: {
                            unitCode: "",
                            name: "",
                            convenor: {
                                userID: "",
                                firstName: "",
                                lastName: "",
                                role: ""
                            }
                        }
                    }
                });
                this.getUnits();
            })
            .catch((error) => {
                // Unit add/update failed
                // Concatenate status messages into alertMessage
                var s;
                for (s in error.statusMessages) {
                    alertMessage += error.statusMessages[s].message;
                    if (s < error.statusMessages.length - 1) {
                        alertMessage += "\n";
                    }
                }
                console.log(authMessage + alertMessage);
                alert(alertMessage);
            });
    }

    deleteUnitOffering() {
        var authMessage = "Unit page response from deleteUnitOffering(): ";
        var alertMessage = "";

        // Call deleteUnit from UnitCalls.js
        deleteUnitOffering(this.state.selectedUnitOffering.value.unitOfferingID)
            .then((response) => {
                // Unit deletion successful
                var s;
                for (s in response.statusMessages) {
                    alertMessage += response.statusMessages[s].message;
                    if (s < response.statusMessages.length - 1) {
                        alertMessage += "\n";
                    }
                }
                console.log(authMessage + alertMessage);
                alert(alertMessage);

                // Clear react-select
                this.setState({
                    selectedUnitOffering: {
                        label: "",
                        value: {
                            unitOfferingID: "",
                            unit: {
                                unitCode: "",
                                convenor: {
                                    userID: "",
                                    firstName: "",
                                    lastName: "",
                                    role: ""
                                },
                                name: "",
                            },
                            teachingPeriod: {
                                teachingPeriodID: "",
                                name: ""
                            },
                            teachingYear: {
                                year: ""
                            }
                        }
                    },
                    selectedUnitOfferingUnit: {
                        label: "",
                        value: {
                            unitCode: "",
                            name: "",
                            convenor: {
                                userID: "",
                                firstName: "",
                                lastName: "",
                                role: ""
                            }
                        }
                    },
                    selectedTeachingPeriod: {
                        label: "",
                        value: {
                            teachingPeriodID: "",
                            name: ""
                        }
                    },
                    selectedTeachingYear: {
                        label: "",
                        value: {
                            year: ""
                        }
                    }
                });
                this.getUnitOfferings();
            })
            .catch((error) => {
                // Unit offering delete failed
                // Concatenate status messages into alertMessage
                var s;
                for (s in error.statusMessages) {
                    alertMessage += error.statusMessages[s].message;
                    if (s < error.statusMessages.length - 1) {
                        alertMessage += "\n";
                    }
                }
                console.log(authMessage + alertMessage);
                alert(alertMessage);
            });
    }

    handleSubmitUnit(event) {
        event.preventDefault();
        // Dump all the form values into the console
        console.log("isUpdate: " + (this.state.unitMode === "edit"));
        console.log("unitCode: '" + this.state.unitCode + "'");
        console.log("convenor: '" + this.state.selectedConvenor.value.userID + "'");
        console.log("name: '" + this.state.name + "'");

        // Submit pressed while radio button set to add or edit options
        if (this.state.unitMode === "add" || this.state.unitMode === "edit") {
            console.log("Add/Edit unit pressed");
            this.addUpdateUnit();
        }

        // Submit pressed while radio button set to remove option
        if (this.state.unitMode === "remove") {
            console.log("Remove unit pressed");
            this.deleteUnit();
        }
    }

    handleSubmitUnitOffering(event) {
        event.preventDefault();
        // Dump all the form values into the console
        console.log("unitOfferingID: '" + this.state.selectedUnitOffering.value.unitOfferingID + "'");
        console.log("unitCode: '" + this.state.selectedUnitOfferingUnit.value.unitCode + "'");
        console.log("teachingPeriod: '" + this.state.selectedTeachingPeriod.value.teachingPeriodID + "'");
        console.log("teachingYear: '" + this.state.selectedTeachingYear.value.year + "'");

        // Submit pressed while radio button set to add or edit options
        if (this.state.unitOfferingMode === "add" || this.state.unitOfferingMode === "edit") {
            console.log("Add/Edit unit pressed");
            this.addUpdateUnitOffering();
        }

        // Submit pressed while radio button set to remove option
        if (this.state.unitOfferingMode === "remove") {
            console.log("Remove unit pressed");
            this.deleteUnitOffering();
        }
    }

    checkPermissions() {
        var authMessage = "Unit page response from checkPermissions(): ";
        // Call checkPermissions from UserCalls.js
        checkPermissions(this.state.permissions)
            .then((response) => {
                // User authentication verified with required permissions
                console.log(authMessage + response.status)
                this.setState({
                    authenticated: true,
                    permitted: true,
                    user: response.user
                });

                // Get convenors and units to populate the react-selects
                this.getUnits();
                this.getUsersByPermission();
                this.getUnitOfferings();
                this.getTeachingPeriods();
                this.getTeachingYears();
            })
            .catch((error) => {
                console.error(authMessage + error.status);
                if (error.status === 401) {
                    // User is not authenticated
                    this.setState({
                        authenticated: false,
                        permitted: false,
                        user: error.user
                    });
                }

                if (error.status === 403) {
                    // User is authenticated but does not have required permissions
                    this.setState({
                        authenticated: true,
                        permitted: false
                    });
                }
            });
    }

    render() {

        if (this.state.authenticated === null) {
            // Waiting for authentication to complete
            return authenticating();
        }

        if (this.state.authenticated === false) {
            // Authentication check completed and user is not logged in.
            // Send them back to the login page.
            console.log("Unit page: Redirecting to Login page.");
            return unauthorized();
        }

        if (this.state.permitted === false) {
            // User is authenticated but doesn't have required permissions to view page.
            // This shouldn't happen if menu links applicable to the user are displayed.
            return forbidden();
        }

        return (
            <div>
                <div className="top-div">
                    <h1>Manage Units</h1>
                    <p>Add, edit, update units and unit offerings here.</p>
                </div>
                <div className="left-div">
                    <h2>Units</h2>
                    <form onSubmit={this.handleSubmitUnit}>
                        <label>
                            <input type="radio" value="add" name="unitMode" onChange={this.handleUnitModeChange} defaultChecked />
                            Add
                        </label>
                        <label>
                            <input type="radio" value="edit" name="unitMode" onChange={this.handleUnitModeChange} />
                            Edit
                        </label>
                        <label>
                            <input type="radio" value="remove" name="unitMode" onChange={this.handleUnitModeChange} />
                            Remove
                        </label>
                        {this.state.unitMode !== "add"
                            ? <Select className={"react-select"} options={this.state.selectUnit}
                                onChange={this.handleSelectUnitChange}
                                value={this.state.selectedUnit.label !== ""
                                    ? { label: this.state.selectedUnit.label, value: this.state.selectedUnit.value }
                                    : null} />
                            : null
                        }
                        {this.state.unitMode === "add" || (this.state.unitMode === "edit" && this.state.selectedUnit.label !== "")
                            ?
                            <div>
                                <input type="text" value={this.state.unitCode || ""}
                                    placeholder={"Unit ID"}
                                    onChange={this.handleUnitCodeChange}
                                    readOnly={this.state.unitMode === "edit" ? true : null} />
                                <input type="text" value={this.state.name || ""}
                                    placeholder={"Unit Name"}
                                    onChange={this.handleNameChange} /> <br />
                                <Select className={"react-select"} options={this.state.selectConvenor}
                                    placeholder={"Select Convenor..."}
                                    onChange={this.handleSelectConvenorChange}
                                    value={this.state.selectedConvenor.label !== ""
                                        ? { label: this.state.selectedConvenor.label, value: this.state.selectedConvenor.value }
                                        : null} />
                            </div>
                            : null
                        }
                        {this.state.unitMode === "add" || ((this.state.unitMode === "edit" || this.state.unitMode === "remove")
                            && this.state.selectedUnit.label !== "")
                            ? < input type="submit" value="Submit" />
                            : null
                        }
                    </form> <br />
                </div>
                <div className="right-div">
                    <h2>Unit Offerings</h2>
                    <form onSubmit={this.handleSubmitUnitOffering}>
                        <label>
                            <input type="radio" value="add" name="unitOfferingMode" onChange={this.handleUnitOfferingModeChange} defaultChecked />
                            Add
                        </label>
                        <label>
                            <input type="radio" value="edit" name="unitOfferingMode" onChange={this.handleUnitOfferingModeChange} />
                            Edit
                        </label>
                        <label>
                            <input type="radio" value="remove" name="unitOfferingMode" onChange={this.handleUnitOfferingModeChange} />
                            Remove
                        </label>
                        {this.state.unitOfferingMode !== "add"
                            ? <Select className={"react-select"} options={this.state.selectUnitOffering}
                                onChange={this.handleSelectUnitOfferingChange}
                                value={this.state.selectedUnitOffering.label !== ""
                                    ? { label: this.state.selectedUnitOffering.label, value: this.state.selectedUnitOffering.value }
                                    : null} />
                            : null
                        }
                        {this.state.unitOfferingMode === "add" || (this.state.unitOfferingMode === "edit" && this.state.selectedUnitOffering.label !== "")
                            ?
                            <div>
                                <Select className={"react-select"} options={this.state.selectUnitOfferingUnit}
                                    placeholder={"Select Unit..."}
                                    onChange={this.handleSelectUnitOfferingUnitChange}
                                    value={this.state.selectedUnitOfferingUnit.label !== ""
                                        ? { label: this.state.selectedUnitOfferingUnit.label, value: this.state.selectedUnitOfferingUnit.value }
                                        : null} />
                                <Select className={"react-select"} options={this.state.selectTeachingPeriod}
                                    placeholder={"Select Period..."}
                                    onChange={this.handleSelectTeachingPeriodChange}
                                    value={this.state.selectedTeachingPeriod.label !== ""
                                        ? { label: this.state.selectedTeachingPeriod.label, value: this.state.selectedTeachingPeriod.value }
                                        : null} />
                                <Select className={"react-select"} options={this.state.selectTeachingYear}
                                    placeholder={"Select Year..."}
                                    onChange={this.handleSelectTeachingYearChange}
                                    value={this.state.selectedTeachingYear.label !== ""
                                        ? { label: this.state.selectedTeachingYear.label, value: this.state.selectedTeachingYear.value }
                                        : null} />
                            </div>
                            : null
                        }
                        {this.state.unitOfferingMode === "add" || ((this.state.unitOfferingMode === "edit" || this.state.unitOfferingMode === "remove")
                            && this.state.selectedUnitOffering.label !== "")
                            ? < input type="submit" value="Submit" />
                            : null
                        }
                    </form>
                </div>
            </div>
        );
    }
}
