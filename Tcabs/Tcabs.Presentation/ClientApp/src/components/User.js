﻿import React, { Component } from 'react';
import Select from 'react-select';
import { authenticating, unauthorized, forbidden } from '../Utilities';
import { checkPermissions, addUpdateUser, getUsers, deleteUser } from '../calls/UserCalls';

export class User extends Component {
    static displayName = User.name;

    constructor(props) {
        super(props);
        this.state = {
            permissions: ["Administrator"],
            user: null,
            permitted: null,
            authenticated: null,
            
            mode: "add",
            selectUser: [{ label: "", value: { userID: "", password: "", firstName: "", lastName: "", role: "" }}],
            selectedUser: { label: "", value: { userID: "", password: "", firstName: "", lastName: "", role: "" }},
            userID: "",
            password: "",
            passwordUnlock: false,
            firstName: "",
            lastName: "",
            role: "Administrator"
        };

        this.handleSelectUserChange = this.handleSelectUserChange.bind(this);
        this.handleUserIDChange = this.handleUserIDChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handlePasswordUnlock = this.handlePasswordUnlock.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        // Startup; check that user has permission to view this page
        this.checkPermissions();
    }

    // User selected a mode radio button (Add, Edit, Remove)
    handleModeChange = (event) => {
        if (event.target.value === "add") {
            this.setState({
                mode: event.target.value,

                selectedUser: {
                    label: "",
                    value: {
                        userID: "",
                        password: "",
                        firstName: "",
                        lastName: "",
                        role: ""
                    }
                },

                userID: "",
                password: "",
                passwordUnlock: false,
                firstName: "",
                lastName: "",
                role: "Administrator"
            })
        }
        else {
            this.setState({
                mode: event.target.value,
                userID: this.state.selectedUser.value.userID,
                password: "",
                firstName: this.state.selectedUser.value.firstName,
                lastName: this.state.selectedUser.value.lastName,
                role: this.state.selectedUser.value.role
            })
        }
    }

    // User changed the selected option in the react-select
    handleSelectUserChange(event) {
        this.setState({
            selectedUser: {
                label: event.label,
                value: {
                    userID: event.value.userID,
                    password: "",
                    firstName: event.value.firstName,
                    lastName: event.value.lastName,
                    role: event.value.role
                }
            },
            userID: event.value.userID,
            firstName: event.value.firstName,
            lastName: event.value.lastName,
            role: event.value.role
        });
    }

    // User typed something into User ID text input
    handleUserIDChange(event) {
        this.setState({ userID: event.target.value });
    }

    // User typed something into Password text input
    handlePasswordChange(event) {
        this.setState({password: event.target.value });
    }

    // Toggles the password lock in edit mode
    handlePasswordUnlock(event) {
        if (event.target.checked) {
            this.setState({
                passwordUnlock: event.target.checked
            });
        }
        else {
            this.setState({
                passwordUnlock: event.target.checked,
                password: ""
            });
        }
    }

    // User typed something into First Name text input
    handleFirstNameChange(event) {
        this.setState({ firstName: event.target.value });
    }

    // User typed something into Last Name text input
    handleLastNameChange(event) {
        this.setState({lastName: event.target.value });
    }

    // User selected a role radio button
    handleUserRoleChange = (event) => {
        console.log(event.target.value);
        this.setState({ role: event.target.value })
    }

    getUsers() {
        // Call getUsers from UserCalls.js
        getUsers()
            .then((response) => {
                // Iterate over user results and make them usable for react-select (label/value).
                var i;
                var userSelection = [];
                for(i in response.users) {
                    userSelection.push({
                        label: response.users[i].firstName + " " + response.users[i].lastName
                            + " (" + response.users[i].userID + ")",
                        value: {
                            userID: response.users[i].userID,
                            password: "",
                            firstName: response.users[i].firstName,
                            lastName: response.users[i].lastName,
                            role: response.users[i].role.name
                        }
                    });
                }

                // apply results from loop to selectUser state for use with react-select
                this.setState({
                    selectUser: userSelection
                });
            })
            .catch((error) => {
                console.error("Error getting users for selection: " + error.status);
            });
    }

    addUpdateUser() {
        var authMessage = "User page response from addUpdateUser(): ";
        var alertMessage = "";

        // Check that all form values are set
        // Exception is password field if doing an update request
        if (this.state.userID === "" || (this.state.password === "" && this.state.mode === "add")
            || this.state.firstName === "" || this.state.lastName === "" || this.state.role === "") {
            alertMessage = "Please enter User ID, Password, First Name, Last Name and Role.";
            alert(alertMessage);
        }
        else {
            // Call addUpdateUser from UserCalls.js
            // this.state.mode === "edit" sets isUpdate to true in api call if the edit radio button is active
            addUpdateUser(this.state.mode === "edit", this.state.userID, this.state.password,
                this.state.firstName, this.state.lastName, this.state.role)
                .then((response) => {
                    // User add successful
                    var s;
                    for (s in response.statusMessages) {
                        alertMessage += response.statusMessages[s].message;
                        if (s < response.statusMessages.length - 1) {
                            alertMessage += "\n";
                        }
                    }
                    console.log(authMessage + alertMessage);
                    alert(alertMessage);

                    // Reset the form
                    if (this.state.mode === "add") {
                        this.setState({
                            userID: "",
                            password: "",
                            firstName: "",
                            lastName: "",
                            role: "Administrator"
                        });
                    }
                    else {
                        // Update the react-select
                        this.setState({
                            selectedUser: {
                                label: this.state.firstName + " " + this.state.lastName
                                    + " (" + this.state.userID + ")",
                                value: {
                                    userID: this.state.userID,
                                    password: "",
                                    firstName: this.state.firstName,
                                    lastName: this.state.lastName,
                                    role: this.state.role
                                }
                            }
                        });
                    }
                    this.getUsers();
                })
                .catch((error) => {
                    // User add failed
                    // Concatenate status messages into alertMessage
                    var s;
                    for (s in error.statusMessages) {
                        alertMessage += error.statusMessages[s].message;
                        if (s < error.statusMessages.length - 1) {
                            alertMessage += "\n";
                        }
                    }
                    console.log(authMessage + alertMessage);
                    alert(alertMessage);
                });
        }
    }

    deleteUser() {
        var authMessage = "User page response from deleteUser(): ";
        var alertMessage = "";

        // Call deleteUser from UserCalls.js
        deleteUser(this.state.userID)
            .then((response) => {
                // User deletion successful
                var s;
                for (s in response.statusMessages) {
                    alertMessage += response.statusMessages[s].message;
                    if (s < response.statusMessages.length - 1) {
                        alertMessage += "\n";
                    }
                }
                console.log(authMessage + alertMessage);
                alert(alertMessage);

                // Clear react-select
                this.setState({
                    selectedUser: {
                        label: "",
                        value: {
                            userID: "",
                            password: "",
                            firstName: "",
                            lastName: "",
                            role: ""
                        }
                    }
                });
                this.getUsers();
            })
            .catch((error) => {
                // User add failed
                // Concatenate status messages into alertMessage
                var s;
                for (s in error.statusMessages) {
                    alertMessage += error.statusMessages[s].message;
                    if (s < error.statusMessages.length - 1) {
                        alertMessage += "\n";
                    }
                }
                console.log(authMessage + alertMessage);
                alert(alertMessage);
            });
    }

    handleSubmit(event) {
        event.preventDefault();

        // Dump all the form values into the console
        console.log("isUpdate: " + (this.state.mode === "edit"));
        console.log("userID: '" + this.state.userID + "'");
        console.log("password: '" + this.state.password + "'");
        console.log("firstName: '" + this.state.firstName + "'");
        console.log("lastName: '" + this.state.lastName + "'");
        console.log("role: '" + this.state.role + "'");

        // Submit pressed while radio button set to add or edit options
        if (this.state.mode === "add" || this.state.mode === "edit") {
            console.log("Add user pressed");
            this.addUpdateUser();
        }

        // Submit pressed while radio button set to remove option
        if (this.state.mode === "remove") {
            console.log("Remove user pressed");
            this.deleteUser();
        }
    }

    checkPermissions() {
        var authMessage = "User page response from checkPermissions(): ";
        // Call checkPermissions from UserCalls.js
        checkPermissions(this.state.permissions)
            .then((response) => {

                // User authentication verified with required permissions
                console.log(authMessage + response.status)
                this.setState({
                    authenticated: true,
                    permitted: true,
                    user: response.user
                });

                // Get users to populate the react-select
                this.getUsers();
            })
            .catch((error) => {
                console.error(authMessage + error.status);

                if (error.status === 401) {
                    // User is not authenticated
                    this.setState({
                        authenticated: false,
                        permitted: false,
                        user: error.user
                    });
                }

                if (error.status === 403) {
                    // User is authenticated but does not have required permissions
                    this.setState({
                        authenticated: true,
                        permitted: false
                    });
                }
            });
    }

    render() {

        if (this.state.authenticated === null) {
            // Waiting for authentication to complete
            return authenticating();
        }

        if (this.state.authenticated === false) {
            // Authentication check completed and user is not logged in.
            // Send them back to the login page.
            console.log("User page: Redirecting to Login page.");
            return unauthorized();
        }

        if (this.state.permitted === false) {
            // User is authenticated but doesn't have required permissions to view page.
            // This shouldn't happen if menu links applicable to the user are displayed.
            return forbidden();
        }

        return (
            <div>
                <div className="top-div">
                    <h1>Manage Users</h1>
                    <p>Add and edit users here.</p>
                </div>
                <div className="left-div">
                    <form onSubmit={this.handleSubmit}>
                        <label>
                            <input type="radio" value="add" name="mode" onChange={this.handleModeChange} defaultChecked />
                            Add
                        </label>
                        <label>
                            <input type="radio" value="edit" name="mode" onChange={this.handleModeChange} />
                            Edit
                        </label>
                        <label>
                            <input type="radio" value="remove" name="mode" onChange={this.handleModeChange} />
                            Remove
                        </label>
                        {this.state.mode !== "add"
                            ? <Select className={"react-select"} options={this.state.selectUser}
                                onChange={this.handleSelectUserChange}
                                value={this.state.selectedUser.label !== ""
                                    ? { label: this.state.selectedUser.label, value: this.state.selectedUser.value }
                                    : null} />
                            : null
                        }
                        {this.state.mode === "add" || (this.state.mode === "edit" && this.state.selectedUser.label !== "")
                            ?
                            <div>
                                <input type="text" value={this.state.userID || ""}
                                    placeholder={"User ID"}
                                    onChange={this.handleUserIDChange}
                                    readOnly={this.state.mode === "edit" ? true : null} />
                                <input type="password" value={this.state.password || ""}
                                    placeholder={this.state.mode === "edit" ? "Password (Unchanged)" : "Password"}
                                    onChange={this.handlePasswordChange}
                                    readOnly={this.state.passwordUnlock === false && this.state.mode === "edit" ? true : null} />
                                {this.state.mode !== "add"
                                    ? <input type="checkbox"
                                        checked={this.state.passwordUnlock}
                                        onChange={this.handlePasswordUnlock} />
                                    : null
                                }
                                <br />
                                <input type="text" value={this.state.firstName || ""}
                                    placeholder={"First Name"}
                                    onChange={this.handleFirstNameChange} />
                                <input type="text" value={this.state.lastName || ""}
                                    placeholder={"Last Name"}
                                    onChange={this.handleLastNameChange} /> <br />
                                <fieldset>
                                    <legend>Role:</legend>
                                    <label>
                                        <input type="radio" value="Administrator" name="role"
                                            onChange={this.handleUserRoleChange}
                                            checked={this.state.role === "Administrator" ? true : false}
                                            disabled={this.state.selectedUser.value.role === "Student"
                                                && this.state.mode !== "add" ? true : null} />
                                        Administrator
                                    </label>
                                    <label>
                                        <input type="radio" value="Convenor" name="role"
                                            onChange={this.handleUserRoleChange}
                                            checked={this.state.role === "Convenor" ? true : false}
                                            disabled={this.state.selectedUser.value.role === "Student"
                                                && this.state.mode !== "add" ? true : null} />
                                        Convenor
                                    </label>
                                    <label>
                                        <input type="radio" value="Supervisor" name="role"
                                            onChange={this.handleUserRoleChange}
                                            checked={this.state.role === "Supervisor" ? true : false}
                                            disabled={this.state.selectedUser.value.role === "Student"
                                                && this.state.mode !== "add" ? true : null} />
                                        Supervisor
                                    </label>
                                    <label>
                                        <input type="radio" value="Student" name="role"
                                            onChange={this.handleUserRoleChange}
                                            checked={this.state.role === "Student" ? true : false}
                                            disabled={(this.state.selectedUser.value.role === "Administrator"
                                                || this.state.selectedUser.value.role === "Convenor"
                                                || this.state.selectedUser.value.role === "Supervisor")
                                                && this.state.mode !== "add" ? true : null} />
                                        Student
                                    </label>
                                </fieldset>
                            </div>
                            : null
                        }
                        {this.state.mode === "add" || ((this.state.mode === "edit" || this.state.mode === "remove")
                            && this.state.selectedUser.label !== "")
                            ? < input type="submit" value="Submit" />
                            : null
                        }
                    </form> <br />
                </div>
                <div className="right-div">
                    {/* Empty */}
                </div>
            </div>
        );
    }
}
