﻿import React, { Component } from 'react';
import Select from 'react-select';
import { authenticating, unauthorized, forbidden } from '../Utilities';
import { checkPermissions, getUsersByPermission } from '../calls/UserCalls';
import {addUpdateProject, getProjects, deleteProject} from '../calls/ProjectCalls';

export class Project extends Component {
    static displayName = Project.name;

    constructor(props) {
        super(props);
        this.state = {
            permissions: ["Administrator", "Convenor"],
            user: null,
            permitted: null,
            authenticated: null,    
            selectedProjectDescription : "",       
            selectProject: [{label:"", value:{projectID: "", name:"", description:""}}],
            selectedProject: {label:"", value:{projectID: "", name:"", description:"" }}, 
            selectedProjectID:null,
           projectMode: "add",
            selectedProjectName: "",
           

        };
        
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleSubmitProject = this.handleSubmitProject.bind(this);
        this.addUpdateProject = this.addUpdateProject.bind(this);
        this.getProjects = this.getProjects.bind(this);
        this.handleProjectChange = this.handleProjectChange.bind(this);
    }

    componentDidMount() {
        // Startup; check that user has permission to view this page
        this.checkPermissions();
    }


    // User selected a mode radio button (Add, Edit, Remove)
    handleProjectModeChange = (event) => {
        

        console.log(event.target.value);
        this.setState({
            projectMode: event.target.value
        })
    }
   

    
        
    checkPermissions() {
        var authMessage = "Project page response from checkPermissions(): ";
        // Call checkPermissions from UserCalls.js
        checkPermissions(this.state.permissions)
            .then((response) => {

                // User authentication verified with required permissions
                console.log(authMessage + response.status)
                this.setState({
                    authenticated: true,
                    permitted: true,
                    user: response.user
                });

                // Get users to populate the react-select
                
                this.getProjects();
            })
            .catch((error) => {
                console.error(authMessage + error.status);

                if (error.status === 401) {
                    // User is not authenticated
                    this.setState({
                        authenticated: false,
                        permitted: false,
                        user: error.user
                    });
                }

                if (error.status === 403) {
                    // User is authenticated but does not have required permissions
                    this.setState({
                        authenticated: true,
                        permitted: false
                    });
                }
            });
    }

   

    // User typed something into Team Name text input
    handleNameChange(event) {
        this.setState({ ProjectName: event.target.value });
    }

    handleDescriptionChange(event) {
        this.setState({ ProjectDescription: event.target.value });
    }





addUpdateProject() {
    var authMessage = "Project page response from addUpdateProject(): ";
    var alertMessage = "";

   

    // // Check that all form values are set
    // if (this.state.selectedSupervisor.value.userID === "" || this.state.name === "") {
    //     alertMessage = "Please enter Team ID, Supervisor and Name.";
    //     alert(alertMessage);
    // }
    // else {
        // Call addUpdateTean from UserCalls.js
        // this.state.teamMode === "edit" sets isUpdate to true in api call if the edit radio button is active
       
        if(this.state.projectMode === "edit")
        {
           
            var name = this.state.selectedProjectName;
            var description = this.state.selectedProjectDescription;
            var projectID = Number(this.state.selectedProject.value.projectID);
            
        }
        else{
            
            var name = this.state.selectedProjectName;
            var description = this.state.selectedProjectDescription;
            var projectID = 0;
        }     

        addUpdateProject(this.state.projectMode === "edit", name, description, projectID)
            .then((response) => {
                // User add successful
                var s;
                for (s in response.statusMessages) {
                    alertMessage += response.statusMessages[s].message;
                    if (s < response.statusMessages.length - 1) {
                        alertMessage += "\n";
                    }
                }
                console.log(authMessage + alertMessage);
                alert(alertMessage);

                // Reset the form
                if (this.state.projectMode === "add") {
                    this.setState({
                        description: "",
                        name: ""
                       
                            
                        
                    });
                }
                else {
                    // Update the react-select
                    this.setState({
                        selectedProject: {
                            label: this.state.name + "(" + this.state.projectID + ")",
                            value: {
                                projectID: this.state.projectID,
                               name: this.state.name,
                               description: this.state.selectedDescription
                            }
                        }
                    });
                }
                this.getProjects();
            })
            .catch((error) => {
                // User add failed
                // Concatenate status messages into alertMessage
                var s;
                for (s in error.statusMessages) {
                    alertMessage += error.statusMessages[s].message;
                    if (s < error.statusMessages.length - 1) {
                        alertMessage += "\n";
                    }
                }
                console.log(authMessage + alertMessage);
                alert(alertMessage);
            });
    }




getProjects() {
    // Call getProject 
    console.log("Retreiving Project data ...")
    getProjects()
        .then((response) => {
            // Iterate over team results and make them usable for react-select (label/value).
            var i;
            var projectSelection = [];
            for (i in response.projects) {
                projectSelection.push({
                    label: response.projects[i].name  + " (" + response.projects[i].projectID + ")",
                    value: {
                        projectID: response.projects[i].projectID,
                        name: response.projects[i].name,
                        description: response.projects[i].description
                    }
                });
            }
            console.log("ProjectSel", projectSelection);

            // apply results from loop to selectTeam state for use with react-select
            this.setState({
                selectProject: projectSelection
            });
            console.log("Project state updated");
        })
        .catch((error) => {
            console.error("Error getting project for selection: " + error.status);
        });
}


deleteProject() {
    var authMessage = "Project page response from deleteProject(): ";
    var alertMessage = "";

    // Call deleteProject from ProjectCalls.js
    deleteProject(this.state.selectedProject.value.projectID)
        .then((response) => {
            // Project deletion successful
            var s;
            for (s in response.statusMessages) {
                alertMessage += response.statusMessages[s].message;
                if (s < response.statusMessages.length - 1) {
                    alertMessage += "\n";
                }
            }
            console.log(authMessage + alertMessage);
            alert(alertMessage);

            // Clear react-select
            this.setState({
                selectedProject: {
                    label: "",
                    value: {
                        projectID: "",
                        name: "",
                        description: "",
                        
                    }
                }
            });
            this.getProjects();
        })
        .catch((error) => {
            // User add failed
            // Concatenate status messages into alertMessage
            var s;
            for (s in error.statusMessages) {
                alertMessage += error.statusMessages[s].message;
                if (s < error.statusMessages.length - 1) {
                    alertMessage += "\n";
                }
            }
            console.log(authMessage + alertMessage);
            alert(alertMessage);
        });
}



    handleSubmitProject(event) {
        event.preventDefault();

        // Dump all the form values into the console
        // console.log("TeamId: '" + this.state.TeamId + "'");
        if(this.state.projectMode === 'add' || this.state.projectMode === 'edit'){
            console.log("ProjectName: '" + this.state.selectedProjectName + "'" + this.state.selectedProjectDescription + "");
            this.addUpdateProject();
        }else{
            console.log(this.state.selectedProject);
            this.deleteProject();
        }
        

    }

    // //selectedTeam: {label:"", value:{teamID: "", supervisor:{userID:"", firstName: "", lastName: "", role:""}}}, 
    handleProjectChange(event){
        this.setState({
            selectedProject: {
                label: event.label,
                value: {
                    projectID:event.value.projectID,
                    name: event.value.name,
                    description: event.value.description          
                }
            },            
        });
        this.setState({
            selectedProjectName: event.label.split("(")[0].trim(),
            selectedProjectDescription: event.value.description


        });
    }

    handleProjectChangeInput(event){
        this.setState({
            selectedProjectName: event.target.value,
            //selectedProjectDescription: event.target.value
    
        })
    }

    handleProjectChangeInputDescription(event){
        this.setState({
            selectedProjectDescription: event.target.value
    
        })
    }

    

    render() {

        //var projectName = this.state.selectedProject.label !== "" ? this.state.selectedTeam.label.split("(")[0] : null;
        if (this.state.authenticated === null) {
            // Waiting for authentication to complete
            return authenticating();
        }

        if (this.state.authenticated === false) {
            // Authentication check completed and user is not logged in.
            // Send them back to the login page.
            console.log("Project page: Redirecting to Login page.");
            return unauthorized();
        }

        if (this.state.permitted === false) {
            // User is authenticated but doesn't have required permissions to view page.
            // This shouldn't happen if menu links applicable to the user are displayed.
            return forbidden();
        }

        return (
            <div>
                <div className="top-div">
                    <h1>Manage Projects</h1>
                    <p>Add and edit projects here.</p>
                </div>
                <div className="left-div">
                <form onSubmit={this.handleSubmitProject}>
                <label>
                            <input type="radio" value="add" name="projectMode" onChange={this.handleProjectModeChange} defaultChecked />
                            Add
                        </label>
                        <label>
                            <input type="radio" value="edit" name="projectMode" onChange={this.handleProjectModeChange} />
                            Edit
                        </label>
                        <label>
                            <input type="radio" value="remove" name="projectMode" onChange={this.handleProjectModeChange} />
                            Remove
                        </label>
                        <br />



                        {
                        this.state.projectMode === "add" ?(<>                        
                        <input type="text" value={this.state.selectedProjectName || ""}
                                    placeholder={"Project Name"}
                                    onChange={this.handleProjectChangeInput.bind(this)}
                                    readOnly={this.state.projectMode === "edit" ? true : null} /> 
                                    <input type="text" onChange={this.handleProjectChangeInputDescription.bind(this)} value={this.state.selectedProjectDescription}/>
                            
                                <input type="submit" value="Add Project"/>
                        </>) : (<></>)
                        } 

                         {
                         this.state.projectMode === "edit" ? (<>
                           

                           <Select className={"react-select"} options={this.state.selectProject}
                            placeholder={"Select Projects..."} 
                             onChange={this.handleProjectChange}
                            value={this.state.selectedProject.label !== ""
                                ? { label: this.state.selectedProject.label, value: this.state.selectedProject.value }
                                : null} />  

                        <input type="text" value={this.state.selectedProjectName || ""}
                                    placeholder={"Project Name"}
                                    onChange={this.handleProjectChangeInput.bind(this)}/> 

                                    <input type="text" onChange={this.handleProjectChangeInputDescription.bind(this)} value={this.state.selectedProjectDescription}/>

<input type="submit" value="Edit Project"/>     
                        
                       </>):(<></>)}
                        {
                        this.state.projectMode === "remove" ? (<>
                        
                            <Select className={"react-select"} options={this.state.selectProject}
                            placeholder={"Select Projects..."} 
                             onChange={this.handleProjectChange}
                            value={this.state.selectedProject.label !== ""
                                ? { label: this.state.selectedProject.label, value: this.state.selectedProject.value }
                                : null} />  
                            <input type="submit" value="Remove Project"/>
                        
                        </>) : (<></>)}
                        
                    
                </form>
                </div>

            </div>
        );
    }
}
