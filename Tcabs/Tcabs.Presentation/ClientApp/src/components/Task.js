﻿import React, { Component } from 'react';
import Select from 'react-select';
import { authenticating, unauthorized, forbidden } from '../Utilities';
import { checkPermissions } from '../calls/UserCalls';

export class Task extends Component {
    static displayName = Task.name;

    constructor(props) {
        super(props);
        this.state = {
            permissions: ["Student"],
            user: null,
            permitted: null,
            authenticated: null
        };
    }

    componentDidMount() {
        // Startup; check that user has permission to view this page
        this.checkPermissions();
    }

    checkPermissions() {
        var authMessage = "Task page response from checkPermissions(): ";
        // Call checkPermissions from UserCalls.js
        checkPermissions(this.state.permissions)
            .then((response) => {

                // User authentication verified with required permissions
                console.log(authMessage + response.status)
                this.setState({
                    authenticated: true,
                    permitted: true,
                    user: response.user
                });

                // Get users to populate the react-select
                this.getUsers();
            })
            .catch((error) => {
                console.error(authMessage + error.status);

                if (error.status === 401) {
                    // User is not authenticated
                    this.setState({
                        authenticated: false,
                        permitted: false,
                        user: error.user
                    });
                }

                if (error.status === 403) {
                    // User is authenticated but does not have required permissions
                    this.setState({
                        authenticated: true,
                        permitted: false
                    });
                }
            });
    }

    render() {

        if (this.state.authenticated === null) {
            // Waiting for authentication to complete
            return authenticating();
        }

        if (this.state.authenticated === false) {
            // Authentication check completed and user is not logged in.
            // Send them back to the login page.
            console.log("Task page: Redirecting to Login page.");
            return unauthorized();
        }

        if (this.state.permitted === false) {
            // User is authenticated but doesn't have required permissions to view page.
            // This shouldn't happen if menu links applicable to the user are displayed.
            return forbidden();
        }

        return (
            <div>
                <div className="top-div">
                    <h1>Submit Tasks</h1>
                    <p>Students to submit individual tasks here.</p>
                </div>
            </div>
        );
    }
}
