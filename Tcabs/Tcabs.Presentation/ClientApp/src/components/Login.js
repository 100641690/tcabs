﻿import React, { Component } from 'react';
import { authenticating } from '../Utilities';
import { checkAuthenticated, authenticateUser } from '../calls/UserCalls';
import { Redirect } from 'react-router-dom';

export class Login extends Component {
    static displayName = Login.name;

    constructor(props) {
        super(props);
        this.state = {
            userId: "",
            password: "",
            authenticated: null
        };

        this.handleUserIdChange = this.handleUserIdChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.checkAuthenticated();
    }

    checkAuthenticated() {
        var authMessage = "Login page response from checkAuthenticated(): ";
        checkAuthenticated()
        .then((response) => {
            console.log(authMessage + response.status);
            this.setState({ authenticated: true });
        })
        .catch((error) => {
            console.error(authMessage + error.status);
            this.setState({ authenticated: false });
        });
    }

    handleUserIdChange(event) {
        this.setState({ userId: event.target.value });
    }

    handlePasswordChange(event) {
        this.setState({ password: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        var authMessage = "Login page response from login(): ";
        var alertMessage = "";

        if (this.state.userId === "" || this.state.password === "") {
            alertMessage = "Please enter User ID and Password.";
            alert(alertMessage);
        }
        else {
            authenticateUser(this.state.userId, this.state.password)
                .then((response) => {
                    var s;
                    for (s in response.statusMessages) {
                        alertMessage += response.statusMessages[s].message + "\n";
                    }
                    
                    alertMessage += "Authenticated as: " +
                        response.user.firstName + " " + response.user.lastName
                        + "\nRole: " + response.user.role.name + "\nPermissions: ";
                    var p;
                    for (p in response.permissions) {
                        alertMessage += response.permissions[p].name;
                        if (p < response.permissions.length - 1) {
                            alertMessage += ", ";
                        }
                    }

                    console.log(authMessage + alertMessage);
                    alert(alertMessage);
                    this.setState({
                        authenticated: true
                    });
                })
                .catch((error) => {
                    var s;
                    for (s in error.statusMessages) {
                        alertMessage += error.statusMessages[s].message + "\n";
                    }
                    alert(alertMessage);
                    this.setState({
                        authenticated: false
                    });
                });
        }
    }

    render() {
        if (this.state.authenticated === null) {
            // Waiting for authentication to complete
            return authenticating();
        }

        if (this.state.authenticated === true) {
            // User is authenticated. Redirect to home page.
            console.log("Login: Redirecting to Home page.");
            return < Redirect to="/" />;
        }

        // Authentication check has finished and user in not logged in
        // Show the login form

        return (
            <div>
                <h1>Login</h1>
                <p>Please enter your login details below.</p>
                <form onSubmit={this.handleSubmit}>
                    <input type="text" value={this.state.userId} placeholder={"User ID"} onChange={this.handleUserIdChange} />< br/>
                    <input type="password" value={this.state.password} placeholder={"Password"} onChange={this.handlePasswordChange} />< br />
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
}
