﻿import React, { Component } from 'react';
import { logout } from '../calls/UserCalls'
import { Redirect } from 'react-router-dom';

export class Logout extends Component {
    static displayName = Logout.name;

    constructor(props) {
        super(props);
        this.state = {
            logoutComplete: false,
            logoutSuccess: false
        };
    }

    componentDidMount() {
        this.tryLogout();
    }

    tryLogout() {
        var authMessage = "Logout page response from logout(): ";
        logout()
            .then((response) => {
                // Logout call success
                console.log(authMessage + response.status);
                this.setState({
                    logoutComplete: true,
                    logoutSuccess: true
                });
            })
            .catch((error) => {
                // Logout call failure
                console.error(authMessage + error.status);
                this.setState({
                    logoutComplete: true,
                    logoutSuccess: false
                });
            });
    }

    render() {
        // Currently attempting to logout
        if (!this.state.logoutComplete) {
            return <p>Logging out...</p>;
        }
        
        // Logout was unsuccessful; display error
        if (!this.state.logoutSuccess) {
            return (
                <div>
                    <h1>Logout Failed</h1>
                    <p>Please try again.</p>
                </div>
            );
        }

        //Logout was successful; redirect
        return < Redirect to="/user/login" />;
    }
}
