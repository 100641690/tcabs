﻿import React, { Component } from 'react';
import Select from 'react-select';
import DualListBox from 'react-dual-listbox';
import 'react-dual-listbox/lib/react-dual-listbox.css';
import 'font-awesome/css/font-awesome.min.css';
import { authenticating, unauthorized, forbidden } from '../Utilities';
import { checkPermissions, getUsersByRole } from '../calls/UserCalls';
import { addUpdateEnrolments, getEnrolmentsByUnitOffering } from '../calls/EnrolmentCalls';
import { getUnitOfferings } from '../calls/UnitCalls';

export class Enrolment extends Component {
    static displayName = Enrolment.name;

    constructor(props) {
        super(props);
        this.state = {
            permissions: ["Administrator"],
            user: null,
            permitted: null,
            authenticated: null,

            selectUnitOffering: [{
                label: "",
                value: {
                    unitOfferingID: "",
                    unit: {
                        unitCode: "",
                        name: "",
                    },
                    teachingPeriod: {
                        name: ""
                    },
                    teachingYear: {
                        year: ""
                    }
                }
            }],
            selectedUnitOffering: {
                label: "",
                value: {
                    unitOfferingID: "",
                    unit: {
                        unitCode: "",
                        name: "",
                    },
                    teachingPeriod: {
                        name: ""
                    },
                    teachingYear: {
                        year: ""
                    }
                }
            },
            selectEnrollable: [],
            selectEnrolled: []
        };

        this.handleSelectUnitOfferingChange = this.handleSelectUnitOfferingChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        // Startup; check that user has permission to view this page
        this.checkPermissions();
    }

    handleSelectUnitOfferingChange(event) {
        this.setState({
            selectedUnitOffering: {
                label: event.label,
                value: {
                    unitOfferingID: event.value.unitOfferingID,
                    unit: {
                        unitCode: event.value.unit.unitCode,
                        name: event.value.unit.name
                    },
                    teachingPeriod: {
                        teachingPeriodID: event.value.teachingPeriod.teachingPeriodID,
                        name: event.value.teachingPeriod.name
                    },
                    teachingYear: {
                        year: event.value.teachingYear.year
                    }
                }
            }
        });
        this.getEnrolmentsByUnitOffering(event.value.unitOfferingID);
    }
    
    handleEnrolmentChange = (selectEnrolled) => {
        this.setState({ selectEnrolled });
    }

    getUnitOfferings() {
        // Call getUnitOfferings from UnitCalls.js
        getUnitOfferings()
            .then((response) => {
                // Iterate over unit offering results and make them usable for react-select (label/value).
                var i;
                var unitOfferingSelection = [];
                for (i in response.unitOfferings) {
                    unitOfferingSelection.push({
                        label: response.unitOfferings[i].unit.name
                            + " (" + response.unitOfferings[i].unit.unitCode + ") "
                            + response.unitOfferings[i].teachingPeriod.name + ", "
                            + response.unitOfferings[i].teachingYear.year,
                        value: {
                            unitOfferingID: response.unitOfferings[i].unitOfferingID,
                            unit: {
                                unitCode: response.unitOfferings[i].unit.unitCode,
                                name: response.unitOfferings[i].unit.name
                            },
                            teachingPeriod: {
                                name: response.unitOfferings[i].teachingPeriod.name
                            },
                            teachingYear: {
                                year: response.unitOfferings[i].teachingYear.year
                            }
                        }
                    });
                }

                // apply results from loop to selectUser state for use with react-select
                this.setState({
                    selectUnitOffering: unitOfferingSelection
                });
            })
            .catch((error) => {
                console.error("Error getting unit offerings for selection: " + error.status);
            });
    }

    getEnrolmentsByUnitOffering(unitOfferingID) {
        // Call getUsers from UserCalls.js
        getEnrolmentsByUnitOffering(unitOfferingID)
            .then((response) => {

                var enrollable = response.enrolments.concat(response.notEnrolled);
                var enrollableSelection = [];
                var enrolledSelection = [];

                var i;
                for (i in enrollable) {
                    enrollableSelection.push({
                        label: enrollable[i].student.firstName + " " + enrollable[i].student.lastName
                            + " (" + enrollable[i].student.userID + ")",
                        value: JSON.stringify({
                            enrolmentID: enrollable[i].enrolmentID,
                            student: {
                                userID: enrollable[i].student.userID,
                                firstName: enrollable[i].student.firstName,
                                lastName: enrollable[i].student.lastName
                            }
                        })
                    });
                }
                
                for (i in response.enrolments) {
                    enrolledSelection.push(
                        JSON.stringify({
                            enrolmentID: response.enrolments[i].enrolmentID,
                            student: {
                                userID: response.enrolments[i].student.userID,
                                firstName: response.enrolments[i].student.firstName,
                                lastName: response.enrolments[i].student.lastName
                            }
                        })
                    );
                }

                // apply results from loop to selectEnrolment state for use with listbox
                this.setState({
                    selectEnrollable: enrollableSelection,
                    selectEnrolled: enrolledSelection
                });
            })
            .catch((error) => {
                console.error("Error getting enrolments for selection: " + error.status);
            });
    }

    addUpdateEnrolments() {
        var authMessage = "Enrolment page response from addUpdateEnrolment(): ";
        var alertMessage = "";

        // Check that all form values are set
        // Exception is password field if doing an update request
        if (this.state.selectedUnitOffering.value.unitOfferingID === "") {
            alertMessage = "Please select a unit offering from the dropdown box.";
            alert(alertMessage);
        }
        else {
            //console.log(JSON.stringify(this.state.selectEnrollable, null, 4));
            //console.log("============");
            //console.log(JSON.stringify(this.state.selectEnrolled, null, 4));

            var toRemove = [];
            var toAdd = [];
            var remaining = [];
            var i;

            // Enrollable (all students including those already enrolled)
            for (i in this.state.selectEnrollable) {
                var enrollable = JSON.parse(this.state.selectEnrollable[i].value);
                if (enrollable.enrolmentID !== null) {
                    toRemove.push(enrollable.enrolmentID);
                }
            }

            // Enrolled
            for (i in this.state.selectEnrolled) {
                var enrolled = JSON.parse(this.state.selectEnrolled[i]);
                if (enrolled.enrolmentID === null) {
                    toAdd.push(enrolled.student.userID);
                }
                else {
                    remaining.push(enrolled.enrolmentID);
                }
            }

            toRemove = toRemove.filter(n => !remaining.includes(n));

            console.log("unitOfferingID: " + this.state.selectedUnitOffering.value.unitOfferingID)
            console.log("Students to Enrol: " + toAdd);
            console.log("Enrolments to remove: " + toRemove);
            //console.log("remaining: " + remaining);

            if (toAdd.length === 0 && toRemove.length === 0) {
                alertMessage = "No changes to make.";
                alert(alertMessage);
            }
            else {
                addUpdateEnrolments(this.state.selectedUnitOffering.value.unitOfferingID, toAdd, toRemove)
                    .then((response) => {
                        // Enrolment update successful
                        var s;
                        for (s in response.statusMessages) {
                            alertMessage += response.statusMessages[s].message;
                            if (s < response.statusMessages.length - 1) {
                                alertMessage += "\n";
                            }
                        }
                        console.log(authMessage + alertMessage);
                        alert(alertMessage);
                    })
                    .then((response) => {
                        this.getEnrolmentsByUnitOffering(this.state.selectedUnitOffering.value.unitOfferingID);
                    })
                    .catch((error) => {
                        // Enrolment update failed
                        // Concatenate status messages into alertMessage
                        var s;
                        for (s in error.statusMessages) {
                            alertMessage += error.statusMessages[s].message;
                            if (s < error.statusMessages.length - 1) {
                                alertMessage += "\n";
                            }
                        }
                        console.log(authMessage + alertMessage);
                        alert(alertMessage);
                    });
            }
        }
    }

    checkPermissions() {
        var authMessage = "Enrolment page response from checkPermissions(): ";
        // Call checkPermissions from UserCalls.js
        checkPermissions(this.state.permissions)
            .then((response) => {

                // User authentication verified with required permissions
                console.log(authMessage + response.status);
                this.setState({
                    authenticated: true,
                    permitted: true,
                    user: response.user
                });

                // Get users to populate the react-select
                this.getUnitOfferings();
            })
            .catch((error) => {
                console.error(authMessage + error.status);

                if (error.status === 401) {
                    // User is not authenticated
                    this.setState({
                        authenticated: false,
                        permitted: false,
                        user: error.user
                    });
                }

                if (error.status === 403) {
                    // User is authenticated but does not have required permissions
                    this.setState({
                        authenticated: true,
                        permitted: false
                    });
                }
            });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.addUpdateEnrolments();
    }

    render() {

        if (this.state.authenticated === null) {
            // Waiting for authentication to complete
            return authenticating();
        }

        if (this.state.authenticated === false) {
            // Authentication check completed and user is not logged in.
            // Send them back to the login page.
            console.log("Enrolment page: Redirecting to Login page.");
            return unauthorized();
        }

        if (this.state.permitted === false) {
            // User is authenticated but doesn't have required permissions to view page.
            // This shouldn't happen if menu links applicable to the user are displayed.
            return forbidden();
        }

        return (
            <div>
                <div className="top-div">
                    <h1>Manage Enrolments</h1>
                    <p>Enrol students into units here.</p>
                </div>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <Select className={"react-select"} options={this.state.selectUnitOffering}
                            canFilter
                            onChange={this.handleSelectUnitOfferingChange}
                            value={this.state.selectedUnitOffering.label !== ""
                                ? { label: this.state.selectedUnitOffering.label, value: this.state.selectedUnitOffering.value }
                                : null} />
                        <DualListBox
                            canFilter
                            showHeaderLabels
                            options={this.state.selectEnrollable}
                            selected={this.state.selectEnrolled}
                            onChange={this.handleEnrolmentChange} />
                        <input type="submit" value="Submit" />
                    </form>
                </div>
            </div>
        );
    }
}
