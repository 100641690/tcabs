import React, { Component } from 'react';
import { authenticating, unauthorized, forbidden } from '../Utilities';
import { checkPermissions } from '../calls/UserCalls';

export class Home extends Component {
    static displayName = Home.name;

    constructor(props) {
        super(props);
        this.state = {
            permissions: [],
            user: null,
            permitted: null,
            authenticated: null
        };
    }
    
    componentDidMount() {
        this.checkPermissions();
    }

    checkPermissions() {
        var authMessage = "Home page response from checkPermissions(): ";
        // Call checkPermissions from UserCalls.js
        checkPermissions(this.state.permissions)
            .then((response) => {

                // User authentication verified with required permissions
                console.log(authMessage + response.status)
                this.setState({
                    authenticated: true,
                    permitted: true,
                    user: response.user

                    //***********************************************//
                    //* Additional API calls for page content below *//
                    //***********************************************//

                    // CALL
                    // CALL
                    // CALL
                });
            })
            .catch((error) => {
                console.error(authMessage + error.status);

                if (error.status === 401) {
                    // User is not authenticated
                    this.setState({
                        authenticated: false,
                        permitted: false,
                        user: error.user
                    });
                }

                if (error.status === 403) {
                    // User is authenticated but does not have required permissions
                    this.setState({
                        authenticated: true,
                        permitted: false
                    });
                }
            });
    }

    render() {
        if (this.state.authenticated === null) {
            // Waiting for authentication to complete
            return authenticating();
        }

        if (this.state.authenticated === false) {
            // Authentication check completed and user is not logged in.
            // Send them back to the login page.
            console.log("Home: Redirecting to Login page.");
            return unauthorized();
        }

        if (this.state.permitted === false) {
            // User is authenticated but doesn't have required permissions to view page.
            // This shouldn't happen if menu links applicable to the user are displayed.
            return forbidden();
        }

        // All checks are complete; display the final page content.
        return (
            <div>
                <h1>Home</h1>
                <p>Welcome to TCABS, {this.state.user.firstName}!</p>
            </div>
        );
    }
}
