﻿import { statusCode } from '../Utilities';

// ************************************** //
// *      Team calls for Management     * //
// ************************************** //

export function addUpdateTeam(isUpdate, supervisorID, name, teamID) {
    console.log("sup", supervisorID);
    return new Promise((resolve, reject) => {
        
        if(isUpdate){
            var data = {
                IsUpdate: isUpdate,
                TeamID: teamID,
                SupervisorID: supervisorID,
                Name: name
            };

        }else{
            var data = {
                IsUpdate: isUpdate,
                SupervisorID: supervisorID,
                Name: name
            };
        }
        console.log("body", JSON.stringify(data))

        console.log("addTeam() starting API call to AddTeam()");
        var logMessage = "Response from API AddTeam(): ";
        console.log("body", JSON.stringify(data))
        fetch("team", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)            
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.

                console.log(logMessage + statusCode(obj.status));

                // Added Team Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }

                // Added Team Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function getTeams() {
    return new Promise((resolve, reject) => {

        console.log("getTeams() starting API call to GetTeams()");
        var logMessage = "Response from API GetTeams(): ";

        fetch("team", {
            method: "get"
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.
                // Get Teams Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        teams: obj.body.teams
                    });
                }

                // Get Teams Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function deleteTeam(teamID) {
    return new Promise((resolve, reject) => {
        console.log("deleteTeam() starting API call to DeleteTeam()");
        var logMessage = "Response from API DeleteTeam(): ";

        fetch("team/" + teamID, {
            method: "delete",
            headers: {
                "Accept": "application/json"
            }
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                console.log(logMessage + statusCode(obj.status));

                // Delete Team Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                    });
                }

                // Delete Team Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}