﻿import { statusCode } from '../Utilities';

// ************************************** //
// *      Project calls for Management     * //
// ************************************** //

export function addUpdateProject(isUpdate, name, description, projectID) {
    
    return new Promise((resolve, reject) => {
if(isUpdate){
        var data = {
            IsUpdate: isUpdate,
            ProjectID: projectID,
            Name: name,
            Description: description
            
            
        };
    }
    else{
        var data = {
            IsUpdate: isUpdate,
            Name: name,
            Description: description
            
        };
    }
        console.log("body", JSON.stringify(data))
        console.log("addProject() starting API call to AddProject()");
        var logMessage = "Response from API AddProject(): ";
        console.log("body", JSON.stringify(data))
        
        fetch("project", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                 //alert(JSON.stringify(obj, null, 4)); // Show JSON contents.

                console.log(logMessage + statusCode(obj.status));

                // Added Project Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }

                // Added Project Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });

}

export function getProjects() {
    return new Promise((resolve, reject) => {

        console.log("getProjects() starting API call to GetProjects()");
        var logMessage = "Response from API GetProjects(): ";

        fetch("project", {
            method: "get"
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.
                // Get Projects Successful
                
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        projects: obj.body.projects
                    });
                }

                // Get Projects Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function deleteProject(projectID) {
    return new Promise((resolve, reject) => {
        console.log("deleteProject() starting API call to DeleteProject()");
        var logMessage = "Response from API DeleteProject(): ";

        fetch("Project/" + projectID, {
            method: "delete",
            headers: {
                "Accept": "application/json"
            }
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                console.log(logMessage + statusCode(obj.status));

                // Delete Project Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                    });
                }

                // Delete Project Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}