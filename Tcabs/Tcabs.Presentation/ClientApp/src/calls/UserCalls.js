﻿import { statusCode } from '../Utilities';

// ************************************** //
// *    User calls for Authentication   * //
// ************************************** //

// Used to log the user in by verifying the credentials entered in the form against the database.
// Successful call causes a login identity to be created on the server.

export function authenticateUser(userID, password) {
    return new Promise((resolve, reject) => {
        var data = {
            UserID: userID,
            Password: password
        };

        console.log("authenticateUser() starting API call to AuthenticateUserAsync()");
        var logMessage = "Response from API AuthenticateUserAsync(): ";

        fetch("user/login/authenticate", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                console.log(logMessage + statusCode(obj.status));

                // Login successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        user: obj.body.user,
                        permissions: obj.body.permissions
                    });
                }

                // Login failed
                if (obj.status === 500) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        user: obj.body.user,
                        permissions: obj.body.permissions,
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

// Called when the user goes to the login page when already authenticated.
// If they're logged in, redirect them to home page.

export async function checkAuthenticated() {
    return new Promise((resolve, reject) => {

        console.log("isAuthenticated() starting API call to CheckAuthentication()");
        var logMessage = "Response from API CheckAuthentication(): ";

        fetch("user/login/check", {
            method: "get"
        })
            .then(response => {
                console.log(logMessage + statusCode(response.status));

                // User is already logged in
                if (response.status === 200) {
                    resolve({
                        status: response.status
                    });
                }

                // User is not logged in
                if (response.status === 401) {
                    reject({
                        status: response.status
                    });
                }
            })
            .catch(error => {
                console.error(error)
            });
    });
}

// Called on any page other than login to check if user has permission to view it
// If they're not authenticated, redirect to login page

export function checkPermissions(permissions = []) {
    return new Promise((resolve, reject) => {

        var data = {
            Permissions: permissions
        };

        console.log("checkPermissions() starting API call to CheckPermissions()\nUsing: " + permissions);
        var logMessage = "Response from API CheckPermissions(): ";

        fetch("user/login/permitted", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                console.log(logMessage + statusCode(obj.status));

                // User is logged in and has permission to view page
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        user: obj.body.user
                    });
                }

                // User has not logged in; redirect to login page.
                if (obj.status === 401) {
                    reject({
                        status: obj.status
                    });
                }

                // User logged in but does not have required permission to view page.
                if (obj.status === 403) {
                    reject({
                        status: obj.status,
                        user: obj.body.user
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

// Called by the logout page when the user presses the logout button from the NavMenu.

export function logout() {
    return new Promise((resolve, reject) => {

        console.log("logout() starting API call to LogoutUserAsync()");
        var logMessage = "Response from API Logout(): ";

        fetch("user/logout/destroy", {
            method: "get"
        })
            .then(response => {
                console.log(logMessage + statusCode(response.status));

                // Logout success
                if (response.status === 200) {
                    resolve({
                        status: response.status
                    });
                }

                // Logout failure
                if (response.status === 500) {
                    reject({
                        status: response.status
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

// ************************************** //
// *      User calls for Management     * //
// ************************************** //

export function addUpdateUser(isUpdate, userID, password, firstName, lastName, role) {
    return new Promise((resolve, reject) => {
        
        var data = {
            IsUpdate: isUpdate,
            UserID: userID,
            Password: password,
            FirstName: firstName,
            LastName: lastName,
            Role: role
        };

        console.log("addUser() starting API call to AddUser()");
        var logMessage = "Response from API AddUser(): ";

        fetch("user", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.
                
                console.log(logMessage + statusCode(obj.status));

                // Added User Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }

                // Added User Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function getUsers() {
    return new Promise((resolve, reject) => {

        console.log("getUsers() starting API call to GetUsers()");
        var logMessage = "Response from API GetUsers(): ";

        fetch("user", {
            method: "get"
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                console.log(logMessage + statusCode(obj.status));

                // Get Users Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        users: obj.body.users
                    });
                }

                // Get Users Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function getUsersByRole(name) {
    return new Promise((resolve, reject) => {

        console.log("getUsersByRole() starting API call to GetUsersByRole()");
        var logMessage = "Response from API GetUsersByRole(): ";

        fetch("user/role/" + name, {
            method: "get",
            headers: {
                "Accept": "application/json"
            }
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                console.log(logMessage + statusCode(obj.status));

                // Get Users Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        users: obj.body.users
                    });
                }

                // Get Users Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function getUsersByPermission(name) {
    return new Promise((resolve, reject) => {

        console.log("getUsersByRole() starting API call to GetUsersByRole()");
        var logMessage = "Response from API GetUsersByRole(): ";

        fetch("user/permission/" + name, {
            method: "get",
            headers: {
                "Accept": "application/json"
            }
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                console.log(logMessage + statusCode(obj.status));

                // Get Users Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        users: obj.body.users
                    });
                }

                // Get Users Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function deleteUser(userID) {
    return new Promise((resolve, reject) => {
        console.log("deleteUser() starting API call to DeleteUser()");
        var logMessage = "Response from API DeleteUser(): ";

        fetch("user/" + userID, {
            method: "delete",
            headers: {
                "Accept": "application/json"
            }
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                console.log(logMessage + statusCode(obj.status));

                // Delete Users Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                    });
                }

                // Delete Users Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}