﻿import { statusCode } from '../Utilities';

// ************************************** //
// *   Enrolment calls for Management   * //
// ************************************** //

export function addUpdateEnrolments(unitOfferingID, studentsToEnrol, enrolmentsToRemove) {
    return new Promise((resolve, reject) => {

        var data = {
            UnitOfferingID: unitOfferingID,
            StudentsToEnrol: studentsToEnrol,
            EnrolmentsToRemove: enrolmentsToRemove
        };
        console.log("addUpdateEnrolments() starting API call to AddUpdateEnrolments() with:\n"
            + JSON.stringify(data, null, 4));
        var logMessage = "Response from API AddUpdateEnrolments(): ";

        fetch("enrolment", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.

                console.log(logMessage + statusCode(obj.status));

                // Student enrolment call successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }

                // Student enrolment call failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function getEnrolments() {
    return new Promise((resolve, reject) => {

        console.log("getEnrolments() starting API call to GetEnrolments()");
        var logMessage = "Response from API GetEnrolments(): ";

        fetch("enrolment", {
            method: "get"
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.
                // Get enrolments successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        enrolments: obj.body.enrolments
                    });
                }

                // Get enits failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function getEnrolmentsByUnitOffering(unitOfferingID) {
    return new Promise((resolve, reject) => {

        console.log("getEnrolmentsByUnitOffering() starting API call to GetEnrolmentsByUnitOffering()");
        var logMessage = "Response from API GetEnrolmentsByUnitOffering(): ";

        fetch("enrolment/enrolments/" + unitOfferingID, {
            method: "get"
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.
                // Get enrolments successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        enrolments: obj.body.enrolments,
                        notEnrolled: obj.body.notEnrolled
                    });
                }

                // Get enits failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}