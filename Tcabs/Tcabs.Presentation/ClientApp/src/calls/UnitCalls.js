﻿import { statusCode } from '../Utilities';

// ************************************** //
// *      Unit calls for Management     * //
// ************************************** //

export function addUpdateUnit(isUpdate, unitCode, convenorID, name) {
    return new Promise((resolve, reject) => {
        
        var data = {
            IsUpdate: isUpdate,
            UnitCode: unitCode,
            ConvenorID: convenorID,
            Name: name
        };

        console.log("addUpdateUnit() starting API call to AddUpdateUnit()");
        var logMessage = "Response from API AddUnit(): ";

        fetch("unit", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.

                console.log(logMessage + statusCode(obj.status));

                // Added Unit Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }

                // Added Unit Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function getUnits() {
    return new Promise((resolve, reject) => {

        console.log("getUnits() starting API call to GetUnits()");
        var logMessage = "Response from API GetUnits(): ";

        fetch("unit", {
            method: "get"
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.
                // Get Units Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        units: obj.body.units
                    });
                }

                // Get Units Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function deleteUnit(unitCode) {
    return new Promise((resolve, reject) => {
        console.log("deleteUnit() starting API call to DeleteUnit()");
        var logMessage = "Response from API DeleteUnit(): ";

        fetch("unit/" + unitCode, {
            method: "delete",
            headers: {
                "Accept": "application/json"
            }
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                console.log(logMessage + statusCode(obj.status));

                // Delete Unit Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                    });
                }

                // Delete Unit Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

// ************************************** //
// * Unit Offering calls for Management * //
// ************************************** //

export function addUpdateUnitOffering(unitOfferingID, unitCode, teachingPeriodID, year) {
    return new Promise((resolve, reject) => {

        var castUnitOfferingID;
        if (unitOfferingID === "") {
            castUnitOfferingID = null;
        }
        else {
            castUnitOfferingID = parseInt(unitOfferingID);
        }

        var data = {
            UnitOfferingID: castUnitOfferingID,
            UnitCode: unitCode,
            TeachingPeriodID: teachingPeriodID,
            Year: year
        };
        console.log(typeof(data.unitOfferingID));
        console.log("addUpdateUnitOffering() starting API call to AddUpdateUnitOffering()");
        var logMessage = "Response from API AddUpdateUnitOffering(): ";

        fetch("unitoffering", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.

                console.log(logMessage + statusCode(obj.status));

                // Added Unit Offering Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }

                // Added Unit Offering Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function getUnitOfferings() {
    return new Promise((resolve, reject) => {

        console.log("getUnitOfferings() starting API call to GetUnitOfferings()");
        var logMessage = "Response from API GetUnitOfferings(): ";

        fetch("unitoffering", {
            method: "get"
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.
                // Get Unit Offerings Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        unitOfferings: obj.body.unitOfferings
                    });
                }

                // Get Unit Offerings Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function deleteUnitOffering(unitOfferingID) {
    return new Promise((resolve, reject) => {
        console.log("deleteUnitOffering() starting API call to DeleteUnitOffering()");
        var logMessage = "Response from API DeleteUnitOffering(): ";

        fetch("unitoffering/" + unitOfferingID, {
            method: "delete",
            headers: {
                "Accept": "application/json"
            }
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                console.log(logMessage + statusCode(obj.status));

                // Delete Unit Offering Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                    });
                }

                // Delete Unit Offering Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function getTeachingPeriods() {
    return new Promise((resolve, reject) => {

        console.log("getTeachingPeriods() starting API call to GetTeachingPeriods()");
        var logMessage = "Response from API GetTeachingPeriods(): ";

        fetch("unitoffering/periods", {
            method: "get"
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.
                // Get Teaching Periods Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        teachingPeriods: obj.body.teachingPeriods
                    });
                }

                // Get Teaching Periods Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}

export function getTeachingYears() {
    return new Promise((resolve, reject) => {

        console.log("getTeachingYears() starting API call to GetTeachingYears()");
        var logMessage = "Response from API GetTeachingYears(): ";

        fetch("unitoffering/years", {
            method: "get"
        })
            .then(response => response.json().then(data => ({ status: response.status, body: data })))
            .then(obj => {
                // alert(JSON.stringify(obj, null, 4)); // Show JSON contents.
                // Get Teaching Years Successful
                if (obj.status === 200) {
                    resolve({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages,
                        teachingYears: obj.body.teachingYears
                    });
                }

                // Get Teaching Years Failed
                if (obj.status !== 200) {
                    reject({
                        status: obj.status,
                        statusMessages: obj.body.statusMessages
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    });
}