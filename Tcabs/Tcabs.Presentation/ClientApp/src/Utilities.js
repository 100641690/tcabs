﻿import React from 'react';
import { Redirect } from 'react-router-dom';

export function authenticating() {
    return <p>Authenticating...</p>;
}

export function forbidden() {
    return (
        <div>
            <h1>403 Forbidden</h1>
            <p>You do not have permission to view this page</p>
        </div>
    );
}

export function unauthorized() {
    return < Redirect to="/user/login" />;
}

export function statusCode(code) {
    if (code === 200) return code + " (OK)";
    if (code === 400) return code + " (Bad Request)";
    if (code === 401) return code + " (Unauthorized)";
    if (code === 403) return code + " (Forbidden)";
    if (code === 500) return code + " (Internal Server Error)";
    return code + "(UNEXPECTED)";
}