﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tcabs.Logic;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Utilities;
using System.Net;
using Tcabs.Common.Dtos;

namespace Tcabs.Testing
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void TestAddUnitValid()
        {
            // First add a convenor

            var serviceA = new UserService();

            var requestA = new AddUpdateUserRequest()
            {
                UserID = StringGenerator.GenerateAlphaString(10),
                Password = StringGenerator.GenerateAlphaString(32),
                FirstName = StringGenerator.GenerateAlphaString(32),
                LastName = StringGenerator.GenerateAlphaString(32),
                Role = "Convenor"
            };

            var responseA = serviceA.AddUpdateUser(requestA);

            Assert.AreEqual(HttpStatusCode.OK, responseA.Status);

            // Then Add a unit and assign the newly created convenor

            var serviceB = new UnitService();

            var requestB = new AddUpdateUnitRequest()
            {
                UnitCode = StringGenerator.GenerateAlphaString(3) + StringGenerator.GenerateNumericString(5),
                Name = StringGenerator.GenerateAlphaString(32),
                ConvenorID = requestA.UserID
            };

            var responseB = serviceB.AddUpdateUnit(requestB);

            Assert.AreEqual(HttpStatusCode.OK, responseB.Status);
        }

        /// <summary>
        /// Test getting all units.
        /// </summary>
        [TestMethod]
        public void TestGetUnits()
        {
            // First add a convenor

            var serviceA = new UserService();

            var requestA = new AddUpdateUserRequest()
            {
                UserID = StringGenerator.GenerateAlphaString(10),
                Password = StringGenerator.GenerateAlphaString(32),
                FirstName = StringGenerator.GenerateAlphaString(32),
                LastName = StringGenerator.GenerateAlphaString(32),
                Role = "Convenor"
            };

            var responseA = serviceA.AddUpdateUser(requestA);

            Assert.AreEqual(HttpStatusCode.OK, responseA.Status);

            // Then Add a unit and assign the newly created convenor

            var serviceB = new UnitService();

            var requestB = new AddUpdateUnitRequest()
            {
                UnitCode = StringGenerator.GenerateAlphaString(3) + StringGenerator.GenerateNumericString(5),
                Name = StringGenerator.GenerateAlphaString(32),
                ConvenorID = requestA.UserID
            };

            var responseB = serviceB.AddUpdateUnit(requestB);

            Assert.AreEqual(HttpStatusCode.OK, responseB.Status);

            // Add another unit

            var requestC = new AddUpdateUnitRequest()
            {
                UnitCode = StringGenerator.GenerateAlphaString(3) + StringGenerator.GenerateNumericString(5),
                Name = StringGenerator.GenerateAlphaString(32),
                ConvenorID = requestA.UserID
            };

            var responseC = serviceB.AddUpdateUnit(requestC);
            
            Assert.AreEqual(HttpStatusCode.OK, responseC.Status);

            // Get the units

            var requestD = new GetUnitsRequest();

            var responseD = serviceB.GetUnits(requestD);

            Assert.AreEqual(HttpStatusCode.OK, responseD.Status);

            // Check that the units that were added in this test are in the list of units.

            bool gotUnitFromRequestB = false;
            bool gotUnitFromRequestC = false;
            foreach (UnitDto u in responseD.Units)
            {
                if (u.UnitCode == requestB.UnitCode) gotUnitFromRequestB = true;
                if (u.UnitCode == requestC.UnitCode) gotUnitFromRequestC = true;
            }

            Assert.IsTrue(gotUnitFromRequestB);
            Assert.IsTrue(gotUnitFromRequestC);
        }

        [TestMethod]
        public void TestDeleteUnit()
        {
            // First add a convenor

            var serviceA = new UserService();

            var requestA = new AddUpdateUserRequest()
            {
                UserID = StringGenerator.GenerateAlphaString(10),
                Password = StringGenerator.GenerateAlphaString(32),
                FirstName = StringGenerator.GenerateAlphaString(32),
                LastName = StringGenerator.GenerateAlphaString(32),
                Role = "Convenor"
            };

            var responseA = serviceA.AddUpdateUser(requestA);

            Assert.AreEqual(HttpStatusCode.OK, responseA.Status);

            // Then Add a unit and assign the newly created convenor

            var serviceB = new UnitService();

            var requestB = new AddUpdateUnitRequest()
            {
                UnitCode = StringGenerator.GenerateAlphaString(3) + StringGenerator.GenerateNumericString(5),
                Name = StringGenerator.GenerateAlphaString(32),
                ConvenorID = requestA.UserID
            };

            var responseB = serviceB.AddUpdateUnit(requestB);

            Assert.AreEqual(HttpStatusCode.OK, responseB.Status);

            // Then delete the unit

            var requestC = new DeleteUnitRequest()
            {
                UnitCode = requestB.UnitCode
            };

            var responseC = serviceB.DeleteUnit(requestC);

            Assert.AreEqual(HttpStatusCode.OK, responseC.Status);
        }
    }
}