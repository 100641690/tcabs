﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tcabs.Logic;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Utilities;
using System.Net;
using Tcabs.Common.Dtos;

namespace Tcabs.Testing
{
    [TestClass]
    public class TeamTests
    {
        [TestMethod]
        public void TestAddTeamValid()
        {
            // First add a supervisor

            var userService = new UserService();

            var requestA = new AddUpdateUserRequest()
            {
                //TeamID = StringGenerator.GenerateNumericString(10),
                //UserID = StringGenerator.GenerateAlphaString(10),
                //FirstName = StringGenerator.GenerateAlphaString(32),
                //LastName = StringGenerator.GenerateAlphaString(32),
                //Role = "Supervisor"
                UserID = StringGenerator.GenerateAlphaString(10),
                Password = StringGenerator.GenerateAlphaString(32),
                FirstName = StringGenerator.GenerateAlphaString(32),
                LastName = StringGenerator.GenerateAlphaString(32),
                Role = "Supervisor"
            };

            var responseUserService = userService.AddUpdateUser(requestA);
            Assert.AreEqual(HttpStatusCode.OK, responseUserService.Status);
            

           

            //var requestb = new AddUpdateTeamRequest()
            //{
            //    IsUpdate = false,
            //    TeamID = 25,
            //    SupervisorID = StringGenerator.GenerateAlphaString(32),
            //    Name = StringGenerator.GenerateAlphaString(32),
            //};

            //var responseA = serviceA.AddUpdateTeam(requestb);

            //Assert.AreEqual(HttpStatusCode.OK, responseA.Status);

            // Then Add a team and assign the newly created supervisor

            var serviceB = new TeamService();

            var requestB = new AddUpdateTeamRequest()
            {
                //TeamID = 30,
                Name = StringGenerator.GenerateAlphaString(32),
                SupervisorID = requestA.UserID
            };

            var responseB = serviceB.AddUpdateTeam(requestB);

            Assert.AreEqual(HttpStatusCode.OK, responseB.Status);
        }

        /// <summary>
        /// Test getting all teams.
        /// </summary>
        [TestMethod]
        public void TestGetTeams()
        {
            // First add a supervisor

            var serviceA = new UserService();

            var requestA = new AddUpdateUserRequest()
            {
                UserID = StringGenerator.GenerateAlphaString(10),
                Password = StringGenerator.GenerateAlphaString(32),
                FirstName = StringGenerator.GenerateAlphaString(32),
                LastName = StringGenerator.GenerateAlphaString(32),
                Role = "Supervisor"
            };

            var responseA = serviceA.AddUpdateUser(requestA);

            Assert.AreEqual(HttpStatusCode.OK, responseA.Status);

            // Then Add a unit and assign the newly created convenor

            var serviceB = new TeamService();

            var requestB = new AddUpdateTeamRequest()
            {
                //TeamID = StringGenerator.GenerateNumericString(10),
                Name = StringGenerator.GenerateAlphaString(32),
                SupervisorID = requestA.UserID
            };

            var responseB = serviceB.AddUpdateTeam(requestB);

            Assert.AreEqual(HttpStatusCode.OK, responseB.Status);

            // Add another Team

            var requestC = new AddUpdateTeamRequest()
            {
                //TeamID = StringGenerator.GenerateNumericString(10),
                Name = StringGenerator.GenerateAlphaString(32),
                SupervisorID = requestA.UserID
            };

            var responseC = serviceB.AddUpdateTeam(requestC);
            
            Assert.AreEqual(HttpStatusCode.OK, responseC.Status);

            // Get the team

            var requestD = new GetTeamsRequest();

            var responseD = serviceB.GetTeams(requestD);

            Assert.AreEqual(HttpStatusCode.OK, responseD.Status);

            // Check that the units that were added in this test are in the list of units.

            bool gotTeamFromRequestB = false;
            bool gotTeamFromRequestC = false;
            foreach (TeamDto u in responseD.Teams)
            {
                if (u.Name == requestB.Name) gotTeamFromRequestB = true;
                if (u.Name == requestC.Name) gotTeamFromRequestC = true;
            }

            Assert.IsTrue(gotTeamFromRequestB);
            Assert.IsTrue(gotTeamFromRequestC);
        }

        [TestMethod]
        public void TestDeleteTeam()
        {
            // First add a supervisor

            var serviceA = new UserService();

            var requestA = new AddUpdateUserRequest()
            {
                UserID = StringGenerator.GenerateAlphaString(10),
                Password = StringGenerator.GenerateAlphaString(32),
                FirstName = StringGenerator.GenerateAlphaString(32),
                LastName = StringGenerator.GenerateAlphaString(32),
                Role = "Supervisor"
            };

            var responseA = serviceA.AddUpdateUser(requestA);

            Assert.AreEqual(HttpStatusCode.OK, responseA.Status);

            // Then Add a team and assign the newly created convenor

            var serviceB = new TeamService();

            var requestB = new AddUpdateTeamRequest()
            {
                TeamID = 928374,
                Name = StringGenerator.GenerateAlphaString(32),
                SupervisorID = requestA.UserID
            };

            var responseB = serviceB.AddUpdateTeam(requestB);

            Assert.AreEqual(HttpStatusCode.OK, responseB.Status);

            // Then delete the team

            var requestC = new DeleteTeamRequest()
            {
                TeamID = requestB.TeamID
            };

            var responseC = serviceB.DeleteTeam(requestC);

            Assert.AreEqual(HttpStatusCode.OK, responseC.Status);
        }
    }
}