using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tcabs.Logic;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Utilities;
using System.Net;
using Tcabs.Common.Dtos;

namespace Tcabs.Testing
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        public void TestLoginValid()
        {
            var service = new UserService();
            var request = new AuthenticateUserRequest()
            {
                UserID = "Admin",
                Password = "Abc123"
            };
            var response = service.AuthenticateUser(request);

            Assert.IsNotNull(response.Permissions);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
        }

        [TestMethod]
        public void TestLoginInvalid()
        {
            var service = new UserService();
            var request = new AuthenticateUserRequest()
            {
                UserID = "Adminnnnn",
                Password = "Overlord202020"
            };
            var response = service.AuthenticateUser(request);

            Assert.IsNull(response.Permissions);
            Assert.AreEqual(HttpStatusCode.InternalServerError, response.Status);
        }

        [TestMethod]
        public void TestAddUserStudentValid()
        {
            var service = new UserService();

            var request = new AddUpdateUserRequest()
            {
                UserID = StringGenerator.GenerateNumericString(10),
                Password = StringGenerator.GenerateAlphaString(32),
                FirstName = StringGenerator.GenerateAlphaString(32),
                LastName = StringGenerator.GenerateAlphaString(32),
                Role = "Student"
            };

            var response = service.AddUpdateUser(request);

            Assert.AreEqual(HttpStatusCode.OK, response.Status);
        }

        [TestMethod]
        public void TestAddUserStudentInvalid()
        {
            var service = new UserService();

            var request = new AddUpdateUserRequest()
            {
                UserID = "Clown",
                Password = "a",
                FirstName = "hglehbvkrdhrkghsvkysughjlkgvkhejfbvhjekabvkhjb",
                LastName = "gsvkhrjgvhkjsgvkjhghjsvbshkjbvhkjsbvhjkbsrhjvb",
                Role = "Student"
            };

            var response = service.AddUpdateUser(request);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.Status);
        }

        [TestMethod]
        public void TestGetUsers()
        {
            var service = new UserService();

            var request = new GetUsersRequest()
            {
            };

            var response = service.GetUsers(request);

            Assert.AreEqual(HttpStatusCode.OK, response.Status);
        }

        [TestMethod]
        public void TestGetAllStudents()
        {
            var service = new UserService();

            var request = new GetUsersByRoleRequest()
            {
                Name = "Student"
            };

            var response = service.GetUsersByRole(request);

            foreach(UserDto u in response.Users)
            {
                Assert.AreEqual("Student", u.Role.Name);
            }

            Assert.AreEqual(HttpStatusCode.OK, response.Status);
        }
    }
}
