﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tcabs.Logic;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Utilities;
using System.Net;
using Tcabs.Common.Dtos;

namespace Tcabs.Testing
{
    [TestClass]
    public class ProjectTests
    {
        [TestMethod]
        public void TestAddProjectValid()
        {
            // First add a project

            //var userService = new UserService();

            //var requestA = new AddUpdateUserRequest()
            //{
            //    //TeamID = StringGenerator.GenerateNumericString(10),
            //    //UserID = StringGenerator.GenerateAlphaString(10),
            //    //FirstName = StringGenerator.GenerateAlphaString(32),
            //    //LastName = StringGenerator.GenerateAlphaString(32),
            //    //Role = "Supervisor"
            //    UserID = StringGenerator.GenerateAlphaString(10),
            //    Password = StringGenerator.GenerateAlphaString(32),
            //    FirstName = StringGenerator.GenerateAlphaString(32),
            //    LastName = StringGenerator.GenerateAlphaString(32),
            //    Role = "Supervisor"
            //};

            //var responseUserService = userService.AddUpdateUser(requestA);
            //Assert.AreEqual(HttpStatusCode.OK, responseUserService.Status);
            

           

            //var requestb = new AddUpdateTeamRequest()
            //{
            //    IsUpdate = false,
            //    TeamID = 25,
            //    SupervisorID = StringGenerator.GenerateAlphaString(32),
            //    Name = StringGenerator.GenerateAlphaString(32),
            //};

            //var responseA = serviceA.AddUpdateTeam(requestb);

            //Assert.AreEqual(HttpStatusCode.OK, responseA.Status);

            // Then Add a team and assign the newly created supervisor

            var serviceB = new ProjectService();

            var requestB = new AddUpdateProjectRequest()
            {
                //ProjectID = 30,
                Name = StringGenerator.GenerateAlphaString(32),
                Description = StringGenerator.GenerateAlphaString(32)
            };

            var responseB = serviceB.AddUpdateProject(requestB);

            Assert.AreEqual(HttpStatusCode.OK, responseB.Status);
        }

        /// <summary>
        /// Test getting all teams.
        /// </summary>
        [TestMethod]
        public void TestGetProject()
        {
            // First add a supervisor

            //var serviceA = new UserService();

            //var requestA = new AddUpdateUserRequest()
            //{
            //    UserID = StringGenerator.GenerateAlphaString(10),
            //    Password = StringGenerator.GenerateAlphaString(32),
            //    FirstName = StringGenerator.GenerateAlphaString(32),
            //    LastName = StringGenerator.GenerateAlphaString(32),
            //    Role = "Supervisor"
            //};

            //var responseA = serviceA.AddUpdateUser(requestA);

            //Assert.AreEqual(HttpStatusCode.OK, responseA.Status);

            // Then Add a unit and assign the newly created convenor

            var serviceB = new ProjectService();

            var requestB = new AddUpdateProjectRequest()
            {
                //TeamID = StringGenerator.GenerateNumericString(10),
                Name = StringGenerator.GenerateAlphaString(32),
                Description = StringGenerator.GenerateAlphaString(32)
            };

            var responseB = serviceB.AddUpdateProject(requestB);

            Assert.AreEqual(HttpStatusCode.OK, responseB.Status);

            // Add another Team

            var requestC = new AddUpdateProjectRequest()
            {
                //TeamID = StringGenerator.GenerateNumericString(10),
                Name = StringGenerator.GenerateAlphaString(32),
                Description = StringGenerator.GenerateAlphaString(32)
            };

            var responseC = serviceB.AddUpdateProject(requestC);
            
            Assert.AreEqual(HttpStatusCode.OK, responseC.Status);

            // Get the team

            var requestD = new GetProjectsRequest();

            var responseD = serviceB.GetProjects(requestD);

            Assert.AreEqual(HttpStatusCode.OK, responseD.Status);

            // Check that the units that were added in this test are in the list of units.

            bool gotProjectFromRequestB = false;
            bool gotProjectFromRequestC = false;
            foreach (ProjectDto u in responseD.Projects)
            {
                if (u.Name == requestB.Name) gotProjectFromRequestB = true;
                if (u.Name == requestC.Name) gotProjectFromRequestC = true;
            }

            Assert.IsTrue(gotProjectFromRequestB);
            Assert.IsTrue(gotProjectFromRequestC);
        }

        [TestMethod]
        public void TestDeleteProject()
        {
            //// First add a supervisor

            //var serviceA = new UserService();

            //var requestA = new AddUpdateUserRequest()
            //{
            //    UserID = StringGenerator.GenerateAlphaString(10),
            //    Password = StringGenerator.GenerateAlphaString(32),
            //    FirstName = StringGenerator.GenerateAlphaString(32),
            //    LastName = StringGenerator.GenerateAlphaString(32),
            //    Role = "Supervisor"
            //};

            //var responseA = serviceA.AddUpdateUser(requestA);

            //Assert.AreEqual(HttpStatusCode.OK, responseA.Status);

            //// Then Add a team and assign the newly created convenor

            var serviceB = new ProjectService();

            var requestB = new AddUpdateProjectRequest()
            {

                Name = StringGenerator.GenerateAlphaString(32),
                Description = StringGenerator.GenerateAlphaString(32)
            };

            var responseB = serviceB.AddUpdateProject(requestB);

            Assert.AreEqual(HttpStatusCode.OK, responseB.Status);

            // Then delete the team

            var requestC = new DeleteProjectRequest()
            {
                ProjectID = requestB.ProjectID
            };

            var responseC = serviceB.DeleteProject(requestC);

            Assert.AreEqual(HttpStatusCode.OK, responseC.Status);
        }
    }
}