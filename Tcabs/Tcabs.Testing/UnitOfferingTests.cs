﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tcabs.Logic;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Utilities;
using System.Net;
using Tcabs.Common.Dtos;

namespace Tcabs.Testing
{
    [TestClass]
    public class UnitOfferingTests
    {
        [TestMethod]
        public void TestAddUnitOffering()
        {
            // First add a convenor

            var serviceA = new UserService();

            var requestA = new AddUpdateUserRequest()
            {
                UserID = StringGenerator.GenerateAlphaString(10),
                Password = StringGenerator.GenerateAlphaString(32),
                FirstName = StringGenerator.GenerateAlphaString(32),
                LastName = StringGenerator.GenerateAlphaString(32),
                Role = "Convenor"
            };

            var responseA = serviceA.AddUpdateUser(requestA);

            Assert.AreEqual(HttpStatusCode.OK, responseA.Status);

            // Then Add a unit and assign the newly created convenor

            var serviceB = new UnitService();

            var requestB = new AddUpdateUnitRequest()
            {
                UnitCode = StringGenerator.GenerateAlphaString(3) + StringGenerator.GenerateNumericString(5),
                Name = StringGenerator.GenerateAlphaString(32),
                ConvenorID = requestA.UserID
            };

            var responseB = serviceB.AddUpdateUnit(requestB);

            Assert.AreEqual(HttpStatusCode.OK, responseB.Status);

            var serviceC = new UnitOfferingService();

            var requestC = new AddUpdateUnitOfferingRequest()
            {
                UnitCode = requestB.UnitCode,
                TeachingPeriodID = 1,
                Year = 2021
            };

            var responseC = serviceC.AddUpdateUnitOffering(requestC);

            Assert.AreEqual(HttpStatusCode.OK, responseC.Status);
        }

        [TestMethod]
        public void TestGetUnitOfferings()
        {
            // First add a convenor

            var serviceA = new UserService();

            var requestA = new AddUpdateUserRequest()
            {
                UserID = StringGenerator.GenerateAlphaString(10),
                Password = StringGenerator.GenerateAlphaString(32),
                FirstName = StringGenerator.GenerateAlphaString(32),
                LastName = StringGenerator.GenerateAlphaString(32),
                Role = "Convenor"
            };

            var responseA = serviceA.AddUpdateUser(requestA);

            Assert.AreEqual(HttpStatusCode.OK, responseA.Status);

            var requestB = new AddUpdateUserRequest()
            {
                UserID = StringGenerator.GenerateAlphaString(10),
                Password = StringGenerator.GenerateAlphaString(32),
                FirstName = StringGenerator.GenerateAlphaString(32),
                LastName = StringGenerator.GenerateAlphaString(32),
                Role = "Convenor"
            };

            var responseB = serviceA.AddUpdateUser(requestB);

            Assert.AreEqual(HttpStatusCode.OK, responseB.Status);

            // Then Add a unit and assign the newly created convenor

            var serviceC = new UnitService();

            var requestC = new AddUpdateUnitRequest()
            {
                UnitCode = StringGenerator.GenerateAlphaString(3) + StringGenerator.GenerateNumericString(5),
                Name = StringGenerator.GenerateAlphaString(32),
                ConvenorID = requestA.UserID
            };

            var responseC = serviceC.AddUpdateUnit(requestC);

            Assert.AreEqual(HttpStatusCode.OK, responseC.Status);

            var serviceD = new UnitOfferingService();

            var requestD = new AddUpdateUnitOfferingRequest()
            {
                UnitCode = requestC.UnitCode,
                TeachingPeriodID = 1,
                Year = 2021
            };

            var responseD = serviceD.AddUpdateUnitOffering(requestD);

            Assert.AreEqual(HttpStatusCode.OK, responseD.Status);

            var requestE = new GetUnitOfferingsRequest();

            var responseE = serviceD.GetUnitOfferings(requestE);

            Assert.AreEqual(HttpStatusCode.OK, responseE.Status);
        }
    }
}