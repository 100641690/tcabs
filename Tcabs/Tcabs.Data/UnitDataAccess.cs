﻿using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Utilities;
using Tcabs.Common.Dtos;

namespace Tcabs.Data
{
    public class UnitDataAccess : DataAccess
    {
        private MySqlTransaction _transaction;
        private MySqlCommand _command;

        /// <summary>
        /// Calls the stored procedure to add a unit to the database.
        /// </summary>
        /// <param name="request"></param>
        public void AddUpdateUnit(AddUpdateUnitRequest request)
        {
            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            if (request.IsUpdate)
            {
                _command = new MySqlCommand("UPDATE_UNIT", Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
            }
            else
            {
                _command = new MySqlCommand("ADD_UNIT", Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
            }

            _command.Parameters.Add("pUnitCode", MySqlDbType.VarChar).Value = request.UnitCode.ToUpper();
            _command.Parameters.Add("pConvenorID", MySqlDbType.VarChar).Value = request.ConvenorID.ToLower();
            _command.Parameters.Add("pName", MySqlDbType.VarChar).Value = request.Name;          

            // Start transaction and execute command
            try
            {
                // Execute
                _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                _command.ExecuteNonQuery();
                _transaction.Commit();
            }
            catch
            {
                // Command failed, rollback the database
                _transaction.Rollback();
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
        }

        /// <summary>
        /// Call stored procedure to retrieve all units from the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<UnitDto> GetUnits(GetUnitsRequest request)
        {
            var result = new List<UnitDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_ALL_UNITS", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    var convenor = new UserDto()
                    {
                        Role = new RoleDto()
                    };

                    convenor.UserID = (string)row["UserID"];
                    convenor.FirstName = (string)row["FirstName"];
                    convenor.LastName = (string)row["LastName"];
                    convenor.Role.RoleID = Convert.ToUInt32(row["RoleID"]);
                    convenor.Role.Name = (string)row["RoleName"];

                    result.Add(new UnitDto()
                    {
                        UnitCode = (string)row["UnitCode"],
                        Convenor = convenor,
                        Name = (string)row["UnitName"]
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }

        /// <summary>
        /// Call stored procedure to delete a unit from the database by UnitCode.
        /// </summary>
        /// <param name="request"></param>
        public void DeleteUnit(DeleteUnitRequest request)
        {
            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("DELETE_UNIT", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            _command.Parameters.Add("pUnitCode", MySqlDbType.VarChar).Value = request.UnitCode.ToUpper();

            try
            {
                // Execute
                _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                _command.ExecuteNonQuery();
                _transaction.Commit();
            }
            catch
            {
                // Command failed
                _transaction.Rollback();
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
        }
    }
}
