﻿using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Utilities;
using Tcabs.Common.Dtos;

namespace Tcabs.Data
{
    public class UserDataAccess : DataAccess
    {
        private MySqlTransaction _transaction;
        private MySqlCommand _command;

        /// <summary>
        /// Calls the stored procedure to verify user credentials on the database for login request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<PermissionDto> AuthenticateUser(AuthenticateUserRequest request)
        {
            // List of permissions to return
            var result = new List<PermissionDto>();

            // Connect to database
            Connect();

            // Encrypt password
            string encryptedPassword = Encryptor.Encrypt(request.Password);

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("AUTHENTICATE_USER", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            _command.Parameters.Add("pUserID", MySqlDbType.VarChar).Value = request.UserID.ToLower();
            _command.Parameters.Add("pPassword", MySqlDbType.VarChar).Value = encryptedPassword;

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    result.Add(new PermissionDto()
                    {
                        PermissionID = Convert.ToUInt32(row["PermissionID"]),
                        Name = row["Name"].ToString()
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
            return result;
        }

        /// <summary>
        /// Calls the stored procedure to add a user to the database.
        /// </summary>
        /// <param name="request"></param>
        public void AddUpdateUser(AddUpdateUserRequest request)
        {
            // Connect to database
            Connect();

            // Encrypt password
            string encryptedPassword = Encryptor.Encrypt(request.Password);

            // Create the command and specify the stored procedure to call
            if (request.IsUpdate)
            {
                _command = new MySqlCommand("UPDATE_USER", Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
            }
            else
            {
                _command = new MySqlCommand("ADD_USER", Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
            }

            _command.Parameters.Add("pUserID", MySqlDbType.VarChar).Value = request.UserID.ToLower();
            if (request.IsUpdate && request.Password.Length == 0)
            {
                // Empty password; don't update
                _command.Parameters.Add("pPassword", MySqlDbType.VarChar).Value = request.Password;
            }
            else
            {
                // Password has length. use new encrypted password
                _command.Parameters.Add("pPassword", MySqlDbType.VarChar).Value = encryptedPassword;
            }
            _command.Parameters.Add("pFirstName", MySqlDbType.VarChar).Value = request.FirstName;
            _command.Parameters.Add("pLastName", MySqlDbType.VarChar).Value = request.LastName;
            _command.Parameters.Add("pRole", MySqlDbType.VarChar).Value = request.Role;

            // Start transaction and execute command
            try
            {
                // Execute
                _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                _command.ExecuteNonQuery();
                _transaction.Commit();
            }
            catch
            {
                // Command failed, rollback the database
                _transaction.Rollback();
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
        }

        /// <summary>
        /// Call the stored procedure to retrieves a user from the database by UserID.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public UserDto GetUser(GetUserRequest request)
        {
            // User details
            var result = new UserDto();
            result.Role = new RoleDto();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_USER", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            _command.Parameters.Add("pUserID", MySqlDbType.VarChar).Value = request.UserID.ToLower();
            _command.Parameters.Add("rFirstName", MySqlDbType.VarChar).Direction = ParameterDirection.Output;
            _command.Parameters.Add("rLastName", MySqlDbType.VarChar).Direction = ParameterDirection.Output;
            _command.Parameters.Add("rRoleID", MySqlDbType.VarChar).Direction = ParameterDirection.Output;
            _command.Parameters.Add("rRoleName", MySqlDbType.VarChar).Direction = ParameterDirection.Output;

            try
            {
                // Execute
                _command.ExecuteNonQuery();

                result.UserID = request.UserID;
                result.FirstName = _command.Parameters["rFirstName"].Value.ToString();
                result.LastName = _command.Parameters["rLastName"].Value.ToString();
                result.Role.RoleID = Convert.ToUInt32(_command.Parameters["rRoleID"].Value);
                result.Role.Name = _command.Parameters["rRoleName"].Value.ToString();
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
            return result;
        }

        /// <summary>
        /// Call stored procedure to retrieve all users from the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<UserDto> GetUsers(GetUsersRequest request)
        {
            var result = new List<UserDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_ALL_USERS", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    var role = new RoleDto()
                    {
                        RoleID = Convert.ToUInt32(row["RoleID"]),
                        Name = (string)row["Name"]
                    };

                    result.Add(new UserDto()
                    {
                        UserID = (string)row["UserID"],
                        FirstName = (string)row["FirstName"],
                        LastName = (string)row["LastName"],
                        Role = role
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }

        /// <summary>
        /// Call stored procedure to retrieve all users of a specific role from the database
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<UserDto> GetUsersByRole(GetUsersByRoleRequest request)
        {
            var result = new List<UserDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_USERS_BY_ROLE", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            _command.Parameters.Add("pName", MySqlDbType.VarChar).Value = request.Name;

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    var role = new RoleDto()
                    {
                        RoleID = Convert.ToUInt32(row["RoleID"]),
                        Name = (string)row["Name"]
                    };

                    result.Add(new UserDto()
                    {
                        UserID = (string)row["UserID"],
                        FirstName = (string)row["FirstName"],
                        LastName = (string)row["LastName"],
                        Role = role
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }

        /// <summary>
        /// Call stored procedure to retrieve all users based on permissions associated with their role from the database
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<UserDto> GetUsersByPermission(GetUsersByPermissionRequest request)
        {
            var result = new List<UserDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_USERS_BY_PERMISSION", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            _command.Parameters.Add("pName", MySqlDbType.VarChar).Value = request.Name;

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    var role = new RoleDto()
                    {
                        RoleID = Convert.ToUInt32(row["RoleID"]),
                        Name = (string)row["Name"]
                    };

                    result.Add(new UserDto()
                    {
                        UserID = (string)row["UserID"],
                        FirstName = (string)row["FirstName"],
                        LastName = (string)row["LastName"],
                        Role = role
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }

        /// <summary>
        /// Call stored procedure to delete a user from the database by UserID.
        /// </summary>
        /// <param name="request"></param>
        public void DeleteUser(DeleteUserRequest request)
        {
            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("DELETE_USER", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            _command.Parameters.Add("pUserID", MySqlDbType.VarChar).Value = request.UserID.ToLower();

            try
            {
                // Execute
                _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                _command.ExecuteNonQuery();
                _transaction.Commit();
            }
            catch
            {
                // Command failed
                _transaction.Rollback();
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
        }
    }
}
