﻿using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Utilities;
using Tcabs.Common.Dtos;

namespace Tcabs.Data
{
    public class ProjectDataAccess : DataAccess
    {
        private MySqlTransaction _transaction;
        private MySqlCommand _command;

    
       
        public void AddUpdateProject(AddUpdateProjectRequest request)
        {
            // Connect to database
            Connect();

            if (request.IsUpdate)
            {
                _command = new MySqlCommand("UPDATE_PROJECT", Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
            }
            else
            {
                _command = new MySqlCommand("ADD_PROJECT", Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
            }

            _command.Parameters.Add("pName", MySqlDbType.VarChar).Value = request.Name;
            _command.Parameters.Add("pDescription", MySqlDbType.VarChar).Value = request.Description;
            _command.Parameters.Add("pProjectId", MySqlDbType.Int64).Value = request.ProjectID;

            // Start transaction and execute command
            try
            {
                // Execute
                _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                _command.ExecuteNonQuery();
                _transaction.Commit();
            }
            catch
            {
                // Command failed, rollback the database
                _transaction.Rollback();
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
        }

        public List<ProjectDto> GetProjects (GetProjectsRequest request)
        {
            var result = new List<ProjectDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_ALL_PROJECTS", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {

                    result.Add(new ProjectDto()
                    {
                        ProjectID = (int)row["ProjectID"],
                        Name = (string)row["Name"],
                        Description = (string)row["description"]

                    }); ;

                    //ProjectDto temp = new ProjectDto();
                    //temp.ProjectID = (int)row["ProjectID"];
                    //temp.Name = (string)row["Name"];
                    //var c = row["Description"];

                    //if (typeof row["Description"] == )
                    //{
                    //    temp.Description = "";
                    //}
                    //else
                    //{
                    //    temp.Description = (string)row["description"];

                    //}
                    //result.Add(temp);
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }

        public void DeleteProject(DeleteProjectRequest request)
        {
            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("DELETE_PROJECT", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            _command.Parameters.Add("pProjectID", MySqlDbType.Int32).Value = request.ProjectID;

            try
            {
                // Execute
                _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                _command.ExecuteNonQuery();
                _transaction.Commit();
            }
            catch
            {
                // Command failed
                _transaction.Rollback();
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
        }
    }
}
