﻿using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Utilities;
using Tcabs.Common.Dtos;

namespace Tcabs.Data
{
    public class UnitOfferingDataAccess : DataAccess
    {
        private MySqlTransaction _transaction;
        private MySqlCommand _command;
        
        public void AddUpdateUnitOffering(AddUpdateUnitOfferingRequest request)
        {
            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            if (request.UnitOfferingID != null)
            {
                _command = new MySqlCommand("UPDATE_UNIT_OFFERING", Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                _command.Parameters.Add("pUnitOfferingID", MySqlDbType.UInt32).Value = request.UnitOfferingID;
            }
            else
            {
                _command = new MySqlCommand("ADD_UNIT_OFFERING", Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
            }

            _command.Parameters.Add("pUnitCode", MySqlDbType.VarChar).Value = request.UnitCode.ToUpper();
            _command.Parameters.Add("pTeachingPeriodID", MySqlDbType.UInt32).Value = request.TeachingPeriodID;
            _command.Parameters.Add("pYear", MySqlDbType.UInt32).Value = request.Year;

            // Start transaction and execute command
            try
            {
                // Execute
                _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                _command.ExecuteNonQuery();
                _transaction.Commit();
            }
            catch
            {
                // Command failed, rollback the database
                _transaction.Rollback();
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
        }

        public List<UnitOfferingDto> GetUnitOfferings(GetUnitOfferingsRequest request)
        {
            var result = new List<UnitOfferingDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_ALL_UNIT_OFFERINGS", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    var unit = new UnitDto();
                    var teachingPeriod = new TeachingPeriodDto();
                    var teachingYear = new TeachingYearDto();

                    unit.UnitCode = (string)row["UnitCode"];
                    unit.Name = (string)row["UnitName"];
                    teachingPeriod.TeachingPeriodID = Convert.ToUInt32(row["TeachingPeriodID"]);
                    teachingPeriod.Name = (string)row["TeachingPeriodName"];
                    teachingYear.Year = Convert.ToUInt32(row["Year"]);

                    result.Add(new UnitOfferingDto()
                    {
                        UnitOfferingID = Convert.ToUInt32(row["UnitOfferingID"]),
                        Unit = unit,
                        TeachingPeriod = teachingPeriod,
                        TeachingYear = teachingYear
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }

        public void DeleteUnitOffering(DeleteUnitOfferingRequest request)
        {
            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("DELETE_UNIT_OFFERING", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            _command.Parameters.Add("pUnitOfferingID", MySqlDbType.UInt32).Value = request.UnitOfferingID;

            try
            {
                // Execute
                _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                _command.ExecuteNonQuery();
                _transaction.Commit();
            }
            catch
            {
                // Command failed
                _transaction.Rollback();
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
        }

        public List<TeachingPeriodDto> GetTeachingPeriods(GetTeachingPeriodsRequest request)
        {
            var result = new List<TeachingPeriodDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_ALL_TEACHING_PERIODS", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    result.Add(new TeachingPeriodDto()
                    {
                        TeachingPeriodID = Convert.ToUInt32(row["TeachingPeriodID"]),
                        Name = (string)row["Name"]
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }

        public List<TeachingYearDto> GetTeachingYears(GetTeachingYearsRequest request)
        {
            var result = new List<TeachingYearDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_ALL_TEACHING_YEARS", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    result.Add(new TeachingYearDto()
                    {
                        Year = Convert.ToUInt32(row["Year"])
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }
    }
}
