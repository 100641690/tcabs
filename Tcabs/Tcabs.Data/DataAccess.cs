﻿using System;
using MySql.Data.MySqlClient;
using Tcabs.Common;

namespace Tcabs.Data
{
    public abstract class DataAccess
    {
        private AppSettings _appSettings;

        public DataAccess()
        {
            _appSettings = new AppSettings();
            Connection = new MySqlConnection(_appSettings.Database);
        }

        public void Connect()
        {
            try
            {
                Connection.Open();
            }
            catch
            {
                throw;
            }
        }

        public void Disconnect()
        {
            try
            {
                Connection.Close();
            }
            catch
            {
                throw;
            }
        }

        public MySqlConnection Connection { get; }
    }
}
