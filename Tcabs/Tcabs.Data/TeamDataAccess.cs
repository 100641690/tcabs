﻿using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Utilities;
using Tcabs.Common.Dtos;

namespace Tcabs.Data
{
    public class TeamDataAccess : DataAccess
    {
        private MySqlTransaction _transaction;
        private MySqlCommand _command;

    
       
        public void AddUpdateTeam(AddUpdateTeamRequest request)
        {
            // Connect to database
            Connect();

            if (request.IsUpdate)
            {
                _command = new MySqlCommand("UPDATE_TEAM", Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                
            }
            else
            {
                _command = new MySqlCommand("ADD_TEAM", Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
            }

            _command.Parameters.Add("pTeamID", MySqlDbType.Int64).Value = request.TeamID;
            _command.Parameters.Add("pSupervisorID", MySqlDbType.VarChar).Value = request.SupervisorID;
            _command.Parameters.Add("pName", MySqlDbType.VarChar).Value = request.Name;
            

            // Start transaction and execute command
            try
            {
                // Execute
                _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                _command.ExecuteNonQuery();
                _transaction.Commit();
            }
            catch
            {
                // Command failed, rollback the database
                _transaction.Rollback();
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
        }

        public List<TeamDto> GetTeams (GetTeamsRequest request)
        {
            var result = new List<TeamDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_ALL_TEAMS", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                //_command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    var supervisor = new UserDto()
                    {
                        Role = new RoleDto()
               
                    };

                    supervisor.UserID = (string)row["UserID"];
                    supervisor.FirstName = (string)row["FirstName"];
                    supervisor.LastName = (string)row["LastName"];
                    supervisor.Role.RoleID = Convert.ToUInt32(row["RoleID"]);
                    supervisor.Role.Name = (string)row["RoleName"];

                    result.Add(new TeamDto()
                    {
                        TeamID = (int)row["TeamID"],
                        Supervisor = supervisor,
                        Name = (string)row["TeamName"],
                      
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }

        public void DeleteTeam(DeleteTeamRequest request)
        {
            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("DELETE_TEAM", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            _command.Parameters.Add("pTeamID", MySqlDbType.Int32).Value = request.TeamID;

            try
            {
                // Execute
                _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                _command.ExecuteNonQuery();
                _transaction.Commit();
            }
            catch
            {
                // Command failed
                _transaction.Rollback();
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
        }
    }
}
