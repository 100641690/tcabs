﻿using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using Tcabs.Common.Utilities;
using Tcabs.Common.Dtos;

namespace Tcabs.Data
{
    public class EnrolmentDataAccess : DataAccess
    {
        private MySqlTransaction _transaction;
        private MySqlCommand _command;

        /// <summary>
        /// Calls the stored procedure to add and remove student enrolments in the database.
        /// </summary>
        /// <param name="request"></param>
        public void AddUpdateEnrolments(AddUpdateEnrolmentsRequest request)
        {
            // Connect to database
            Connect();

            // Start transaction and execute command
            try
            {
                // Execute
                _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);

                foreach (string s in request.StudentsToEnrol)
                {
                    _command = new MySqlCommand("ADD_ENROLMENT", Connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    _command.Parameters.Add("pUnitOfferingID", MySqlDbType.Int32).Value = request.UnitOfferingID;
                    _command.Parameters.Add("pStudentID", MySqlDbType.VarChar).Value = s;

                    _command.ExecuteNonQuery();
                }

                foreach (uint e in request.EnrolmentsToRemove)
                {
                    _command = new MySqlCommand("DELETE_ENROLMENT", Connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    _command.Parameters.Add("pEnrolmentID", MySqlDbType.UInt32).Value = e;

                    _command.ExecuteNonQuery();
                }

                _transaction.Commit();
            }
            catch
            {
                // Command failed, rollback the database
                _transaction.Rollback();
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }
        }

        /// <summary>
        /// Call stored procedure to retrieve all enrolments from the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<EnrolmentDto> GetEnrolments(GetEnrolmentsRequest request)
        {
            var result = new List<EnrolmentDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_ALL_ENROLMENTS", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    var student = new UserDto()
                    {
                        UserID = _command.Parameters["UserID"].Value.ToString(),
                        FirstName = _command.Parameters["FirstName"].Value.ToString(),
                        LastName = _command.Parameters["LastName"].Value.ToString()
                    };

                    result.Add(new EnrolmentDto()
                    {
                        EnrolmentID = Convert.ToUInt32(row["EnrolmentID"]),
                        Student = student
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }

        /// <summary>
        /// Call stored procedure to retrieve all EnrolmentDtos for students enrolled in specified unit offering.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<EnrolmentDto> GetEnrolmentsByUnitOffering(GetEnrolmentsByUnitOfferingRequest request)
        {
            var result = new List<EnrolmentDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_ENROLMENTS_BY_UNIT_OFFERING", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            _command.Parameters.Add("pUnitOfferingID", MySqlDbType.UInt32).Value = request.UnitOfferingID;

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    var student = new UserDto()
                    {
                        UserID = (string)row["UserID"],
                        FirstName = (string)row["FirstName"],
                        LastName = (string)row["LastName"],
                    };

                    result.Add(new EnrolmentDto()
                    {
                        EnrolmentID = Convert.ToUInt32(row["EnrolmentID"]),
                        Student = student
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }

        /// <summary>
        /// Call stored procedure to retrieve all EnrolmentDtos for students not enrolled in specified unit offering.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<EnrolmentDto> GetEnrolmentsByUnitOfferingInvert(GetEnrolmentsByUnitOfferingRequest request)
        {
            var result = new List<EnrolmentDto>();

            // Connect to database
            Connect();

            // Create the command and specify the stored procedure to call
            _command = new MySqlCommand("GET_ENROLMENTS_BY_UNIT_OFFERING_INVERT", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            _command.Parameters.Add("pUnitOfferingID", MySqlDbType.UInt32).Value = request.UnitOfferingID;

            // Start transaction and execute command
            try
            {
                // Execute
                MySqlDataReader reader;
                DataTable dt = new DataTable("Results");

                _command.ExecuteNonQuery();
                reader = _command.ExecuteReader();
                dt.Load(reader);

                foreach (DataRow row in dt.Rows)
                {
                    var student = new UserDto()
                    {
                        UserID = (string)row["UserID"],
                        FirstName = (string)row["FirstName"],
                        LastName = (string)row["LastName"],
                    };

                    // EnrolmentID should be null
                    result.Add(new EnrolmentDto()
                    {
                        Student = student
                    });
                }
            }
            catch
            {
                // Command failed
                throw;
            }
            finally
            {
                // Disconnect from server regardless of transaction success
                Disconnect();
            }

            return result;
        }
    }
}
