﻿using System;
using System.Collections.Generic;
using System.Text;
using Tcabs.Data;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using System.Net;

namespace Tcabs.Logic
{
    public class UserService
    {
        /// <summary>
        /// Handles business logic for logging a user into the system.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AuthenticateUserResponse AuthenticateUser(AuthenticateUserRequest request)
        {
            var response = new AuthenticateUserResponse();

            try
            {
                var dataAccess = new UserDataAccess();
                
                response.Permissions = dataAccess.AuthenticateUser(request);

                var getUserRequest = new GetUserRequest()
                {
                    UserID = request.UserID
                };
                response.User = dataAccess.GetUser(getUserRequest);

                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.OK, "User login successful."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Handles business logic for add a new user to the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AddUpdateUserResponse AddUpdateUser(AddUpdateUserRequest request)
        {
            var response = new AddUpdateUserResponse();

            try
            {
                response = (AddUpdateUserResponse)request.CheckValidation(response);

                if (response.Status == HttpStatusCode.BadRequest)
                    return response;

                var dataAccess = new UserDataAccess();
                
                dataAccess.AddUpdateUser(request);
                response.Status = HttpStatusCode.OK;

                if (request.IsUpdate)
                {
                    response.StatusMessages.Add(new StatusMessage(
                        HttpStatusCode.OK, "Successfully updated user."));
                }
                else
                {
                    response.StatusMessages.Add(new StatusMessage(
                        HttpStatusCode.OK, "Successfully added user."));
                }
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Handles business logic for retrieveing all users from the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetUsersResponse GetUsers(GetUsersRequest request)
        {
            var response = new GetUsersResponse();

            try
            {
                var dataAccess = new UserDataAccess();
                response.Users = dataAccess.GetUsers(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved users."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Handles business logic for retrieving all users from the database by specified role name.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetUsersByRoleResponse GetUsersByRole(GetUsersByRoleRequest request)
        {
            var response = new GetUsersByRoleResponse();

            try
            {
                var dataAccess = new UserDataAccess();
                response.Users = dataAccess.GetUsersByRole(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved users of role type: " + request.Name));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Handles business logic for retrieving all users with permissions associated with their role.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetUsersByPermissionResponse GetUsersByPermission(GetUsersByPermissionRequest request)
        {
            var response = new GetUsersByPermissionResponse();

            try
            {
                var dataAccess = new UserDataAccess();
                response.Users = dataAccess.GetUsersByPermission(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved users with permissions associated with role type: " + request.Name));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Handles business logic for deleting users from the database by UserID.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DeleteUserResponse DeleteUser(DeleteUserRequest request)
        {
            var response = new DeleteUserResponse();

            try
            {
                var dataAccess = new UserDataAccess();
                dataAccess.DeleteUser(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully removed user."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }
    }
}
