﻿using System;
using System.Collections.Generic;
using System.Text;
using Tcabs.Data;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using System.Net;

namespace Tcabs.Logic
{
    public class ProjectService
    {
        
        public AddUpdateProjectResponse AddUpdateProject(AddUpdateProjectRequest request)
        {
            var response = new AddUpdateProjectResponse();

            try
            {
                response = (AddUpdateProjectResponse)request.CheckValidation(response);

                if (response.Status == HttpStatusCode.BadRequest)
                    return response;

                var dataAccess = new ProjectDataAccess();
                
                dataAccess.AddUpdateProject(request);
                response.Status = HttpStatusCode.OK;

                if (request.IsUpdate)
                {
                    response.StatusMessages.Add(new StatusMessage(
                        HttpStatusCode.OK, "Successfully updated project."));
                }
                else
                {
                    response.StatusMessages.Add(new StatusMessage(
                        HttpStatusCode.OK, "Successfully added project."));
                }
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        public GetProjectsResponse GetProjects(GetProjectsRequest request)
        {
            var response = new GetProjectsResponse();

            try
            {
                var dataAccess = new ProjectDataAccess();
                response.Projects = dataAccess.GetProjects(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retreived projects."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        public DeleteProjectResponse DeleteProject(DeleteProjectRequest request)
        {
            var response = new DeleteProjectResponse();

            try
            {
                var dataAccess = new ProjectDataAccess();
                dataAccess.DeleteProject(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully removed project."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }
    }
}
