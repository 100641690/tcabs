﻿using System;
using System.Collections.Generic;
using System.Text;
using Tcabs.Data;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using System.Net;

namespace Tcabs.Logic
{
    public class TeamService
    {
        
        public AddUpdateTeamResponse AddUpdateTeam(AddUpdateTeamRequest request)
        {
            var response = new AddUpdateTeamResponse();

            try
            {
                response = (AddUpdateTeamResponse)request.CheckValidation(response);

                if (response.Status == HttpStatusCode.BadRequest)
                    return response;

                var dataAccess = new TeamDataAccess();
                
                dataAccess.AddUpdateTeam(request);
                response.Status = HttpStatusCode.OK;

                if (request.IsUpdate)
                {
                    response.StatusMessages.Add(new StatusMessage(
                        HttpStatusCode.OK, "Successfully updated team."));
                }
                else
                {
                    response.StatusMessages.Add(new StatusMessage(
                        HttpStatusCode.OK, "Successfully added team."));
                }
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        public GetTeamsResponse GetTeams(GetTeamsRequest request)
        {
            var response = new GetTeamsResponse();

            try
            {
                var dataAccess = new TeamDataAccess();
                response.Teams = dataAccess.GetTeams(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retreived teams."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        public DeleteTeamResponse DeleteTeam(DeleteTeamRequest request)
        {
            var response = new DeleteTeamResponse();

            try
            {
                var dataAccess = new TeamDataAccess();
                dataAccess.DeleteTeam(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully removed team."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }
    }
}
