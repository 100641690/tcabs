﻿using System;
using System.Collections.Generic;
using System.Text;
using Tcabs.Data;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using System.Net;

namespace Tcabs.Logic
{
    public class UnitService
    {
        /// <summary>
        /// Handles business logic for add a new unit to the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AddUpdateUnitResponse AddUpdateUnit(AddUpdateUnitRequest request)
        {
            var response = new AddUpdateUnitResponse();

            try
            {
                response = (AddUpdateUnitResponse)request.CheckValidation(response);

                if (response.Status == HttpStatusCode.BadRequest)
                    return response;

                var dataAccess = new UnitDataAccess();

                dataAccess.AddUpdateUnit(request);
                response.Status = HttpStatusCode.OK;

                if (request.IsUpdate)
                {
                    response.StatusMessages.Add(new StatusMessage(
                        HttpStatusCode.OK, "Successfully updated unit."));
                }
                else
                {
                    response.StatusMessages.Add(new StatusMessage(
                        HttpStatusCode.OK, "Successfully added unit."));
                }
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Handles business logic for retrieveing all units from the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetUnitsResponse GetUnits(GetUnitsRequest request)
        {
            var response = new GetUnitsResponse();

            try
            {
                var dataAccess = new UnitDataAccess();
                response.Units = dataAccess.GetUnits(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved units."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Handles business logic for deleting a unit from the database by UnitCode.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DeleteUnitResponse DeleteUnit(DeleteUnitRequest request)
        {
            var response = new DeleteUnitResponse();

            try
            {
                var dataAccess = new UnitDataAccess();
                dataAccess.DeleteUnit(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully removed unit."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }
    }
}
