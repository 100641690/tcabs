﻿using System;
using Tcabs.Data;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using System.Net;

namespace Tcabs.Logic
{
    public class EnrolmentService
    {
        /// <summary>
        /// Handles business logic for adding and removing student enrolments in the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AddUpdateEnrolmentsResponse AddUpdateEnrolments(AddUpdateEnrolmentsRequest request)
        {
            var response = new AddUpdateEnrolmentsResponse();

            try
            {
                response = (AddUpdateEnrolmentsResponse)request.CheckValidation(response);

                if (response.Status == HttpStatusCode.BadRequest)
                    return response;

                var dataAccess = new EnrolmentDataAccess();

                dataAccess.AddUpdateEnrolments(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                HttpStatusCode.OK, "Successfully updated enrolment."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Handles business logic for retrieveing all enrolments from the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetEnrolmentsResponse GetEnrolments(GetEnrolmentsRequest request)
        {
            var response = new GetEnrolmentsResponse();

            try
            {
                var dataAccess = new EnrolmentDataAccess();
                response.Enrolments = dataAccess.GetEnrolments(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved enrolments."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Handles business logic for retrieveing all students who are are enrolled in specified unit offering.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetEnrolmentsByUnitOfferingResponse GetEnrolmentsByUnitOffering(GetEnrolmentsByUnitOfferingRequest request)
        {
            var response = new GetEnrolmentsByUnitOfferingResponse();

            try
            {
                var dataAccess = new EnrolmentDataAccess();
                response.Enrolments = dataAccess.GetEnrolmentsByUnitOffering(request);
                response.NotEnrolled = dataAccess.GetEnrolmentsByUnitOfferingInvert(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved enrolments."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }
    }
}
