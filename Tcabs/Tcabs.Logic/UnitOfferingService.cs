﻿using System;
using System.Collections.Generic;
using System.Text;
using Tcabs.Data;
using Tcabs.Common.Requests;
using Tcabs.Common.Responses;
using System.Net;

namespace Tcabs.Logic
{
    public class UnitOfferingService
    {
        /// <summary>
        /// Handles business logic for adding a unit offering to the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AddUpdateUnitOfferingResponse AddUpdateUnitOffering(AddUpdateUnitOfferingRequest request)
        {
            var response = new AddUpdateUnitOfferingResponse();

            try
            {
                response = (AddUpdateUnitOfferingResponse)request.CheckValidation(response);

                if (response.Status == HttpStatusCode.BadRequest)
                    return response;

                var dataAccess = new UnitOfferingDataAccess();

                dataAccess.AddUpdateUnitOffering(request);
                response.Status = HttpStatusCode.OK;

                if (request.UnitOfferingID != null)
                {
                    response.StatusMessages.Add(new StatusMessage(
                        HttpStatusCode.OK, "Successfully updated unit offering."));
                }
                else
                {
                    response.StatusMessages.Add(new StatusMessage(
                        HttpStatusCode.OK, "Successfully added unit offering."));
                }
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Handles business logic for retrieveing all unit offerings from the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetUnitOfferingsResponse GetUnitOfferings(GetUnitOfferingsRequest request)
        {
            var response = new GetUnitOfferingsResponse();

            try
            {
                var dataAccess = new UnitOfferingDataAccess();
                response.UnitOfferings = dataAccess.GetUnitOfferings(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved unit offerings."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Handles business logic for deleting a unit offering from the database by UnitOfferingID.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DeleteUnitOfferingResponse DeleteUnitOffering(DeleteUnitOfferingRequest request)
        {
            var response = new DeleteUnitOfferingResponse();

            try
            {
                var dataAccess = new UnitOfferingDataAccess();
                dataAccess.DeleteUnitOffering(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully removed unit offering."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        public GetTeachingPeriodsResponse GetTeachingPeriods(GetTeachingPeriodsRequest request)
        {
            var response = new GetTeachingPeriodsResponse();

            try
            {
                var dataAccess = new UnitOfferingDataAccess();
                response.TeachingPeriods = dataAccess.GetTeachingPeriods(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved teaching periods."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        public GetTeachingYearsResponse GetTeachingYears(GetTeachingYearsRequest request)
        {
            var response = new GetTeachingYearsResponse();

            try
            {
                var dataAccess = new UnitOfferingDataAccess();
                response.TeachingYears = dataAccess.GetTeachingYears(request);
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved teaching years."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }
    }
}
