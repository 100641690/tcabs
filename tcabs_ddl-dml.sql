DROP DATABASE IF EXISTS tcabs;
CREATE DATABASE tcabs;
USE tcabs;

-------------------
------- DDL -------
-------------------

-- Password currently using SHA-512 encryption
CREATE TABLE User (
	UserID VARCHAR(32) NOT NULL,
	Password VARCHAR(128) NOT NULL,
	FirstName VARCHAR(32) NOT NULL,
	LastName VARCHAR(32) NOT NULL,
    PRIMARY KEY (UserID)
);

CREATE TABLE Role (
	RoleID INT UNSIGNED AUTO_INCREMENT,
	Name VARCHAR(32) NOT NULL,
	PRIMARY KEY (RoleID)
);

CREATE TABLE UserRole (
	UserID VARCHAR(32) NOT NULL,
	RoleID INT UNSIGNED NOT NULL,
	FOREIGN KEY (UserID) REFERENCES User(UserID),
	FOREIGN KEY (RoleID) REFERENCES Role(RoleID)
);

CREATE TABLE Permission (
	PermissionID INT UNSIGNED AUTO_INCREMENT,
	Name VARCHAR(32) NOT NULL,
	PRIMARY KEY (PermissionID)
);

CREATE TABLE RolePermission (
	RoleID INT UNSIGNED NOT NULL,
	PermissionID INT UNSIGNED NOT NULL,
	FOREIGN KEY (RoleID) REFERENCES Role(RoleID),
	FOREIGN KEY (PermissionID) REFERENCES Permission(PermissionID)
);

CREATE TABLE Project (
	ProjectID INT UNSIGNED AUTO_INCREMENT,
	Name VARCHAR(128) NOT NULL,
	Description VARCHAR(2048),
	PRIMARY KEY (ProjectID)
);

CREATE TABLE Duty (
	DutyID INT UNSIGNED AUTO_INCREMENT,
	Name VARCHAR(64) NOT NULL,
	Description VARCHAR(1024),
	Cost DECIMAL UNSIGNED NOT NULL,
	PRIMARY KEY (DutyID)
);

CREATE TABLE ProjectDuty (
	ProjectID INT UNSIGNED NOT NULL,
	DutyID INT UNSIGNED NOT NULL,
	FOREIGN KEY (ProjectID) REFERENCES Project(ProjectID),
	FOREIGN KEY (DutyID) REFERENCES Duty(DutyID)
);

CREATE TABLE Team (
	TeamID INT UNSIGNED AUTO_INCREMENT,
	SupervisorID VARCHAR(32) NOT NULL,
	Name VARCHAR(128) NOT NULL,
	PRIMARY KEY (TeamID),
	FOREIGN KEY (SupervisorID) REFERENCES User(UserID)
);

CREATE TABLE Unit (
	UnitCode VARCHAR(32) NOT NULL,
	ConvenorID VARCHAR(32) NOT NULL,
	Name VARCHAR(60),
	PRIMARY KEY (UnitCode),
	FOREIGN KEY (ConvenorID) REFERENCES User(UserID)
);

CREATE TABLE TeachingPeriod (
	TeachingPeriodID INT UNSIGNED AUTO_INCREMENT,
	Name VARCHAR(60),
	PRIMARY KEY (TeachingPeriodID)
);

CREATE TABLE TeachingYear (
	Year INT UNSIGNED NOT NULL,
	PRIMARY KEY (Year)
);

CREATE TABLE UnitOffering (
	UnitOfferingID INT UNSIGNED AUTO_INCREMENT,
	UnitCode VARCHAR(32) NOT NULL,
	TeachingPeriodID INT UNSIGNED NOT NULL,
	Year INT UNSIGNED NOT NULL,
	PRIMARY KEY (UnitOfferingID),
	FOREIGN KEY (UnitCode) REFERENCES Unit(UnitCode),
	FOREIGN KEY (TeachingPeriodID) REFERENCES TeachingPeriod(TeachingPeriodID),
	FOREIGN KEY (Year) REFERENCES TeachingYear (Year)
);

CREATE TABLE Enrolment (
	EnrolmentID INT UNSIGNED AUTO_INCREMENT,
	UnitOfferingID INT UNSIGNED NOT NULL,
	StudentID VARCHAR(32) NOT NULL,
	PRIMARY KEY (EnrolmentID),
	FOREIGN KEY (UnitOfferingID) REFERENCES UnitOffering(UnitOfferingID),
	FOREIGN KEY (StudentID) REFERENCES User(UserID)
);

CREATE TABLE ProjectTeam (
	ProjectID INT UNSIGNED NOT NULL,
    UnitOfferingID INT UNSIGNED NOT NULL,
	TeamID INT UNSIGNED NOT NULL,
	FOREIGN KEY (ProjectID) REFERENCES Project(ProjectID),
    FOREIGN KEY (UnitOfferingID) REFERENCES UnitOffering(UnitOfferingID),
	FOREIGN KEY (TeamID) REFERENCES Team(TeamID)
);

CREATE TABLE IndividualTask (
	TaskID INT UNSIGNED AUTO_INCREMENT,
    StudentID VARCHAR(32) NOT NULL,
	DutyID INT UNSIGNED NOT NULL,
	TeamID INT UNSIGNED NOT NULL,
    Name VARCHAR(64) NOT NULL,
    Description VARCHAR(1024),
    MinutesTaken VARCHAR(32),
	PRIMARY KEY (TaskID),
	FOREIGN KEY (StudentID) REFERENCES User(UserID),
	FOREIGN KEY (DutyID) REFERENCES Duty(DutyID),
    FOREIGN KEY (TeamID) REFERENCES Team(TeamID)
);

-------------------
------- DML -------
-------------------

-- Create roles
INSERT INTO Role (Name) VALUES ('Administrator');
INSERT INTO Role (Name) VALUES ('Convenor');
INSERT INTO Role (Name) VALUES ('Supervisor');
INSERT INTO Role (Name) VALUES ('Student');

-- Create permissions
INSERT INTO Permission (Name) VALUES ('Administrator');
INSERT INTO Permission (Name) VALUES ('Convenor');
INSERT INTO Permission (Name) VALUES ('Supervisor');
INSERT INTO Permission (Name) VALUES ('Student');

-- Map administrator role to permissions
INSERT INTO RolePermission (RoleID, PermissionID)
VALUES (
		(SELECT RoleID FROM Role WHERE Name = 'Administrator'),
		(SELECT PermissionID FROM Permission WHERE Name = 'Administrator')
);
INSERT INTO RolePermission (RoleID, PermissionID)
VALUES (
		(SELECT RoleID FROM Role WHERE Name = 'Administrator'),
		(SELECT PermissionID FROM Permission WHERE Name = 'Convenor')
);
INSERT INTO RolePermission (RoleID, PermissionID)
VALUES (
		(SELECT RoleID FROM Role WHERE Name = 'Administrator'),
		(SELECT PermissionID FROM Permission WHERE Name = 'Supervisor')
);

-- Map convenor role to permissions
INSERT INTO RolePermission (RoleID, PermissionID)
VALUES (
		(SELECT RoleID FROM Role WHERE Name = 'Convenor'),
		(SELECT PermissionID FROM Permission WHERE Name = 'Convenor')
);
INSERT INTO RolePermission (RoleID, PermissionID)
VALUES (
		(SELECT RoleID FROM Role WHERE Name = 'Convenor'),
		(SELECT PermissionID FROM Permission WHERE Name = 'Supervisor')
);

-- Map supervisor role to permissions
INSERT INTO RolePermission (RoleID, PermissionID)
VALUES (
		(SELECT RoleID FROM Role WHERE Name = 'Supervisor'),
		(SELECT PermissionID FROM Permission WHERE Name = 'Supervisor')
);

-- Map student role to permissions
INSERT INTO RolePermission (RoleID, PermissionID)
VALUES (
		(SELECT RoleID FROM Role WHERE Name = 'Student'),
		(SELECT PermissionID FROM Permission WHERE Name = 'Student')
);

-- Create teaching periods
INSERT INTO TeachingPeriod (Name) VALUES ('Semester 1');
INSERT INTO TeachingPeriod (Name) VALUES ('Semester 2');
INSERT INTO TeachingPeriod (Name) VALUES ('Term 1');
INSERT INTO TeachingPeriod (Name) VALUES ('Term 2');
INSERT INTO TeachingPeriod (Name) VALUES ('Term 3');
INSERT INTO TeachingPeriod (Name) VALUES ('Term 4');
INSERT INTO TeachingPeriod (Name) VALUES ('Summer');
INSERT INTO TeachingPeriod (Name) VALUES ('Winter');

-- Create teaching years
DELIMITER $$
CREATE PROCEDURE GENERATE_TEACHING_YEARS ()
BEGIN
    DECLARE vYear INT DEFAULT 2020;
    DECLARE vEnd INT DEFAULT 2030;
    
    WHILE vYear <= vEnd DO
		INSERT INTO TeachingYear (Year) VALUES (vYear);
        SET vYear = vYear + 1;
    END WHILE;
END$$
DELIMITER ;
CALL GENERATE_TEACHING_YEARS ();
DROP PROCEDURE GENERATE_TEACHING_YEARS;

-- Create default Administrator
-- Username: admin
-- Password: Abc123
INSERT INTO User (UserID, Password, FirstName, LastName) VALUES ('admin', 'bc30ecc758c63691c69e66d846326670924bff9609b5febf39c1e86cca8fb72783ad5e5b7076359e6d709bc58cd379ade1495945f8591e9876cb2634e43139a6', 'Guy', 'Incognito');
INSERT INTO UserRole (UserID, RoleID)
VALUES (
		(SELECT UserID FROM User WHERE UserID = 'admin'),
		(SELECT RoleID FROM Role WHERE Name = 'Administrator')
);

-- Account for Robert Tipping
-- Username: rtipping
-- Password: Def123
INSERT INTO User (UserID, Password, FirstName, LastName) VALUES ('rtipping', 'c3c139ddd27d665b2ad7a8848336be8d51adc21558c6303eb282d8d6fecf852bd364082983a840e52f4d2d70fb87cae8061500e70e032413bcfb90a0d0c410c5', 'Robert', 'Tipping');
INSERT INTO UserRole (UserID, RoleID)
VALUES (
		(SELECT UserID FROM User WHERE UserID = 'rtipping'),
		(SELECT RoleID FROM Role WHERE Name = 'Convenor')
);

-- Account for Andrew Hill
-- Username: 100641690
-- Password: Ghi123
INSERT INTO User (UserID, Password, FirstName, LastName) VALUES ('100641690', 'a5811e61c858e1391a70150844ac9bd9144ecf2654736bca665f1566b7b96dbeceb8df8a502593f84c98850254d1d380ee902fdc290fd1f593d2b098c992e400', 'Andrew', 'Hill');
INSERT INTO UserRole (UserID, RoleID)
VALUES (
		(SELECT UserID FROM User WHERE UserID = '100641690'),
		(SELECT RoleID FROM Role WHERE Name = 'Student')
);

-- Account for Clinton Gilden
-- Username: 102316657
-- Password: Jkl123
INSERT INTO User (UserID, Password, FirstName, LastName) VALUES ('102316657', 'b7d847c44ca22cd8fd4f74656b595fcac2cce61af4fdadfa299dad0e9ae5f72a30b0c556702d88f784fbdf40564761ed77906c3673af1fe8cf9865347be6acee', 'Clinton', 'Gilden');
INSERT INTO UserRole (UserID, RoleID)
VALUES (
		(SELECT UserID FROM User WHERE UserID = '102316657'),
		(SELECT RoleID FROM Role WHERE Name = 'Student')
);

-- Account for Dylan Tran
-- Username: 102238324
-- Password: Mno123
INSERT INTO User (UserID, Password, FirstName, LastName) VALUES ('102238324', 'a9856098c85da91a569edd85cb06c9968c5bf37b87d36bcf4a94b9940ccb97ecd1c54b0144885b8c4f91c31754d6d712d0eff6ee923dbb37ac8266fc30568acf', 'Dylan', 'Tran');
INSERT INTO UserRole (UserID, RoleID)
VALUES (
		(SELECT UserID FROM User WHERE UserID = '102238324'),
		(SELECT RoleID FROM Role WHERE Name = 'Student')
);

-- Account for Harmandeep Kaur
-- Username: 101535794
-- Password: Pqr123
INSERT INTO User (UserID, Password, FirstName, LastName) VALUES ('101535794', '70664150f33b4ebd4a61dbd91f8580a593c3743b4544449d9cbaf8ec9687dd714131db6f6fde945ca5f6560e73c596c4b96134c82a7351a4efc20e6c47964575', 'Harmandeep', 'Kaur');
INSERT INTO UserRole (UserID, RoleID)
VALUES (
		(SELECT UserID FROM User WHERE UserID = '101535794'),
		(SELECT RoleID FROM Role WHERE Name = 'Student')
);

-- Account for Kate Baughan
-- Username: 101155846
-- Password: Stu123
INSERT INTO User (UserID, Password, FirstName, LastName) VALUES ('101155846', '8f595167270fabeaaaeaf91e6836928703bcf287a5c2e9212df8275cc4ed2cc42d7986c4960172671cad25720f1bd71a2c2682431c1d90b75261eeb67ce59c99', 'Kate', 'Baughan');
INSERT INTO UserRole (UserID, RoleID)
VALUES (
		(SELECT UserID FROM User WHERE UserID = '101155846'),
		(SELECT RoleID FROM Role WHERE Name = 'Student')
);

-- Account for Sai Sudhamshu
-- Username: 101960891
-- Password: Vwx123
INSERT INTO User (UserID, Password, FirstName, LastName) VALUES ('101960891', '14035c444c490a9d59c25bdfeb5fc2ae83f3a9bdfbd848359f5d943a91a07e039b8e16e81868ef49098761ad72c6a4a7db7d4c95cde64e2409801d1e25712ae3', 'Sai', 'Sudhamshu');
INSERT INTO UserRole (UserID, RoleID)
VALUES (
		(SELECT UserID FROM User WHERE UserID = '101960891'),
		(SELECT RoleID FROM Role WHERE Name = 'Student')
);